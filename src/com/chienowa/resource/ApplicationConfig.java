package com.chienowa.resource;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationConfig {
    private static Properties properties = new Properties();
    private static final String configPath = new String("config.properties");

    public ApplicationConfig() {
        System.out.println("Call ChangeConfig");
        try {
            final FileInputStream inputStream = new FileInputStream(configPath);
            properties.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLanguage(String lang, String country){
        properties.setProperty("lang", lang);
        properties.setProperty("country", country);
        try {
            final FileOutputStream outputStream = new FileOutputStream(configPath);
            properties.store(outputStream, null);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setProperties(String key, String value){
        properties.setProperty(key, value);
        try {
            final FileOutputStream outputStream = new FileOutputStream(configPath);
            properties.store(outputStream, null);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getProps(String key) {
        return properties.getProperty(key);
    }
}
