package com.chienowa.resource;

import java.util.Locale;
import java.util.ResourceBundle;

public class Resource {
    private static ApplicationConfig applicationConfig = new ApplicationConfig();
    private final Locale locale = new Locale(applicationConfig.getProps("lang"), applicationConfig.getProps("country"));
    public String getResourceString(String key){
        final ResourceBundle resource = ResourceBundle.getBundle("language", locale);
        return resource.getString(key);
    }
    public static String getNewResourceString(String key) {
        final Locale locale = new Locale(applicationConfig.getProps("lang"), applicationConfig.getProps("country"));
        final ResourceBundle resource = ResourceBundle.getBundle("language", locale);
        return resource.getString(key);
    }
}
