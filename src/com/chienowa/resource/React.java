package com.chienowa.resource;

public interface React {
    public void InitUI();
    public void BuildUI();
    public void ReactUI();
}
