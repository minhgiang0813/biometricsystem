/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.nonbio;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnPasswordPanel;
import com.chienowa.customui.CvnSelectModePanel;
import com.chienowa.cvnpalmsensor.MachineWorking;
import com.chienowa.cvnpalmsensor.ManagementMenuFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

/**
 *
 * @author minhg
 */
public class NonSensorMainFrame extends CvnFrame implements ActionListener {
    
    private final short BTNWIDTH = 300;
    private final short BTNHEIGHT = 60;

    private CvnButton btnManagementMenu = null;
    private CvnButton btnTest = null;
    private CvnSelectModePanel pnlSelectMode = null;
    public NonSensorMainFrameThread threadWaitingAction = null;
//    public NonSensorMainFrameMoveScreenThread threadMove = null;

    public NonSensorMainFrame() {
        InitGuide();
        StartThread();
    }

    private void InitGuide() {
        txtCaption.setText("WASHING MACHINE (NONBIO SENSOR MODE)");

        // Selecing mode panel
        pnlSelectMode = new CvnSelectModePanel();
        pnlSelectMode.SetNonBioMainFrame(this);
        pnlSelectMode.setBounds(10, 65, 570, 140);
        getContentPane().add(pnlSelectMode);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);

        btnManagementMenu = new CvnButton();
        btnManagementMenu.setText("Configuration");
        btnManagementMenu.setBounds(12, 530, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnManagementMenu);
        btnManagementMenu.addActionListener(this);

        btnTest = new CvnButton();
        btnTest.setText("Running Machine");
        btnTest.setBounds(330, 530, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnTest);
        btnTest.addActionListener(this);
    }

    // Open Manamgement Menu Frame
    private void Cvn_Open_Management_Menu_Frame() throws IOException {
        CvnPasswordPanel pass = new CvnPasswordPanel();
        if (pass.CheckPassDialog()) {
            StopThread();
            ManagementMenuFrame managamentFrame = new ManagementMenuFrame();
            managamentFrame.SetNonBioMainFrame(this);
            this.setVisible(false);
            managamentFrame.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Wrong Password!!!", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void StopThread() {
        threadWaitingAction.stop();
//        threadMove.StopMe();
    }

    public void StartThread() {
//        AtomicBoolean flgWashing = new AtomicBoolean(false);
        threadWaitingAction = new NonSensorMainFrameThread(this);
        threadWaitingAction.start();
//        threadMove = new NonSensorMainFrameMoveScreenThread(this, flgWashing);
//        threadMove.start();
    }

    public void RunWashing() {
        MachineWorking machineWorking = new MachineWorking();
        machineWorking.setVisible(true);
        this.dispose();
        this.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();

        if (obj.equals(btnManagementMenu)) {
            try {
                Cvn_Open_Management_Menu_Frame();
            } catch (IOException ex) {
                Logger.getLogger(NonSensorMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnTest)) {
            threadWaitingAction.StopMe();
            RunWashing();
        }

    }
}
