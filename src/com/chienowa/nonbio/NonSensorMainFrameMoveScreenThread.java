/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.nonbio;

import java.util.concurrent.atomic.AtomicBoolean;


/**
 *
 * @author minhg
 */
public class NonSensorMainFrameMoveScreenThread extends Thread{
    
    public NonSensorMainFrame mainFrame;
    private AtomicBoolean flgWashing;
    private boolean finish;
    

    public NonSensorMainFrameMoveScreenThread(NonSensorMainFrame frame, AtomicBoolean flg) {
        mainFrame = frame;
        flgWashing = flg;
        finish = false;
        setPriority(1);
    }

    public void run() {
        while (!finish) {            
            if (flgWashing.get()) {
                System.out.println("OUT LOOP");
                mainFrame.RunWashing();
                break;
            }
        }
        

    }
    
    public void StopMe(){
        finish = true;
    }
    
    
}
