/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.nonbio;

import com.chienowa.cvnpalmsensor.MachineWorking;
import com.chienowa.runbackground.AlarmScreen;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class NonSensorMainFrameThread extends Thread {

    private NonSensorMainFrame mainFrame;
    private boolean finish = false;
    private boolean moveWashing = false;

    public NonSensorMainFrameThread(NonSensorMainFrame frame) {
        mainFrame = frame;
    }
    
    public void StopMe(){
        finish = true;
    }
    
    public void StartMe() {
        finish = false;
        this.start();
    }

    @Override
    public void run() {
        //To change body of generated methods, choose Tools | Templates.
        System.out.println("Start receive washing signal!!");
//        if (CommunicateController.WaitWashingCommandFromController()) {
//            System.out.println("Moving to new screen!!");
//            flgWashing.set(true);
//        }
        while (!finish) {

            byte[] receiveData = SerialUartProcess.ReceiveMessage(6);

            if (receiveData != null) {
                UartBufferS receiveBufferS = new UartBufferS(4);
                receiveBufferS.ConvertToUartBuffers(receiveData);
                
                //Get alarm signal
                if (receiveBufferS.headU == UartBufferS.headerTable.get("H_ALARM")) {
                    byte[] arrayR = new byte[4];
                    for (int i = 0; i < arrayR.length; i++) {
                        arrayR[i] = receiveBufferS.contentArray[arrayR.length - 1 - i];
                    }
                    float valueFloat = ConvertByteInt.ConvertByteArrayToFloatArray(arrayR)[0];
                    String alarmString = new String();
                    String valueString = String.format("%.2f", valueFloat);
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_1")) {
                        alarmString = "Error - Over Voltage 1. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_2")) {
                        alarmString = "Error - Over Voltage 2. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_3")) {
                        alarmString = "Error - Over Voltage 3. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("UNDER_VOLTAGE")) {
                        alarmString = "Error - Uder Voltage. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("CURRENT_ABNORMAL")) {
                        alarmString = "Current Abnormal. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_CURRENT")) {
                        alarmString = "Over Current. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("CVCC_ALARM")) {
                        alarmString = "CVCC Alarm. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("FLOW_SENSOR_ERROR")) {
                        alarmString = "Flow Sensor Error: " + valueString;
                    }

                    System.out.println("Got alarm from controller");
                    AlarmScreen alarmScreen = new AlarmScreen(alarmString, mainFrame, receiveBufferS.keyU);
                    alarmScreen.setVisible(true);
                    mainFrame.setVisible(false);
                }
                // Get signal from hand sensor
                if (receiveBufferS.headU == UartBufferS.headerTable.get("H_SET") && receiveBufferS.keyU == UartBufferS.controlstatusTable.get("START_WASHING")) {
                    System.out.println("Received Hand sensor singal");
                    SerialUartProcess.SendByteArray(receiveData);
                    moveWashing = true;
                    StopMe();
                }
            }

        }
        if (moveWashing) {
            mainFrame.RunWashing();
        }
        System.out.println("Exit NonBio MainFrame Thread");

    }

}
