/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.runbackground;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JSeparator;
import org.json.simple.parser.ParseException;

/**
 *
 * @author minhg
 */
public class AlarmScreen extends CvnFrame implements ActionListener {

    private JFrame mainFrame = null;
    private CvnButton btnOK = null;
    private byte keyU;

    public AlarmScreen(String receiveString, JFrame frame, byte keyU) {
        mainFrame = frame;
        this.keyU = keyU;

        txtCaption.setText("ALARM!!!");
        
        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);

        btnOK = new CvnButton();
        btnOK.setText("OK");
        btnOK.setBounds(12, 530, 200, 57);
        this.add(btnOK);
        btnOK.addActionListener(this);
        
        Font labelFont = new Font("MS UI Gothic", Font.BOLD, 20);

        CvnLabel lblInform = new CvnLabel(receiveString);
        lblInform.setFont(labelFont);
        lblInform.setBounds(20, 75, 800, 65);
        this.add(lblInform);
        
        try {
            AlarmJSONLog alarmLog = new AlarmJSONLog();
            alarmLog.WriteJSONLog(receiveString);
            
        } catch (IOException ex) {
            Logger.getLogger(AlarmScreen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AlarmScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void SendClearSignal() {
        System.out.println("Send Clear Signal");
        UartBufferS replyBuffer = new UartBufferS(4);
        replyBuffer.headU = UartBufferS.headerTable.get("H_CLEAR");
        replyBuffer.keyU = keyU;
        replyBuffer.contentArray = new byte[]{0, 0, 0, 0};
        SerialUartProcess.SendByteArray(replyBuffer.ByteBuffer());
        this.dispose();
        this.setVisible(false);
        mainFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnOK)) {
            SendClearSignal();
        }
    }

}
