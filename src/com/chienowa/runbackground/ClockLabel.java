/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.runbackground;

import com.chienowa.ulti.CvnCurrentDateTime;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author minhg
 */
public class ClockLabel extends JLabel implements ActionListener {

    public Timer timerTimeandDate;

    private static final String TIME_FORMATTER = "HH:mm:ss";

    public ClockLabel() {
        Font labelFont = new Font("MS UI Gothic", Font.BOLD, 24);
        setFont(labelFont);
        setBounds(880, 530, 250, 57);
        timerTimeandDate = new Timer(1000, this);
        timerTimeandDate.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            GetCurrentDateTime();
        } catch (IOException ex) {
            Logger.getLogger(ClockLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void GetCurrentDateTime() throws IOException {
        CvnCurrentDateTime current = new CvnCurrentDateTime();
        LocalDateTime localDateTime = current.GetLocalDateTime();
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern(TIME_FORMATTER);
        String formatTime = localDateTime.format(stringTime_formatter);
        setText(formatTime);
    }

}
