/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.runbackground;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author minhg
 */
public class AlarmJSONLog {

    private final String JSONFILE = "alarmlog.json";
    public JSONArray alarmList = null;

    public AlarmJSONLog() throws IOException, ParseException {
        ReadJSONLog();

    }

    public void ReadJSONLog() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();

        try {
            FileReader reader = new FileReader(JSONFILE);
            Object obj = jsonParser.parse(reader);
            alarmList = (JSONArray) obj;
//            System.err.println(alarmList);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AlarmJSONLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void WriteJSONLog(String description) {
        DateTimeFormatter datefm = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter timefm = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        JSONObject alarmDetail = new JSONObject();
        alarmDetail.put("Date", datefm.format(now));
        alarmDetail.put("Description", description);
        alarmDetail.put("Time", timefm.format(now));
        
        alarmList.add(alarmDetail);

        try (FileWriter file = new FileWriter(JSONFILE)) {
            file.write(alarmList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
