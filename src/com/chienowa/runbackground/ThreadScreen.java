/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.runbackground;

import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.SerialUartProcess;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author minhg
 */
public class ThreadScreen extends Thread {
    
    private Screen sc;
    private JFrame nextFrame;
    byte[] receiveData = null;
    
    public ThreadScreen(Screen screen, JFrame nextFrame) {
        this.sc = screen;
        this.nextFrame = nextFrame;
        
        sc.setVisible(true);
        nextFrame.setVisible(false);
    }
    
    @Override
    public void run() {
        if (CommunicateController.SplashScreenConfirm()) {
            System.out.print("Out Splash Screen!!");
            sc.setVisible(false);
            sc.dispose();
            nextFrame.setVisible(true);
        }
    }
    
}
