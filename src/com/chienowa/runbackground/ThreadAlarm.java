/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.runbackground;

import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class ThreadAlarm extends Thread {

    public JFrame frame;

    public ThreadAlarm(JFrame frame) {
        this.frame = frame;
        setPriority(1);
    }

    @Override
    public void run() {
        //To change body of generated methods, choose Tools | Templates.
        System.out.println("Start receive alarm signal!!");
        while (true) {

            byte[] receiveData = SerialUartProcess.ReceiveMessage(6);
//                System.out.println(receiveData);
            if (receiveData != null) {
                UartBufferS receiveBufferS = new UartBufferS(4);
                receiveBufferS.ConvertToUartBuffers(receiveData);
                if (receiveBufferS.headU == UartBufferS.headerTable.get("H_ALARM")) {
                    byte[] arrayR = new byte[4];
                    for (int i = 0; i < arrayR.length; i++) {
                        arrayR[i] = receiveBufferS.contentArray[arrayR.length - 1 - i];
                    }
                    float valueFloat = ConvertByteInt.ConvertByteArrayToFloatArray(arrayR)[0];
                    String alarmString = new String();
                    String valueString = String.format("%.2f", valueFloat);
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_1")) {
                        alarmString = "Error - Over Voltage 1. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_2")) {
                        alarmString = "Error - Over Voltage 2. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_VOLTAGE_3")) {
                        alarmString = "Error - Over Voltage 3. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("UNDER_VOLTAGE")) {
                        alarmString = "Error - Uder Voltage. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("CURRENT_ABNORMAL")) {
                        alarmString = "Current Abnormal. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("OVER_CURRENT")) {
                        alarmString = "Over Current. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("CVCC_ALARM")) {
                        alarmString = "CVCC Alarm. Value: " + valueString;
                    }
                    if (receiveBufferS.keyU == UartBufferS.controlstatusTable.get("FLOW_SENSOR_ERROR")) {
                        alarmString = "Flow Sensor Error: " + valueString;
                    }

                    System.out.println("Got alarm from controller");
//                    JOptionPane.showMessageDialog(frame, alarmString, "Error", JOptionPane.ERROR_MESSAGE);
//                    UartBufferS replyBuffer = new UartBufferS(4);
//                    replyBuffer.headU = UartBufferS.headerTable.get("H_CLEAR");
//                    replyBuffer.keyU = receiveBufferS.keyU;
//                    replyBuffer.contentArray = new byte[]{0, 0, 0, 0};
//                    SerialUartProcess.SendByteArray(replyBuffer.ByteBuffer());
                    AlarmScreen alarmScreen = new AlarmScreen(alarmString, frame, receiveBufferS.keyU);
                    alarmScreen.setVisible(true);
                    frame.setVisible(false);
                }
            }

        }

    }

}
