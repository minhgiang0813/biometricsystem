/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;

/**
 *
 * @author minhg
 */
public class CvnSV extends JButton {

    private static final String PS_FONT_NAME = null;
    private static final int PS_BTN_FONT_SIZE = 14;
    

    public CvnSV(String name) {
        Font buttonFont = new Font(PS_FONT_NAME, Font.BOLD, PS_BTN_FONT_SIZE);
        setFont(buttonFont);
        setText(name);
    }

    public void SetNormalButton() {
        Color normalBg = new Color(238, 238, 238);
        this.setBackground(normalBg);
        this.setForeground(Color.BLACK);
    }

    public void SetActiveButton() {
        this.setBackground(Color.GRAY);
        this.setForeground(Color.WHITE);
    }

}
