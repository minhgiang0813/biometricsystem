/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;

/**
 *
 * @author minhg
 */
public class CvnGuideImageLabel extends JLabel {

    private BufferedImage guideImage = null;

    public CvnGuideImageLabel() {
    }

    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(guideImage, 0, 0, null);
    }

    public void SetGuideImage(BufferedImage handGuide) {

        int displayW = getWidth();
        int displayH = getHeight();

        BufferedImage resize = new BufferedImage(displayW, displayH, handGuide.getType());
        Graphics g = resize.getGraphics();
        g.drawImage(
                handGuide.getScaledInstance(displayW, displayH, Image.SCALE_SMOOTH),
                0, 0, displayW, displayH, null);
        g.dispose();

        guideImage = resize;
    }
}
