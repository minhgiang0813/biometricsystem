/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author minhg
 */
public class CvnFrame extends JFrame {
    
    private static final String PS_FONT_NAME = null;

    public JTextField txtCaption = null;

    public CvnFrame() {

        // Set main frame
        setBounds(0, 0, 1024, 599);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        getContentPane().setLayout(null);
        setUndecorated(true);

        // Text for title
        txtCaption = new JTextField();
        txtCaption.setEditable(false);
        txtCaption.setFont(new Font(PS_FONT_NAME, Font.BOLD, 26));
        txtCaption.setHorizontalAlignment(SwingConstants.CENTER);
        txtCaption.setText("Chienowa Vietnam Product");
        txtCaption.setBackground(Color.CYAN);
        txtCaption.setBounds(12, 10, 1000, 35);
        getContentPane().add(txtCaption);
        txtCaption.setColumns(10);
    }

}
