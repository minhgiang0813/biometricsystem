/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author minhg
 */
public class CvnLabel extends JLabel {

    public CvnLabel(String text) {

        Font labelFont = new Font("MS UI Gothic", Font.PLAIN, 22);
        setFont(labelFont);
        setText(text);
        
    }
}
