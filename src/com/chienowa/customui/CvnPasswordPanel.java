/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import com.chienowa.ulti.VirtualKeyboard;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

/**
 *
 * @author minhg
 */
public class CvnPasswordPanel extends JPanel {

    private final String FILENAME = "CvnPPanel.au";

    public CvnPasswordPanel() {
    }

    public boolean CheckPassDialog() throws IOException {
        VirtualKeyboard.Show();
        boolean result = false;
        String pwd = GetPasswordFromFile();
        JLabel label = new JLabel("Enter a password:");
        JPasswordField pass = new JPasswordField(10);
        this.add(label);
        this.add(pass);
        String[] options = new String[]{"OK", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, this, "Password",
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[1]);
        if (option == 0) // pressing OK button
        {
            char[] password = pass.getPassword();
            String pwdString;
            pwdString = String.valueOf(password);
            
            if (pwdString.equals(pwd)) {
                result = true;
            }
        }
        VirtualKeyboard.Hide();
        return result;

    }

    private String GetPasswordFromFile() throws FileNotFoundException, IOException {
        File f = new File(FILENAME);
        FileReader fr = new FileReader(f);

        BufferedReader br = new BufferedReader(fr);
        String line = null;
        line = br.readLine();
        if (line != null) {
            return line;
        }

        fr.close();
        br.close();
        return line;
    }

}
