/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.customui;

import com.chienowa.nonbio.NonSensorMainFrame;
import com.chienowa.nonbio.NonSensorMainFrameMoveScreenThread;
import com.chienowa.nonbio.NonSensorMainFrameThread;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.UartBufferS;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import com.fujitsu.frontech.palmsecure_smpl.PsSampleApl;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

/**
 *
 * @author minhg
 */
public class CvnSelectModePanel extends JPanel {

    private static final String PS_FONT_NAME = null;
    private CvnButton btnBaseMode;
    private CvnButton btnAcidMode;
    private CvnButton btnWatterMode;
    private CvnButton btnHandMode;

    private final short XBTNPOSITION = 20;
    private final short YBTNPOSITION = 10;
    private final short XBTNSPACE = 280;
    private final short YBTNSPACE = 60;
    private final short BTNWIDTH = 250;
    private final short BTNHEIGHT = 55;
    private PsMainFrame mFrame = null;
    private NonSensorMainFrame nonFrame = null;

    public int currentMode = -1;

    public CvnSelectModePanel() {
        setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        setLayout(null);
        InitUI();
        GetInitMode();
    }

    public void SetBioMainFrame(PsMainFrame frame) {
        mFrame = frame;
    }

    public void SetNonBioMainFrame(NonSensorMainFrame frame) {
        nonFrame = frame;
    }

    private void InitUI() {
        btnHandMode = new CvnButton();
        btnHandMode.setText("Hand Mode");
        btnHandMode.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnHandMode.SetNormalButton();
        this.add(btnHandMode);
        btnHandMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currentMode = 1;
                CheckCurrentMode();
                SaveCurrentMode();
            }
        });

        btnWatterMode = new CvnButton();
        btnWatterMode.setText("Water Mode");
        btnWatterMode.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnWatterMode.SetNormalButton();
        this.add(btnWatterMode);
        btnWatterMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currentMode = 2;
                CheckCurrentMode();
                SaveCurrentMode();
            }
        });

        btnAcidMode = new CvnButton();
        btnAcidMode.setText("Acid Mode");
        btnAcidMode.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnAcidMode.SetNormalButton();
        this.add(btnAcidMode);
        btnAcidMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currentMode = 3;
                CheckCurrentMode();
                SaveCurrentMode();
            }
        });

        btnBaseMode = new CvnButton();
        btnBaseMode.setText("Base Mode");
        btnBaseMode.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnBaseMode.SetNormalButton();
        this.add(btnBaseMode);
        btnBaseMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currentMode = 4;
                CheckCurrentMode();
                SaveCurrentMode();
            }
        });

    }

    private void CheckCurrentMode() {
        switch (currentMode) {
            case 1:
                SetActiveButton(btnHandMode);
                break;
            case 2:
                SetActiveButton(btnWatterMode);
                break;
            case 3:
                SetActiveButton(btnAcidMode);
                break;
            case 4:
                SetActiveButton(btnBaseMode);
                break;
            default:
                System.out.println("Mode is not valid");
        }
    }

    private void SetActiveButton(CvnButton button) {

        //set normal background for button
        btnBaseMode.SetNormalButton();
        btnAcidMode.SetNormalButton();
        btnWatterMode.SetNormalButton();
        btnHandMode.SetNormalButton();

        button.SetActiveButton();
    }

    private void SaveCurrentMode() {
        if (PsSampleApl.bioMode == 1) {
            mFrame.StopBackgroundRunning();
        } else {
            nonFrame.StopThread();
        }
        if (currentMode > 0) {
            UartBufferS sendUart = new UartBufferS(4);
            sendUart.headU = UartBufferS.headerTable.get("H_SET");
            sendUart.keyU = UartBufferS.controlstatusTable.get("WASHING_MODE");
            sendUart.contentArray = ConvertByteInt.intToByteArray(currentMode);
            if (CommunicateController.SendDataToController(sendUart)) {
//                JOptionPane.showMessageDialog(this, "Save Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                System.out.println("Change mode Successfully!!");
            } else {
                JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Error! Mode is not valid", "Error", JOptionPane.ERROR_MESSAGE);
        }
        if (PsSampleApl.bioMode == 1) {
            mFrame.StartBackgroundRunning();
        } else {
            nonFrame.StartThread();
        }

    }

    private void GetInitMode() {
        currentMode = DataFromController.GetModeSettingFromController();
        System.out.println("Current mode: " + currentMode);
        CheckCurrentMode();
    }

}
