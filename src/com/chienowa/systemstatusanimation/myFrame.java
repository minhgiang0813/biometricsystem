package com.chienowa.systemstatusanimation;

import com.chienowa.customui.CvnButton;
import com.chienowa.cvnpalmsensor.ManagementMenuFrame;
import com.chienowa.cvnpalmsensor.TestModeFrame;
import com.chienowa.cvnpalmsensor.ThreadSystemStatus;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.UartBufferS;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import javax.swing.Timer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class myFrame extends JFrame implements ActionListener {

    private Timer timerTimeandDate;
    private javax.swing.JPanel WaterSofnerPanel;
    private javax.swing.JButton Menu;
    private javax.swing.JLabel textWaterSofner;
    private javax.swing.JPanel FilterPanel;
    private javax.swing.JLabel textFilter;
    private javax.swing.JPanel RS1Panel;
    private javax.swing.JLabel textRS1;
    private javax.swing.JLabel valueRS1;
    private javax.swing.JLabel unitRS1;

    private javax.swing.JPanel ElectrolyzePanel;
    private javax.swing.JLabel unitI;
    private javax.swing.JLabel unitV;
    private javax.swing.JLabel valueI;
    private javax.swing.JLabel valueV;
    private javax.swing.JLabel textElectrolyze;
    private javax.swing.JLabel textElectrolyzeTime;
    private javax.swing.JLabel textElectrolyzeTimeValue;
    private javax.swing.JLabel textDrainageModeTime;
    private javax.swing.JLabel textDrainageModeTimeValue;
    private javax.swing.JLabel textTotalRunTimeValue;
    private javax.swing.JLabel textTotalRunTime;
    private javax.swing.JLabel textDate;
    private javax.swing.JLabel textTime;

    private CvnButton btnMode;
 

    private javax.swing.JPanel TitlePanel;
    private javax.swing.JLabel textTitle;
    private javax.swing.JPanel Tank1Panel;
    private javax.swing.JLabel Tank1Icon;
    private javax.swing.JPanel Tank2Panel;
    private javax.swing.JLabel Tank2Icon;
    private javax.swing.JPanel SlatTankPanel;
    private javax.swing.JLabel SlatTankIcon;

    private javax.swing.JPanel TotalRunPanel;
    private javax.swing.JPanel TotalElectrolyzeRunPanel;
    private javax.swing.JPanel DrainageModeRunPanel;
    private javax.swing.JPanel DateAndTimePanel;
    private static final String TIME_FORMATTER = "HH:mm:ss";
    private static final String DATE_FORMATTER = "dd/MM/yyyy";
    private int h = 0;
    public int Mode;
    public int Mode_old = 0;
    public int Hello = 0;
    public int Hello_old = 0;
    private String RunTime;
    private String ElectrolyedTime;
    private String DraingeTime;
    private String RS1_value;
    private String V_Electrolyze;
    private String I_Electrolyze;
    private String V_SP;
    private int Tank1Level;
    private int Tank2Level;
    private int SlatTankLevel;
    private flowWater FlowWater;
    private ThreadSystemStatus threadSystemStatus;
    private ManagementMenuFrame motherFrame;
    
    public myFrame(ManagementMenuFrame frame) throws HeadlessException, InterruptedException {
        this.motherFrame = frame;
        threadSystemStatus = new ThreadSystemStatus("t1", this);
        threadSystemStatus.start();
    }

    public void SetTankLevel(int Tank1Level, int Tank2Level, int SlatTankLevel) {
        this.Tank1Level = Tank1Level;
        this.Tank2Level = Tank2Level;
        this.SlatTankLevel = SlatTankLevel;
    }

    public void ThongSo(int RunTime, int ElectrolyedTime,
            int DraingeTime, float RS1_value,
            float V_Electrolyze, float I_Electrolyze, int V_SP) {
        this.RunTime = String.valueOf(RunTime);
        this.ElectrolyedTime = String.valueOf(ElectrolyedTime);
        this.DraingeTime = String.valueOf(DraingeTime);
        this.RS1_value = String.format("%.2f", RS1_value);
        this.V_Electrolyze = String.format("%.2f", V_Electrolyze);
        this.I_Electrolyze = String.format("%.2f", I_Electrolyze);
//        this.RS1_value = String.valueOf(RS1_value);
//        this.V_Electrolyze = String.valueOf(V_Electrolyze);
//        this.I_Electrolyze = String.valueOf(I_Electrolyze);
        this.V_SP = String.valueOf(V_SP);
    }

    public void SetMode(int Mode) {
        this.Mode = Mode;
    }

    private void ReturnMainFrame() throws IOException {
        this.threadSystemStatus.executor.shutdown();
        this.threadSystemStatus.stop();
        this.setVisible(false);
        this.dispose();
        
        motherFrame.setVisible(true);
        motherFrame.StartBackgroundRunning();

//        Runtime.getRuntime().exec("java -jar PalmSecureSample_Java.jar");
//        System.exit(0);
    }

    public void initUI() {

        timerTimeandDate = new Timer(1000, this);
        timerTimeandDate.start();

        /**
         * ****** Flow Water start ********
         */
        FlowWater = new flowWater();
        add(FlowWater);
        FlowWater.setLayout(null);
        FlowWater.setBounds(0, 0, 1024, 600);
//        FlowWater.inputBackgroud("backgroundMode1.png");

        /**
         * ******* Water Sofner Panel **********
         */
        WaterSofnerPanel = new javax.swing.JPanel();
        FlowWater.add(WaterSofnerPanel);
        WaterSofnerPanel.setLayout(null);
        WaterSofnerPanel.setBounds(17, 248, 125, 55);
        WaterSofnerPanel.setBackground(new java.awt.Color(128, 128, 192));
        WaterSofnerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textWaterSofner = new javax.swing.JLabel();
        WaterSofnerPanel.add(textWaterSofner);
        textWaterSofner.setText("Water Sofner");
        textWaterSofner.setBounds(8, 18, 125, 20);
        textWaterSofner.setFont(new java.awt.Font("Unispace", 1, 14));

        /**
         * ******** Filter Panel **********
         */
        FilterPanel = new javax.swing.JPanel();
        FlowWater.add(FilterPanel);
        FilterPanel.setLayout(null);
        FilterPanel.setBounds(17, 330, 125, 55);
        FilterPanel.setBackground(new java.awt.Color(128, 128, 192));
        FilterPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textFilter = new javax.swing.JLabel();
        FilterPanel.add(textFilter);
        textFilter.setBounds(35, 18, 125, 20);
        textFilter.setText("Filter");
        textFilter.setFont(new java.awt.Font("Unispace", 1, 14));

        /**
         * ******** Title **********
         */
        TitlePanel = new javax.swing.JPanel();
        FlowWater.add(TitlePanel);
        TitlePanel.setLayout(null);
        TitlePanel.setBounds(362, 16, 300, 60);
        TitlePanel.setBackground(new java.awt.Color(255, 255, 255));
        TitlePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textTitle = new javax.swing.JLabel();
        TitlePanel.add(textTitle);
        textTitle.setBounds(75, 0, 300, 60);
        textTitle.setText("SYSTEM STATUS");
        textTitle.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * **** Tank 1 ******
         */
        Tank1Panel = new javax.swing.JPanel();
        FlowWater.add(Tank1Panel);
        Tank1Panel.setLayout(null);
        Tank1Panel.setBounds(372, 218, 100, 142);
        Tank1Icon = new javax.swing.JLabel();
        Tank1Panel.add(Tank1Icon);
        Tank1Icon.setIcon(new ImageIcon("Tank1L.png"));
        Tank1Icon.setBounds(0, 0, 100, 142);

        /**
         * **** Tank 2 ******
         */
        Tank2Panel = new javax.swing.JPanel();
        FlowWater.add(Tank2Panel);
        Tank2Panel.setLayout(null);
        Tank2Panel.setBounds(593, 218, 100, 142);
        Tank2Icon = new javax.swing.JLabel();
        Tank2Panel.add(Tank2Icon);
        Tank2Icon.setIcon(new ImageIcon("Tank2L.png"));
        Tank2Icon.setBounds(0, 0, 100, 142);

        /**
         * **** Slat Tank ******
         */
        SlatTankPanel = new javax.swing.JPanel();
        FlowWater.add(SlatTankPanel);
        SlatTankPanel.setLayout(null);
        SlatTankPanel.setBounds(855, 135, 100, 70);
        SlatTankIcon = new javax.swing.JLabel();
        SlatTankPanel.add(SlatTankIcon);
        SlatTankIcon.setIcon(new ImageIcon("SlatTankH.png"));
        SlatTankIcon.setBounds(0, 0, 100, 70);

        /**
         * ******** RS1 Panel **********
         */
        RS1Panel = new javax.swing.JPanel();
        FlowWater.add(RS1Panel);
        RS1Panel.setLayout(null);
        RS1Panel.setBounds(17, 164, 125, 60);
        RS1Panel.setBackground(new java.awt.Color(128, 128, 192));
        RS1Panel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textRS1 = new javax.swing.JLabel();
        RS1Panel.add(textRS1);
        textRS1.setBounds(50, 5, 125, 20);
        textRS1.setText("RS1");
        textRS1.setFont(new java.awt.Font("Unispace", 1, 14));

        unitRS1 = new javax.swing.JLabel();
        RS1Panel.add(unitRS1);
        unitRS1.setBounds(85, 30, 40, 20);
        unitRS1.setText("L/M");
        unitRS1.setFont(new java.awt.Font("Unispace", 1, 16));

        valueRS1 = new javax.swing.JLabel();
        RS1Panel.add(valueRS1);
        valueRS1.setBounds(15, 30, 50, 20);
        valueRS1.setText(RS1_value);
        valueRS1.setFont(new java.awt.Font("Unispace", 1, 16));

        /**
         * ******* Total Run Time **********
         */
        TotalRunPanel = new javax.swing.JPanel();
        FlowWater.add(TotalRunPanel);
        TotalRunPanel.setLayout(null);
        TotalRunPanel.setBounds(894, 404, 120, 50);
        TotalRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        TotalRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textTotalRunTime = new javax.swing.JLabel();
        TotalRunPanel.add(textTotalRunTime);
        textTotalRunTime.setBounds(15, 18, 125, 20);
        textTotalRunTime.setText(RunTime);
        textTotalRunTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textTotalRunTimeValue = new javax.swing.JLabel();
        TotalRunPanel.add(textTotalRunTimeValue);
        textTotalRunTimeValue.setBounds(90, 18, 125, 20);
        textTotalRunTimeValue.setText("H");
        textTotalRunTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * ******* Total Electrolyze Run Time *********
         */
        TotalElectrolyzeRunPanel = new javax.swing.JPanel();
        FlowWater.add(TotalElectrolyzeRunPanel);
        TotalElectrolyzeRunPanel.setLayout(null);
        TotalElectrolyzeRunPanel.setBounds(894, 462, 120, 52);
        TotalElectrolyzeRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        TotalElectrolyzeRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textElectrolyzeTime = new javax.swing.JLabel();
        TotalElectrolyzeRunPanel.add(textElectrolyzeTime);
        textElectrolyzeTime.setBounds(15, 18, 125, 20);
        textElectrolyzeTime.setText(ElectrolyedTime);
        textElectrolyzeTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textElectrolyzeTimeValue = new javax.swing.JLabel();
        TotalElectrolyzeRunPanel.add(textElectrolyzeTimeValue);
        textElectrolyzeTimeValue.setBounds(90, 18, 125, 20);
        textElectrolyzeTimeValue.setText("H");
        textElectrolyzeTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * **** Total Drainage Mode Run Time ******
         */
        DrainageModeRunPanel = new javax.swing.JPanel();
        FlowWater.add(DrainageModeRunPanel);
        DrainageModeRunPanel.setLayout(null);
        DrainageModeRunPanel.setBounds(894, 524, 120, 50);
        DrainageModeRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        DrainageModeRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textDrainageModeTime = new javax.swing.JLabel();
        DrainageModeRunPanel.add(textDrainageModeTime);
        textDrainageModeTime.setBounds(15, 18, 125, 20);
        textDrainageModeTime.setText(DraingeTime);
        textDrainageModeTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textDrainageModeTimeValue = new javax.swing.JLabel();
        DrainageModeRunPanel.add(textDrainageModeTimeValue);
        textDrainageModeTimeValue.setBounds(90, 18, 125, 20);
        textDrainageModeTimeValue.setText("H");
        textDrainageModeTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * ******* Date And Time Panel *********
         */
        DateAndTimePanel = new javax.swing.JPanel();
        FlowWater.add(DateAndTimePanel);
        DateAndTimePanel.setLayout(null);
        DateAndTimePanel.setBounds(850, 15, 165, 60);
        DateAndTimePanel.setBackground(new java.awt.Color(184, 230, 254));
        DateAndTimePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textTime = new javax.swing.JLabel();
        DateAndTimePanel.add(textTime);
        textTime.setBounds(45, 5, 125, 20);
        textTime.setText("14 : 47");
        textTime.setFont(new java.awt.Font("Unispace", 1, 16));

        textDate = new javax.swing.JLabel();
        DateAndTimePanel.add(textDate);
        textDate.setBounds(35, 33, 125, 20);
        textDate.setText("11 / 08 / 2021");
        textDate.setFont(new java.awt.Font("Unispace", 1, 16));

        /**
         * ******* Electrolyze Panel ***********
         */
        ElectrolyzePanel = new javax.swing.JPanel();
        FlowWater.add(ElectrolyzePanel);
        ElectrolyzePanel.setLayout(null);
        ElectrolyzePanel.setBounds(435, 93, 190, 82);
        ElectrolyzePanel.setBackground(new java.awt.Color(128, 128, 192));
        ElectrolyzePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textElectrolyze = new javax.swing.JLabel();
        ElectrolyzePanel.add(textElectrolyze);
        textElectrolyze.setBounds(50, 5, 125, 20);
        textElectrolyze.setText("Electrolyze");
        textElectrolyze.setFont(new java.awt.Font("Unispace", 1, 14));

        unitV = new javax.swing.JLabel();
        ElectrolyzePanel.add(unitV);
        unitV.setBounds(140, 30, 20, 20);
        unitV.setText("V");
        unitV.setFont(new java.awt.Font("Unispace", 1, 16));

        unitI = new javax.swing.JLabel();
        ElectrolyzePanel.add(unitI);
        unitI.setBounds(140, 55, 20, 20);
        unitI.setText("A");
        unitI.setFont(new java.awt.Font("Unispace", 1, 16));

        valueV = new javax.swing.JLabel();
        ElectrolyzePanel.add(valueV);
        valueV.setBounds(40, 30, 80, 20);
        valueV.setText(V_Electrolyze);
        valueV.setFont(new java.awt.Font("Unispace", 1, 16));

        valueI = new javax.swing.JLabel();
        ElectrolyzePanel.add(valueI);
        valueI.setBounds(40, 55, 80, 20);
        valueI.setText(I_Electrolyze);
        valueI.setFont(new java.awt.Font("Unispace", 1, 16));

        // /********* Mode button***********/
        // btnMode = new javax.swing.JButton();
        // FlowWater.add(btnMode);
        // btnMode.setText("MODE");
        // btnMode.setBounds(564, 14, 130, 60);
        // btnMode.setFont(new java.awt.Font("Unispace", 1, 24));
        // btnMode.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent e) {
        // } 				
        // }
        // });
        /**
         * ******* Back button**********
         */
        btnMode = new CvnButton();
        FlowWater.add(btnMode);
        btnMode.setText("BACK");
        btnMode.setBounds(12, 530, 150, 57);
        btnMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ReturnMainFrame();
                } catch (IOException ex) {
                    Logger.getLogger(myFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Timer timer = FlowWater.getTimer();
                timer.stop();
            }
        });


        /**
         * * Setup Frame **
         */
        setTitle("FlowWater");
        setSize(1024, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        System.out.println("Hello");
    }

//    private void OpenTestMode() {
//           
//            threadSystemStatus.stop();
//            this.setVisible(false);
//            this.dispose();
//            TestModeFrame testModeFrame = new TestModeFrame();
//            testModeFrame.setVisible(true);
//    }



    @Override
    public void actionPerformed(ActionEvent e) {

        /**
         * ** Get Time ***
         */
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern(TIME_FORMATTER);
        String formatTime = localTime.format(stringTime_formatter);
        textTime.setText(formatTime);

        /**
         * ** Get Date ***
         */
        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter stringDate_formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDate = localDate.format(stringDate_formatter);
        textDate.setText(formatDate);

        /**
         * Thong So *
         */
        textDrainageModeTime.setText(DraingeTime);
        textElectrolyzeTime.setText(ElectrolyedTime);
        textTotalRunTime.setText(RunTime);
        valueI.setText(I_Electrolyze);
        valueV.setText(V_Electrolyze);
        valueRS1.setText(RS1_value);

        /* tank 1 & tank 2 */
        if (Tank1Level == 0) {
            Tank1Icon.setIcon(new ImageIcon("Tank1N.png"));
        } else if (Tank1Level == 1) {
            Tank1Icon.setIcon(new ImageIcon("Tank1L.png"));
        } else if (Tank1Level == 2) {
            Tank1Icon.setIcon(new ImageIcon("Tank1M.png"));
        } else if (Tank1Level == 3) {
            Tank1Icon.setIcon(new ImageIcon("Tank1H.png"));
        }

        if (Tank2Level == 0) {
            Tank2Icon.setIcon(new ImageIcon("Tank2N.png"));
        } else if (Tank2Level == 1) {
            Tank2Icon.setIcon(new ImageIcon("Tank2L.png"));
        } else if (Tank2Level == 2) {
            Tank2Icon.setIcon(new ImageIcon("Tank2M.png"));
        } else if (Tank2Level == 3) {
            Tank2Icon.setIcon(new ImageIcon("Tank2H.png"));
        }

        if (SlatTankLevel == 0) {
            SlatTankIcon.setIcon(new ImageIcon("SaltWaterN.png"));
        } else if (SlatTankLevel == 1) {
            SlatTankIcon.setIcon(new ImageIcon("SaltWaterL.png"));
        } else if (SlatTankLevel == 2) {
            SlatTankIcon.setIcon(new ImageIcon("SaltWaterH.png"));
        }

        /**
         * ********* Set Mode **********
         */
        if (Mode != Mode_old) {
            Mode_old = Mode;
            if (Mode == 1) {
                /* thoat nuoc tank 1*/
                FlowWater.reset();
                FlowWater.inputBackgroud("DrainageTank1.png");
                FlowWater.input(396, 360, 396, 445,
                        396, 445, 527, 445,
                        527, 445, 527, 550,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0);
            } else if (Mode == 2) {
                /* thoat nuoc tank 2*/
                FlowWater.reset();
                FlowWater.inputBackgroud("DrainageTank2.png");
                FlowWater.input(668, 360, 668, 445,
                        668, 445, 527, 445,
                        527, 445, 527, 550,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0);
            } else if (Mode == 3) {
                /* mo van SV2 */
                FlowWater.reset();
                FlowWater.inputBackgroud("on_sv2.PNG");
                FlowWater.input(75, 490, 75, 130,
                        75, 130, 215, 130,
                        215, 130, 215, 495,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0);
            } else if (Mode == 4) {
                /* xa nuoc kiem */
                FlowWater.reset();
                FlowWater.inputBackgroud("xaBazo.PNG");
                FlowWater.input(667, 360, 667, 554,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0);
            } else if (Mode == 5) {
                /* xa nuoc axit */
                FlowWater.reset();
                FlowWater.inputBackgroud("XaAxit.png");
                FlowWater.input(396, 360, 396, 554,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0,
                        0, 0, 0, 0);
            } else if (Mode == 6) {
                /* Dien phan */
                FlowWater.reset();
                FlowWater.inputBackgroud("DienPhan.png");
                FlowWater.input(75, 490, 75, 130,
                        80, 130, 673, 130,
                        446, 130, 446, 217,
                        673, 130, 673, 217,
                        898, 200, 898, 290,
                        890, 290, 832, 290);
            }
        }
    }
}
