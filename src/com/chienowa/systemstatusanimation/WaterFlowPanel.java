/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.systemstatusanimation;

import com.chienowa.customui.CvnSV;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.BitSet;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author minhg
 */
public class WaterFlowPanel extends JPanel implements ActionListener {

    public static final int PANEL_WIDTH = 1024;
    public static final int PANEL_HIGHT = 600;

//    private List<WaterMovingObj> listMovingObj;
    private Image backgroundImage;
    private Timer timer;
    private final int size = 6;
    private final int xVelocity = 1;
    private final int yVelocity = 1;
    private final int xSpace = 20;
    private final int ySpace = 20;
    private final int tubeSize = 10;
    private final int BTNWIDTH = 60;
    private final int BTNHEIGHT = 50;
    
    private JFrame frame = null;

    private CvnSV btnSV1;
    private CvnSV btnSV2;
    private CvnSV btnSV3;
    private CvnSV btnSV4;
    private CvnSV btnSV5;
    private CvnSV btnSV6;
    private CvnSV btnSV7;
    private CvnSV btnP1;
    private CvnSV btnP2;
    private CvnSV btnPSalt;
    private CvnSV btnCVCC_ON;

    private boolean vlSV1 = false;
    private boolean vlSV2 = false;
    private boolean vlSV3 = false;
    private boolean vlSV4 = false;
    private boolean vlSV5 = false;
    private boolean vlSV6 = false;
    private boolean vlSV7 = false;
    private boolean vlSV8 = false;
    private boolean vlSV9 = false;
    private boolean vlP1 = false;
    private boolean vlP2 = false;
    private boolean vlPSalt = false;
    private boolean vlCVCC_ON = false;

    private int x = 0;
    private int y = 0;

    private Graphics2D g2D;

    public WaterFlowPanel() {

        InitUI();

        backgroundImage = new ImageIcon("background_final.jpg").getImage();

        timer = new Timer(50, this);
        timer.start();
    }

    private void InitUI() {

        btnSV1 = new CvnSV("SV1");
        btnSV1.setBounds(265, 135 - BTNHEIGHT / 2, BTNWIDTH, BTNHEIGHT);
        btnSV1.SetNormalButton();
        btnSV1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV1 = !vlSV1;
                if (vlSV1) {
                    btnSV1.SetActiveButton();
                } else {
                    btnSV1.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV1);

        btnSV2 = new CvnSV("SV2");
        btnSV2.setBounds(207 - BTNWIDTH / 2, 420, BTNWIDTH, BTNHEIGHT);
        btnSV2.SetNormalButton();
        btnSV2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV2 = !vlSV2;
                if (vlSV2) {
                    btnSV2.SetActiveButton();
                } else {
                    btnSV2.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV2);

        btnP1 = new CvnSV("P1");
        btnP1.setBounds(388 - BTNWIDTH / 2, 380, BTNWIDTH, BTNHEIGHT);
        btnP1.SetNormalButton();
        btnP1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlP1 = !vlP1;
                if (vlP1) {
                    btnP1.SetActiveButton();
                } else {
                    btnP1.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnP1);

        btnSV3 = new CvnSV("SV3");
        btnSV3.setBounds(388 - BTNWIDTH / 2, 470, BTNWIDTH, BTNHEIGHT);
        btnSV3.SetNormalButton();
        btnSV3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV3 = !vlSV3;
                if (vlSV3) {
                    btnSV3.SetActiveButton();
                } else {
                    btnSV3.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV3);

        btnP2 = new CvnSV("P2");
        btnP2.setBounds(658 - BTNWIDTH / 2, 380, BTNWIDTH, BTNHEIGHT);
        btnP2.SetNormalButton();
        btnP2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlP2 = !vlP2;
                if (vlP2) {
                    btnP2.SetActiveButton();
                } else {
                    btnP2.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnP2);

        btnSV4 = new CvnSV("SV4");
        btnSV4.setBounds(658 - BTNWIDTH / 2, 470, BTNWIDTH, BTNHEIGHT);
        btnSV4.SetNormalButton();
        btnSV4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV4 = !vlSV4;
                if (vlSV4) {
                    btnSV4.SetActiveButton();
                } else {
                    btnSV4.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV4);

        btnSV5 = new CvnSV("SV5");
        btnSV5.setBounds(442, 444 - BTNHEIGHT / 2, BTNWIDTH, BTNHEIGHT);
        btnSV5.SetNormalButton();
        btnSV5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV5 = !vlSV5;
                if (vlSV5) {
                    btnSV5.SetActiveButton();
                } else {
                    btnSV5.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV5);

        btnSV6 = new CvnSV("SV6");
        btnSV6.setBounds(542, 444 - BTNHEIGHT / 2, BTNWIDTH, BTNHEIGHT);
        btnSV6.SetNormalButton();
        btnSV6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV6 = !vlSV6;
                if (vlSV6) {
                    btnSV6.SetActiveButton();
                } else {
                    btnSV6.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV6);

        btnSV7 = new CvnSV("SV7");
        btnSV7.setBounds(638, 135 - BTNHEIGHT / 2, BTNWIDTH, BTNHEIGHT);
        btnSV7.SetNormalButton();
        btnSV7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlSV7 = !vlSV7;
                if (vlSV7) {
                    btnSV7.SetActiveButton();
                } else {
                    btnSV7.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnSV7);

        btnPSalt = new CvnSV("PSalt");
        btnPSalt.setBounds(855, 214, 70, BTNHEIGHT);
        btnPSalt.SetNormalButton();
        btnPSalt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlPSalt = !vlPSalt;
                if (vlPSalt) {
                    btnPSalt.SetActiveButton();
                } else {
                    btnPSalt.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnPSalt);

        btnCVCC_ON = new CvnSV("CVCC_ON");
        btnCVCC_ON.setBounds(200, 530, 100, BTNHEIGHT);
        btnCVCC_ON.SetNormalButton();
        btnCVCC_ON.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vlCVCC_ON = !vlCVCC_ON;
                if (vlCVCC_ON) {
                    btnCVCC_ON.SetActiveButton();
                } else {
                    btnCVCC_ON.SetNormalButton();
                }
                SendDataToController();
            }
        });
        this.add(btnCVCC_ON);
    }

    private void PainComleteTube() {
        PaintYTube(71 - 5, 129, 375, false, Color.BLUE); // 1
//        PaintXTube(81 - 5, 129, 572, true, Color.BLUE); //2
        PaintXTube(81 - 5, 129, 180 + BTNWIDTH / 2, true, Color.BLUE); //2a
        if (vlSV1) {
            PaintXTube(81 - 5 + 180 + BTNWIDTH / 2, 129, 572 - (180 + BTNWIDTH / 2), true, Color.BLUE); //2b
            PaintYTube(443 - 5, 174, 45, true, Color.BLUE); // 4
            if (vlSV7) {
                PaintYTube(669 - 5, 94, 125 - 90, false, Color.BLUE); //8a
                PaintXTube(669 - 5, 94, 202, true, Color.BLUE); //10
                PaintYTube(862 - 5, 94, 45, true, Color.BLUE); //11
            } else {
                PaintYTube(669 - 5, 94 + 35, 125 - 35, true, Color.BLUE); //8b
            }
        }
        if (vlSV2) {
            PaintYTube(212 - 5, 139, 365, true, Color.BLUE); //3
        }
        if (vlP1) {
            if (vlSV3) {
                PaintYTube(393 - 5, 360, 195, true, Color.RED); //5
            }
            if (vlSV5) {
                PaintYTube(393 - 5, 360, 94, true, Color.RED); //5a
                PaintXTube(403 - 5, 444, 120, true, Color.RED); //6a
                if (vlP2 && vlSV6) {
                    PaintYTube(523 - 5, 444, 112, true, Color.GRAY); //7
                } else {
                    PaintYTube(523 - 5, 444, 112, true, Color.RED); //7
                }
            }

        }
        if (vlP2) {
            if (vlSV4) {
                PaintYTube(663 - 5, 360, 195, true, Color.BLUE); //9

            }
            if (vlSV6) {
                PaintYTube(663 - 5, 360, 94, true, Color.BLUE); //9a
                PaintXTube(528, 444, 131, false, Color.BLUE); //6b
                if (!vlP1 || (vlP1 && vlSV3 && !vlSV5) || (vlP1 && !vlSV3 && !vlSV5)) {
                    PaintYTube(523 - 5, 444, 112, true, Color.BLUE); //7
                }
            }
        }

//        PaintYTube(895 - 5, 200, 74, true, Color.BLUE); //12
//        PaintXTube(831 - 5, 289, 50, true, Color.BLUE); //13
//        PaintXTube(919 - 5, 289, 48, true, Color.BLUE); //14
//        PaintYTube(957 - 5, 289, 61, true, Color.BLUE); //15
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g2D = (Graphics2D) g;
        g2D.drawImage(backgroundImage, 0, 0, null);

        PainComleteTube();

    }

    private void PaintXTube(int x, int y, int length, boolean direction, Color color) {
        g2D.setPaint(color);
        g2D.fillRect(x, y, length, tubeSize);
        g2D.setPaint(Color.WHITE);
        PaintXLine(x, y, length, direction);

    }

    private void PaintYTube(int x, int y, int length, boolean direction, Color color) {
        g2D.setPaint(color);
        g2D.fillRect(x, y, tubeSize, length);
        g2D.setPaint(Color.WHITE);
        PaintYLine(x, y, length, direction);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        x = x + xVelocity;
        if (x >= xSpace) {
            x = 0;
        }

        y = y + yVelocity;
        if (y >= ySpace) {
            y = 0;
        }
        repaint();
    }

    private void PaintXLine(int xPo, int yPo, int length, boolean direction) {
        int directV;
        int yR = yPo + (tubeSize - size) / 2;
        if (direction) {
            directV = 1;
        } else {
            directV = -1;
        }
        int numRun = length / xSpace;
        for (int i = 0; i < numRun; i++) {
            int xR;
            if (direction) {
                xR = (x * directV + xSpace * i) + xPo;
            } else {
                xR = (x * directV + xSpace * i) + xPo + xSpace;
            }

            g2D.fillOval(xR, yR, size, size);
        }
    }

    private void PaintYLine(int xPo, int yPo, int length, boolean direction) {
        int directV;
        int xR = xPo + (tubeSize - size) / 2;
        if (direction) {
            directV = 1;
        } else {
            directV = -1;
        }
        int numRun = length / ySpace;
        for (int i = 0; i < numRun; i++) {
            int yR;
            if (direction) {
                yR = (y * directV + ySpace * i) + yPo;
            } else {
                yR = (y * directV + ySpace * i) + yPo + ySpace;
            }
            g2D.fillOval(xR, yR, size, size);
        }
    }

    private void SendDataToController() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TEST_INDIVIDUAL");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            BitSet bitset = new BitSet(25);
            bitset.set(0, vlSV1);
            bitset.set(1, vlSV2);
            bitset.set(2, vlSV3);
            bitset.set(3, vlSV4);
            bitset.set(4, vlSV5);
            bitset.set(5, vlSV6);
            bitset.set(6, vlSV7);
            bitset.set(7, vlSV8);
            bitset.set(8, vlSV9);
            bitset.set(9, false);
            bitset.set(10, false);
            bitset.set(11, false);
            bitset.set(12, false);
            bitset.set(13, false);
            bitset.set(14, false);
            bitset.set(15, false);
            bitset.set(16, vlP1);
            bitset.set(17, vlP2);
            bitset.set(18, vlPSalt);
            bitset.set(19, vlCVCC_ON);
            bitset.set(20, false);
            bitset.set(21, false);
            bitset.set(22, false);
            bitset.set(23, false);
            bitset.set(24, true);
            bitset.set(25, true);
            bitset.set(26, true);
            bitset.set(27, true);

            byte[] sendArray = bitset.toByteArray();

            SerialUartProcess.SendByteArray(sendArray);

//            BitSet bitsetS = BitSet.valueOf(sendArray);
//            for (int i = 0; i < bitsetS.length(); i++) {
//                System.out.print("| Value " + i + ":" + bitset.get(i));
//            }

        } else {
            System.out.println("Cannot get respond from Controller");
        }

    }

}
