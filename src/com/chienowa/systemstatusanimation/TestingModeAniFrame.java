/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.systemstatusanimation;

import com.chienowa.cvnpalmsensor.ManagementMenuFrame;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.UartBufferS;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author minhg
 */
public class TestingModeAniFrame extends JFrame implements ActionListener {

    private javax.swing.JPanel WaterSofnerPanel;
    private javax.swing.JPanel FilterPanel;
    private javax.swing.JPanel RS1Panel;
    private javax.swing.JLabel textRS1;
    private javax.swing.JLabel valueRS1;
    private javax.swing.JLabel unitRS1;

    private javax.swing.JPanel ElectrolyzePanel;
    private javax.swing.JLabel unitI;
    private javax.swing.JLabel unitV;
    private javax.swing.JLabel valueI;
    private javax.swing.JLabel valueV;
    private javax.swing.JLabel textElectrolyze;
    private javax.swing.JLabel textElectrolyzeTime;
    private javax.swing.JLabel textElectrolyzeTimeValue;
    private javax.swing.JLabel textDrainageModeTime;
    private javax.swing.JLabel textDrainageModeTimeValue;
    private javax.swing.JLabel textTotalRunTimeValue;
    private javax.swing.JLabel textTotalRunTime;
    private javax.swing.JLabel textDate;
    private javax.swing.JLabel textTime;

    private JButton btnMode;

    private javax.swing.JPanel TitlePanel;
    private javax.swing.JLabel textTitle;
    private javax.swing.JPanel Tank1Panel;
    private javax.swing.JLabel Tank1Icon;
    private javax.swing.JPanel Tank2Panel;
    private javax.swing.JLabel Tank2Icon;
    private javax.swing.JPanel SlatTankPanel;
    private javax.swing.JLabel SlatTankIcon;

    private javax.swing.JPanel TotalRunPanel;
    private javax.swing.JPanel TotalElectrolyzeRunPanel;
    private javax.swing.JPanel DrainageModeRunPanel;
    private javax.swing.JPanel DateAndTimePanel;
    private static final String TIME_FORMATTER = "HH:mm:ss";
    private static final String DATE_FORMATTER = "dd/MM/yyyy";

    private String RunTime;
    private String ElectrolyedTime;
    private String DraingeTime;
    private String RS1_value;
    private String V_Electrolyze;
    private String I_Electrolyze;
    private int Tank1Level;
    private int Tank2Level;
    private int SlatTankLevel;
    private WaterFlowPanel waterFlowPanel;
    
    private final ManagementMenuFrame mainframe;

    public TestingModeAniFrame(ManagementMenuFrame frame) throws HeadlessException {
//
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1024, 600);
        this.setUndecorated(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        
        mainframe = frame;

        InitUI();
        StartTestingMode();

    }

    private void InitUI() {

        int moveX = -7;
        int moveY = 0;

        Timer timerTimeandDate = new Timer(1000, this);
        timerTimeandDate.start();
        waterFlowPanel = new WaterFlowPanel();
        waterFlowPanel.setBounds(0, 0, 1024, 600);
        waterFlowPanel.setLayout(null);
        this.add(waterFlowPanel);

        WaterSofnerPanel = new javax.swing.JPanel();
        WaterSofnerPanel.setLayout(null);
        WaterSofnerPanel.setVisible(true);
        WaterSofnerPanel.setBounds(17 + moveX, 248 + moveY, 125, 55);
        WaterSofnerPanel.setBackground(new java.awt.Color(128, 128, 192));
        WaterSofnerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        JLabel textWaterSofner = new javax.swing.JLabel();
        WaterSofnerPanel.add(textWaterSofner);
        textWaterSofner.setText("Water Sofner");
        textWaterSofner.setBounds(8, 18, 125, 20);
        textWaterSofner.setFont(new java.awt.Font("Unispace", 1, 14));

        waterFlowPanel.add(WaterSofnerPanel);

        /**
         * ******** Filter Panel **********
         */
        FilterPanel = new javax.swing.JPanel();
        waterFlowPanel.add(FilterPanel);
        FilterPanel.setLayout(null);
        FilterPanel.setBounds(17 + moveX, 330 + moveY, 125, 55);
        FilterPanel.setBackground(new java.awt.Color(128, 128, 192));
        FilterPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        JLabel textFilter = new javax.swing.JLabel();
        FilterPanel.add(textFilter);
        textFilter.setBounds(35, 18, 125, 20);
        textFilter.setText("Filter");
        textFilter.setFont(new java.awt.Font("Unispace", 1, 14));

        /**
         * ******** Title **********
         */
        TitlePanel = new javax.swing.JPanel();
        waterFlowPanel.add(TitlePanel);
        TitlePanel.setLayout(null);
        TitlePanel.setBounds(362 + moveX, 16 + moveY, 300, 60);
        TitlePanel.setBackground(new java.awt.Color(255, 255, 255));
        TitlePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textTitle = new javax.swing.JLabel();
        TitlePanel.add(textTitle);
        textTitle.setBounds(90, 0, 300, 60);
        textTitle.setText("TEST MODE");
        textTitle.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * **** Tank 1 ******
         */
        Tank1Panel = new javax.swing.JPanel();
        waterFlowPanel.add(Tank1Panel);
        Tank1Panel.setLayout(null);
        Tank1Panel.setBounds(372 + moveX, 218 + moveY, 100, 142);
        Tank1Icon = new javax.swing.JLabel();
        Tank1Panel.add(Tank1Icon);
        Tank1Icon.setIcon(new ImageIcon("Tank1N.png"));
        Tank1Icon.setBounds(0, 0, 100, 142);

        /**
         * **** Tank 2 ******
         */
        Tank2Panel = new javax.swing.JPanel();
        waterFlowPanel.add(Tank2Panel);
        Tank2Panel.setLayout(null);
        Tank2Panel.setBounds(593 + moveX, 218 + moveY, 100, 142);
        Tank2Icon = new javax.swing.JLabel();
        Tank2Panel.add(Tank2Icon);
        Tank2Icon.setIcon(new ImageIcon("Tank2N.png"));
        Tank2Icon.setBounds(0, 0, 100, 142);

        /**
         * **** Slat Tank ******
         */
        SlatTankPanel = new javax.swing.JPanel();
        waterFlowPanel.add(SlatTankPanel);
        SlatTankPanel.setLayout(null);
        SlatTankPanel.setBounds(855 + moveX, 135 + moveY, 100, 70);
        SlatTankIcon = new javax.swing.JLabel();
        SlatTankPanel.add(SlatTankIcon);
        SlatTankIcon.setIcon(new ImageIcon("SaltWaterN.png"));
        SlatTankIcon.setBounds(0, 0, 100, 70);

        /**
         * ******** RS1 Panel **********
         */
        RS1Panel = new javax.swing.JPanel();
        waterFlowPanel.add(RS1Panel);
        RS1Panel.setLayout(null);
        RS1Panel.setBounds(17 + moveX, 164 + moveY, 125, 60);
        RS1Panel.setBackground(new java.awt.Color(128, 128, 192));
        RS1Panel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textRS1 = new javax.swing.JLabel();
        RS1Panel.add(textRS1);
        textRS1.setBounds(50, 5, 125, 20);
        textRS1.setText("RS1");
        textRS1.setFont(new java.awt.Font("Unispace", 1, 14));

        unitRS1 = new javax.swing.JLabel();
        RS1Panel.add(unitRS1);
        unitRS1.setBounds(85, 30, 40, 20);
        unitRS1.setText("L/M");
        unitRS1.setFont(new java.awt.Font("Unispace", 1, 16));

        valueRS1 = new javax.swing.JLabel();
        RS1Panel.add(valueRS1);
        valueRS1.setBounds(15, 30, 50, 20);
        valueRS1.setText(RS1_value);
        valueRS1.setFont(new java.awt.Font("Unispace", 1, 16));

        /**
         * ******* Total Run Time **********
         */
        TotalRunPanel = new javax.swing.JPanel();
        waterFlowPanel.add(TotalRunPanel);
        TotalRunPanel.setLayout(null);
        TotalRunPanel.setBounds(894 + moveX, 404 + moveY, 120, 50);
        TotalRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        TotalRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textTotalRunTime = new javax.swing.JLabel();
        TotalRunPanel.add(textTotalRunTime);
        textTotalRunTime.setBounds(15, 18, 125, 20);
        textTotalRunTime.setText(RunTime);
        textTotalRunTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textTotalRunTimeValue = new javax.swing.JLabel();
        TotalRunPanel.add(textTotalRunTimeValue);
        textTotalRunTimeValue.setBounds(90, 18, 125, 20);
        textTotalRunTimeValue.setText("H");
        textTotalRunTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * ******* Total Electrolyze Run Time *********
         */
        TotalElectrolyzeRunPanel = new javax.swing.JPanel();
        waterFlowPanel.add(TotalElectrolyzeRunPanel);
        TotalElectrolyzeRunPanel.setLayout(null);
        TotalElectrolyzeRunPanel.setBounds(894 + moveX, 462 + moveY, 120, 52);
        TotalElectrolyzeRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        TotalElectrolyzeRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textElectrolyzeTime = new javax.swing.JLabel();
        TotalElectrolyzeRunPanel.add(textElectrolyzeTime);
        textElectrolyzeTime.setBounds(15, 18, 125, 20);
        textElectrolyzeTime.setText(ElectrolyedTime);
        textElectrolyzeTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textElectrolyzeTimeValue = new javax.swing.JLabel();
        TotalElectrolyzeRunPanel.add(textElectrolyzeTimeValue);
        textElectrolyzeTimeValue.setBounds(90, 18, 125, 20);
        textElectrolyzeTimeValue.setText("H");
        textElectrolyzeTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * **** Total Drainage Mode Run Time ******
         */
        DrainageModeRunPanel = new javax.swing.JPanel();
        waterFlowPanel.add(DrainageModeRunPanel);
        DrainageModeRunPanel.setLayout(null);
        DrainageModeRunPanel.setBounds(894 + moveX, 524 + moveY, 120, 50);
        DrainageModeRunPanel.setBackground(new java.awt.Color(128, 128, 192));
        DrainageModeRunPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textDrainageModeTime = new javax.swing.JLabel();
        DrainageModeRunPanel.add(textDrainageModeTime);
        textDrainageModeTime.setBounds(15, 18, 125, 20);
        textDrainageModeTime.setText(DraingeTime);
        textDrainageModeTime.setFont(new java.awt.Font("Unispace", 1, 20));

        textDrainageModeTimeValue = new javax.swing.JLabel();
        DrainageModeRunPanel.add(textDrainageModeTimeValue);
        textDrainageModeTimeValue.setBounds(90, 18, 125, 20);
        textDrainageModeTimeValue.setText("H");
        textDrainageModeTimeValue.setFont(new java.awt.Font("Unispace", 1, 20));

        /**
         * ******* Date And Time Panel *********
         */
        DateAndTimePanel = new javax.swing.JPanel();
        waterFlowPanel.add(DateAndTimePanel);
        DateAndTimePanel.setLayout(null);
        DateAndTimePanel.setBounds(850 + moveX, 15 + moveY, 165, 60);
        DateAndTimePanel.setBackground(new java.awt.Color(184, 230, 254));
        DateAndTimePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        textTime = new javax.swing.JLabel();
        DateAndTimePanel.add(textTime);
        textTime.setBounds(45, 5, 125, 20);
        textTime.setText("14 : 47");
        textTime.setFont(new java.awt.Font("Unispace", 1, 16));

        textDate = new javax.swing.JLabel();
        DateAndTimePanel.add(textDate);
        textDate.setBounds(35, 33, 125, 20);
        textDate.setText("11 / 08 / 2021");
        textDate.setFont(new java.awt.Font("Unispace", 1, 16));

        /**
         * ******* Electrolyze Panel ***********
         */
        ElectrolyzePanel = new javax.swing.JPanel();
        waterFlowPanel.add(ElectrolyzePanel);
        ElectrolyzePanel.setLayout(null);
        ElectrolyzePanel.setBounds(435 + moveX, 93 + moveY, 190, 82);
        ElectrolyzePanel.setBackground(new java.awt.Color(128, 128, 192));
        ElectrolyzePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textElectrolyze = new javax.swing.JLabel();
        ElectrolyzePanel.add(textElectrolyze);
        textElectrolyze.setBounds(50, 5, 125, 20);
        textElectrolyze.setText("Electrolyze");
        textElectrolyze.setFont(new java.awt.Font("Unispace", 1, 14));

        unitV = new javax.swing.JLabel();
        ElectrolyzePanel.add(unitV);
        unitV.setBounds(140, 30, 20, 20);
        unitV.setText("V");
        unitV.setFont(new java.awt.Font("Unispace", 1, 16));

        unitI = new javax.swing.JLabel();
        ElectrolyzePanel.add(unitI);
        unitI.setBounds(140, 55, 20, 20);
        unitI.setText("A");
        unitI.setFont(new java.awt.Font("Unispace", 1, 16));

        valueV = new javax.swing.JLabel();
        ElectrolyzePanel.add(valueV);
        valueV.setBounds(40, 30, 80, 20);
        valueV.setText(V_Electrolyze);
        valueV.setFont(new java.awt.Font("Unispace", 1, 16));

        valueI = new javax.swing.JLabel();
        ElectrolyzePanel.add(valueI);
        valueI.setBounds(40, 55, 80, 20);
        valueI.setText(I_Electrolyze);
        valueI.setFont(new java.awt.Font("Unispace", 1, 16));

        btnMode = new JButton();
        waterFlowPanel.add(btnMode);
        btnMode.setText("BACK");
        btnMode.setBounds(12, 530, 150, 57);
        btnMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               ReturnMainFrame();
            }
        });

    }

    private void RunAnimation() {
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern(TIME_FORMATTER);
        String formatTime = localTime.format(stringTime_formatter);
        textTime.setText(formatTime);

        /**
         * ** Get Date ***
         */
        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter stringDate_formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDate = localDate.format(stringDate_formatter);
        textDate.setText(formatDate);

        /**
         * Thong So *
         */
        textDrainageModeTime.setText(DraingeTime);
        textElectrolyzeTime.setText(ElectrolyedTime);
        textTotalRunTime.setText(RunTime);
        valueI.setText(I_Electrolyze);
        valueV.setText(V_Electrolyze);
        valueRS1.setText(RS1_value);

        /* tank 1 & tank 2 */
        switch (Tank1Level) {
            case 0:
                Tank1Icon.setIcon(new ImageIcon("Tank1N.png"));
                break;
            case 1:
                Tank1Icon.setIcon(new ImageIcon("Tank1L.png"));
                break;
            case 2:
                Tank1Icon.setIcon(new ImageIcon("Tank1M.png"));
                break;
            case 3:
                Tank1Icon.setIcon(new ImageIcon("Tank1H.png"));
                break;
            default:
                break;
        }

        switch (Tank2Level) {
            case 0:
                Tank2Icon.setIcon(new ImageIcon("Tank2N.png"));
                break;
            case 1:
                Tank2Icon.setIcon(new ImageIcon("Tank2L.png"));
                break;
            case 2:
                Tank2Icon.setIcon(new ImageIcon("Tank2M.png"));
                break;
            case 3:
                Tank2Icon.setIcon(new ImageIcon("Tank2H.png"));
                break;
            default:
                break;
        }

        switch (SlatTankLevel) {
            case 0:
                SlatTankIcon.setIcon(new ImageIcon("SaltWaterN.png"));
                break;
            case 1:
                SlatTankIcon.setIcon(new ImageIcon("SaltWaterL.png"));
                break;
            case 2:
                SlatTankIcon.setIcon(new ImageIcon("SaltWaterH.png"));
                break;
            default:
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RunAnimation();
    }

    public void ReturnMainFrame() {
        StopTestingMode();
        this.dispose();
        this.setVisible(false);
        mainframe.setVisible(true);
        mainframe.StartBackgroundRunning();
    }

    private void StartTestingMode() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TESTING_MODE_START");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            System.out.println("Enter testing mode");
        } else {
            System.out.println("Problem when exit testing mode");
            System.exit(0);
        }
    }

    private void StopTestingMode() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TESTING_MODE_STOP");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            System.out.println("Exit testing mode");
        } else {
            System.out.println("Problem when enter testing mode");
            System.exit(0);
        }
    }

}
