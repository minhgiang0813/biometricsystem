/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.customui.CvnTextField;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.UartBufferS;
import com.chienowa.ulti.VirtualKeyboard;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author minhg
 */
public class TimeSettingFrame extends CvnFrame implements ActionListener {

    public static short processStep;

    private final ThreadTimeSettingFrame t1;
    CvnButton returnButton = null;
    CvnButton saveButton = null;
    CvnButton quitButton = null;

    private final ManagementMenuFrame mainFrame;
    private final int TXTFIELD_WIDTH = 50;
    private final int TXTFIELD_HEIGHT = 30;
    private final int CX1 = 12;
    private final int CX2 = 412;
    private final int CX3 = 500;
    private final int CX4 = 900;
    private final int CY1 = 80;
    private final int CYS = 40;
    
    public ThreadAlarm alarmSystemManagement;
    private ClockLabel clockLabel = null;

    public CvnTextField input01 = null;
    public CvnTextField input02 = null;
    public CvnTextField input03 = null;
    public CvnTextField input04 = null;
    public CvnTextField input05 = null;
    public CvnTextField input06 = null;
    public CvnTextField input07 = null;
    public CvnTextField input08 = null;
    public CvnTextField input09 = null;
    public CvnTextField input10 = null;
    public CvnTextField input11 = null;
    public CvnTextField input12 = null;
    public CvnTextField input13 = null;
    public CvnTextField input14 = null;
    public CvnTextField input15 = null;
    public CvnTextField input16 = null;
    public CvnTextField input17 = null;
    public CvnTextField input18 = null;
    public CvnTextField input19 = null;
    public CvnTextField input20 = null;
    public CvnTextField input21 = null;
    public CvnTextField input22 = null;
    public CvnTextField input23 = null;
    public CvnTextField input24 = null;
    public CvnTextField input25 = null;
    public CvnTextField input26 = null;
    public CvnTextField input27 = null;
    public CvnTextField input28 = null;
    public CvnTextField input29 = null;
    public CvnTextField input30 = null;
    public CvnTextField input31 = null;
    public CvnTextField input32 = null;
    public CvnTextField input33 = null;
    public CvnTextField input34 = null;
    public CvnTextField input35 = null;
    public CvnTextField input36 = null;
    public CvnTextField input37 = null;



    private JTabbedPane tabbedPanel;
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();

    public TimeSettingFrame(ManagementMenuFrame frame) throws InterruptedException {

        processStep = 0;

        mainFrame = frame;

        tabbedPanel = new JTabbedPane();

        panel3.add(new JLabel("Panel 3"));
        panel4.add(new JLabel("Panel 4"));

        tabbedPanel.addTab("Panel 1", panel1);
        panel1.setLayout(null);
        tabbedPanel.addTab("Panel 2", panel2);
        panel2.setLayout(null);
        tabbedPanel.addTab("Panel 3", panel3);
        panel3.setLayout(null);
        tabbedPanel.addTab("Panel 4", panel4);
        panel4.setLayout(null);

        JPanel mainPanel = new JPanel();
        mainPanel.setBounds(10, 50, 1004, 470);
        getContentPane().add(mainPanel);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(tabbedPanel, BorderLayout.CENTER);

        txtCaption.setText("Time Setting");

        returnButton = new CvnButton();
        returnButton.setText("Return");
        returnButton.setBounds(12, 530, 200, 57);
        getContentPane().add(returnButton);
        returnButton.addActionListener(this);

        saveButton = new CvnButton();
        saveButton.setText("Save");
        saveButton.setBounds(250, 530, 200, 57);
        getContentPane().add(saveButton);
        saveButton.addActionListener(this);

        quitButton = new CvnButton();
        quitButton.setText("Quit");
        quitButton.setBounds(488, 530, 200, 57);
        getContentPane().add(quitButton);
        quitButton.addActionListener(this);
        
        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);
        

        Panel1Render();
        Panel2Render();
        Panel3Render();
        Panel4Render();
        VirtualKeyboard.Show();

//        //Start thread for this frame
        t1 = new ThreadTimeSettingFrame(this);
        t1.start();
    }

    private void Panel1Render() {
        CvnLabel label01 = new CvnLabel("[1] Initial Water Drainage Operation (s)");
        label01.setBounds(CX1, CY1, 350, 35);
        panel1.add(label01);

        input01 = new CvnTextField();
        input01.setBounds(CX2, CY1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input01);

        CvnLabel label02 = new CvnLabel("[2] Flow Sensor Start Time (s)");
        label02.setBounds(CX1, CY1 + CYS * 1, 350, 35);
        panel1.add(label02);

        input02 = new CvnTextField();
        input02.setBounds(CX2, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input02);
//
        CvnLabel label03 = new CvnLabel("[3] Flow Sensor Monitor Time (s)");
        label03.setBounds(CX1, CY1 + CYS * 2, 350, 35);
        panel1.add(label03);

        input03 = new CvnTextField();
        input03.setBounds(CX2, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input03);
//
        CvnLabel label04 = new CvnLabel("[4] Electrolysis Operation Start (s)");
        label04.setBounds(CX1, CY1 + CYS * 3, 350, 35);
        panel1.add(label04);

        input04 = new CvnTextField();
        input04.setBounds(CX2, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input04);

        CvnLabel label05 = new CvnLabel("[5] Electrolysis Stop Delay (s)");
        label05.setBounds(CX1, CY1 + CYS * 4, 350, 35);
        panel1.add(label05);

        input05 = new CvnTextField();
        input05.setBounds(CX2, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input05);

        CvnLabel label06 = new CvnLabel("[6] Drainage Off Time (h)");
        label06.setBounds(CX3, CY1 + CYS * 0, 350, 35);
        panel1.add(label06);

        input06 = new CvnTextField();
        input06.setBounds(CX4, CY1 + CYS * 0, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input06);

        CvnLabel label07 = new CvnLabel("[7] Power On Preparation (s)");
        label07.setBounds(CX3, CY1 + CYS * 1, 350, 35);
        panel1.add(label07);

        input07 = new CvnTextField();
        input07.setBounds(CX4, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input07);

        CvnLabel label08 = new CvnLabel("[8] Flow Rate Adjustment Release (s)");
        label08.setBounds(CX3, CY1 + CYS * 2, 350, 35);
        panel1.add(label08);

        input08 = new CvnTextField();
        input08.setBounds(CX4, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input08);

        CvnLabel label09 = new CvnLabel("[9] Current Adjustment Release (s)");
        label09.setBounds(CX3, CY1 + CYS * 3, 350, 35);
        panel1.add(label09);

        input09 = new CvnTextField();
        input09.setBounds(CX4, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input09);

        CvnLabel label10 = new CvnLabel("[10] Electrolysis Current Alarm Specified (s)");
        label10.setBounds(CX3, CY1 + CYS * 4, 350, 35);
        panel1.add(label10);

        input10 = new CvnTextField();
        input10.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input10);
    }

    private void Panel2Render() {
        CvnLabel label11 = new CvnLabel("[11] Over Voltage 1 Time (s)");
        label11.setBounds(CX1, CY1, 350, 35);
        panel2.add(label11);

        input11 = new CvnTextField();
        input11.setBounds(CX2, CY1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input11);

        CvnLabel label12 = new CvnLabel("[12] Over Voltage 2 Time (s)");
        label12.setBounds(CX1, CY1 + CYS * 1, 350, 35);
        panel2.add(label12);

        input12 = new CvnTextField();
        input12.setBounds(CX2, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input12);

        CvnLabel label13 = new CvnLabel("[13] Over Voltage 3 Time (s)");
        label13.setBounds(CX1, CY1 + CYS * 2, 350, 35);
        panel2.add(label13);

        input13 = new CvnTextField();
        input13.setBounds(CX2, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input13);

        CvnLabel label14 = new CvnLabel("[14] Low Voltage Start Time (s)");
        label14.setBounds(CX1, CY1 + CYS * 3, 350, 35);
        panel2.add(label14);

        input14 = new CvnTextField();
        input14.setBounds(CX2, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input14);

        CvnLabel label15 = new CvnLabel("[15] Low Voltage Delay Time (s)");
        label15.setBounds(CX1, CY1 + CYS * 4, 350, 35);
        panel2.add(label15);

        input15 = new CvnTextField();
        input15.setBounds(CX2, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input15);

        CvnLabel label16 = new CvnLabel("[16] Current Monitoring Start (s)");
        label16.setBounds(CX3, CY1 + CYS * 0, 350, 35);
        panel2.add(label16);

        input16 = new CvnTextField();
        input16.setBounds(CX4, CY1 + CYS * 0, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input16);

        CvnLabel label17 = new CvnLabel("[17] Solenoid Leakage Start Time (s)");
        label17.setBounds(CX3, CY1 + CYS * 1, 350, 35);
        panel2.add(label17);

        input17 = new CvnTextField();
        input17.setBounds(CX4, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input17);

        CvnLabel label18 = new CvnLabel("[18] Full Water Monitoring Start (h)");
        label18.setBounds(CX3, CY1 + CYS * 2, 350, 35);
        panel2.add(label18);

        input18 = new CvnTextField();
        input18.setBounds(CX4, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input18);

        CvnLabel label19 = new CvnLabel("[19] Water Filter Alarm (h)");
        label19.setBounds(CX3, CY1 + CYS * 3, 350, 35);
        panel2.add(label19);

        input19 = new CvnTextField();
        input19.setBounds(CX4, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input19);

        CvnLabel label20 = new CvnLabel("[20] Water Filter Alarm Ignore (h)");
        label20.setBounds(CX3, CY1 + CYS * 4, 350, 35);
        panel2.add(label20);

        input20 = new CvnTextField();
        input20.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel2.add(input20);
    }

    private void Panel3Render() {
        CvnLabel label21 = new CvnLabel("[26] On Delay Empty Level (s)");
        label21.setBounds(CX1, CY1, 350, 35);
        panel3.add(label21);

        input21 = new CvnTextField();
        input21.setBounds(CX2, CY1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input21);

        CvnLabel label22 = new CvnLabel("[27] On Delay Low Level (s)");
        label22.setBounds(CX1, CY1 + CYS * 1, 350, 35);
        panel3.add(label22);

        input22 = new CvnTextField();
        input22.setBounds(CX2, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input22);

        CvnLabel label23 = new CvnLabel("[28] On Delay High Level (s)");
        label23.setBounds(CX1, CY1 + CYS * 2, 350, 35);
        panel3.add(label23);

        input23 = new CvnTextField();
        input23.setBounds(CX2, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input23);

        CvnLabel label24 = new CvnLabel("[30] Off Delay Empty Level (s)");
        label24.setBounds(CX1, CY1 + CYS * 3, 350, 35);
        panel3.add(label24);

        input24 = new CvnTextField();
        input24.setBounds(CX2, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input24);

        CvnLabel label25 = new CvnLabel("[31] Salt Low Level Delay (s)");
        label25.setBounds(CX1, CY1 + CYS * 4, 350, 35);
        panel3.add(label25);

        input25 = new CvnTextField();
        input25.setBounds(CX2, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input25);

        CvnLabel label26 = new CvnLabel("[32] Salt High Level Delay (s)");
        label26.setBounds(CX3, CY1 + CYS * 0, 350, 35);
        panel3.add(label26);

        input26 = new CvnTextField();
        input26.setBounds(CX4, CY1 + CYS * 0, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input26);

        CvnLabel label27 = new CvnLabel("[51] Alkaline Water Spouting Time (s)");
        label27.setBounds(CX3, CY1 + CYS * 1, 350, 35);
        panel3.add(label27);

        input27 = new CvnTextField();
        input27.setBounds(CX4, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input27);

        CvnLabel label28 = new CvnLabel("[52] Acid Water Spouting Time (s)");
        label28.setBounds(CX3, CY1 + CYS * 2, 350, 35);
        panel3.add(label28);

        input28 = new CvnTextField();
        input28.setBounds(CX4, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input28);

        CvnLabel label29 = new CvnLabel("[53] Washing Water Spouting Time (s)");
        label29.setBounds(CX3, CY1 + CYS * 3, 350, 35);
        panel3.add(label29);

        input29 = new CvnTextField();
        input29.setBounds(CX4, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input29);

        CvnLabel label30 = new CvnLabel("[54] Over Lap Time (ms)");
        label30.setBounds(CX3, CY1 + CYS * 4, 350, 35);
        panel3.add(label30);

        input30 = new CvnTextField();
        input30.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel3.add(input30);
    }

    private void Panel4Render() {
        CvnLabel label31 = new CvnLabel("[55] Water Discharge Delay (s)");
        label31.setBounds(CX1, CY1, 350, 35);
        panel4.add(label31);

        input31 = new CvnTextField();
        input31.setBounds(CX2, CY1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input31);

        CvnLabel label32 = new CvnLabel("[56] Acid Water Down Time (s)");
        label32.setBounds(CX1, CY1 + CYS * 1, 350, 35);
        panel4.add(label32);

        input32 = new CvnTextField();
        input32.setBounds(CX2, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input32);

        CvnLabel label33 = new CvnLabel("[59] Alkaline Water Down Time (s)");
        label33.setBounds(CX1, CY1 + CYS * 2, 350, 35);
        panel4.add(label33);

        input33 = new CvnTextField();
        input33.setBounds(CX2, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input33);

        CvnLabel label34 = new CvnLabel("[61] Curran Cleaning IntervalTime (h)");
        label34.setBounds(CX1, CY1 + CYS * 3, 350, 35);
        panel4.add(label34);

        input34 = new CvnTextField();
        input34.setBounds(CX2, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input34);

        CvnLabel label35 = new CvnLabel("[62] Callan Wash Spouting Time (s)");
        label35.setBounds(CX1, CY1 + CYS * 4, 350, 35);
        panel4.add(label35);

        input35 = new CvnTextField();
        input35.setBounds(CX2, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input35);

        CvnLabel label36 = new CvnLabel("[63] Neutralization solenoid valve opening start time (h)");
        label36.setBounds(CX3, CY1 + CYS * 0, 350, 35);
        panel4.add(label36);

        input36 = new CvnTextField();
        input36.setBounds(CX4, CY1 + CYS * 0, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input36);

        CvnLabel label37 = new CvnLabel("[64] Neutralization solenoid valve open time (s)");
        label37.setBounds(CX3, CY1 + CYS * 1, 350, 35);
        panel4.add(label37);

        input37 = new CvnTextField();
        input37.setBounds(CX4, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel4.add(input37);

//        CvnLabel label38 = new CvnLabel("[18] ");
//        label38.setBounds(CX3, CY1 + CYS * 2, 350, 35);
//        panel4.add(label38);
//
//        input38 = new CvnTextField();
//        input38.setBounds(CX4, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
//        panel4.add(input38);
//
//        CvnLabel label39 = new CvnLabel("[19]");
//        label39.setBounds(CX3, CY1 + CYS * 3, 350, 35);
//        panel4.add(label39);
//
//        input39 = new CvnTextField();
//        input39.setBounds(CX4, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
//        panel4.add(input39);
//        
//        CvnLabel label40 = new CvnLabel("[10] 10");
//        label40.setBounds(CX3, CY1 + CYS * 4, 350, 35);
//        panel4.add(label40);
//
//        input40 = new CvnTextField();
//        input40.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
//        panel4.add(input40);
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(returnButton)) {
            ReturnMainFrame();
        }
        if (obj.equals(saveButton)) {
            if (CheckValidation()) {
                SaveSpec();
            }
        }
        if (obj.equals(quitButton)) {
            QuitApp();
        }
    }
    
    public void StartBackgroundRunning() {
        alarmSystemManagement = new ThreadAlarm(this);
        alarmSystemManagement.start();
        clockLabel.timerTimeandDate.start();
    }
    
    public void StopBackgroundRunning() {
        alarmSystemManagement.stop();
        clockLabel.timerTimeandDate.stop();
    }

    public void ReturnMainFrame() {
        VirtualKeyboard.Hide();
        StopBackgroundRunning();
        this.dispose();
        this.setVisible(false);
        mainFrame.setVisible(true);
        mainFrame.StartBackgroundRunning();

    }

    public void QuitApp() {
        System.exit(0);
    }

    private boolean CheckValidation() {

        String inputValue01 = input01.getText();
        String inputValue02 = input02.getText();
        String inputValue03 = input03.getText();
        String inputValue04 = input04.getText();
        String inputValue05 = input05.getText();
        String inputValue06 = input06.getText();
        String inputValue07 = input07.getText();
        String inputValue08 = input08.getText();
        String inputValue09 = input09.getText();

        String errorString = "";
        Boolean result = true;

//        if (inputValue01.equals("") || Integer.valueOf(inputValue01) > 30 || Integer.valueOf(inputValue01) < 0) {
//            errorString += "[1] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue02.equals("") || Integer.valueOf(inputValue02) > 30 || Integer.valueOf(inputValue02) < 0) {
//            errorString += "[2] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue03.equals("") || Integer.valueOf(inputValue03) > 720 || Integer.valueOf(inputValue03) < 24) {
//            errorString += "[3] - Valid value from 24-720\n";
//            result = false;
//        }
//        if (inputValue04.equals("") || Integer.valueOf(inputValue04) > 30 || Integer.valueOf(inputValue04) < 0) {
//            errorString += "[4] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue05.equals("") || Integer.valueOf(inputValue05) > 30 || Integer.valueOf(inputValue05) < 0) {
//            errorString += "[5] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue06.equals("") || Integer.valueOf(inputValue06) > 60 || Integer.valueOf(inputValue06) < 0) {
//            errorString += "[6] - Valid value from 0-60\n";
//            result = false;
//        }
//        if (inputValue07.equals("") || Integer.valueOf(inputValue07) > 180 || Integer.valueOf(inputValue07) < 0) {
//            errorString += "[7] - Valid value from 0-180\n";
//            result = false;
//        }
//        if (inputValue08.equals("") || Integer.valueOf(inputValue08) > 30 || Integer.valueOf(inputValue08) < 0) {
//            errorString += "[8] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue09.equals("") || Integer.valueOf(inputValue09) > 30 || Integer.valueOf(inputValue09) < 0) {
//            errorString += "[9] - Valid value from 0-30\n";
//            result = false;
//        }
//
//        if (!result) {
//            JOptionPane.showMessageDialog(this, errorString, "Error", JOptionPane.ERROR_MESSAGE);
//        }
        return result;
    }
    
    private void SaveSpec() {
        
        StopBackgroundRunning();
        
        String inputValue01 = input01.getText();
        String inputValue02 = input02.getText();
        String inputValue03 = input03.getText();
        String inputValue04 = input04.getText();
        String inputValue05 = input05.getText();
        String inputValue06 = input06.getText();
        String inputValue07 = input07.getText();
        String inputValue08 = input08.getText();
        String inputValue09 = input09.getText();
        String inputValue10 = input10.getText();
        String inputValue11 = input11.getText();
        String inputValue12 = input12.getText();
        String inputValue13 = input13.getText();
        String inputValue14 = input14.getText();
        String inputValue15 = input15.getText();
        String inputValue16 = input16.getText();
        String inputValue17 = input17.getText();
        String inputValue18 = input18.getText();
        String inputValue19 = input19.getText();
        String inputValue20 = input20.getText();
        String inputValue21 = input21.getText();
        String inputValue22 = input22.getText();
        String inputValue23 = input23.getText();
        String inputValue24 = input24.getText();
        String inputValue25 = input25.getText();
        String inputValue26 = input26.getText();
        String inputValue27 = input27.getText();
        String inputValue28 = input28.getText();
        String inputValue29 = input29.getText();
        String inputValue30 = input30.getText();
        String inputValue31 = input31.getText();
        String inputValue32 = input32.getText();
        String inputValue33 = input33.getText();
        String inputValue34 = input34.getText();
        String inputValue35 = input35.getText();
        String inputValue36 = input36.getText();
        String inputValue37 = input37.getText();

        


        int intVl01 = Integer.valueOf(inputValue01);
        int intVl02 = Integer.valueOf(inputValue02);
        int intVl03 = Integer.valueOf(inputValue03);
        int intVl04 = Integer.valueOf(inputValue04);
        int intVl05 = Integer.valueOf(inputValue05);
        int intVl06 = Integer.valueOf(inputValue06);
        int intVl07 = Integer.valueOf(inputValue07);
        int intVl08 = Integer.valueOf(inputValue08);
        int intVl09 = Integer.valueOf(inputValue09);
        int intVl10 = Integer.valueOf(inputValue10);
        int intVl11 = Integer.valueOf(inputValue11);
        int intVl12 = Integer.valueOf(inputValue12);
        int intVl13 = Integer.valueOf(inputValue13);
        int intVl14 = Integer.valueOf(inputValue14);
        int intVl15 = Integer.valueOf(inputValue15);
        int intVl16 = Integer.valueOf(inputValue16);
        int intVl17 = Integer.valueOf(inputValue17);
        int intVl18 = Integer.valueOf(inputValue18);
        int intVl19 = Integer.valueOf(inputValue19);
        int intVl20 = Integer.valueOf(inputValue20);
        int intVl21 = Integer.valueOf(inputValue21);
        int intVl22 = Integer.valueOf(inputValue22);
        int intVl23 = Integer.valueOf(inputValue23);
        int intVl24 = Integer.valueOf(inputValue24);
        int intVl25 = Integer.valueOf(inputValue25);
        int intVl26 = Integer.valueOf(inputValue26);
        int intVl27 = Integer.valueOf(inputValue27);
        int intVl28 = Integer.valueOf(inputValue28);
        int intVl29 = Integer.valueOf(inputValue29);
        int intVl30 = Integer.valueOf(inputValue30);
        int intVl31 = Integer.valueOf(inputValue31);
        int intVl32 = Integer.valueOf(inputValue32);
        int intVl33 = Integer.valueOf(inputValue33);
        int intVl34 = Integer.valueOf(inputValue34);
        int intVl35 = Integer.valueOf(inputValue35);
        int intVl36 = Integer.valueOf(inputValue36);
        int intVl37 = Integer.valueOf(inputValue37);

        int[] intArray = {intVl01, intVl02, intVl03, intVl04, intVl05, intVl06, intVl07, intVl08, intVl09, intVl10, intVl11, intVl12, intVl13, intVl14, intVl15, intVl16, intVl17, intVl18, intVl19, intVl20, intVl21, intVl22, intVl23, intVl24, intVl25, intVl26, intVl27, intVl28, intVl29, intVl30, intVl31, intVl32, intVl33, intVl34, intVl35, intVl36, intVl37};
        byte[] contentArray = ConvertByteInt.intToBytes(intArray);
        UartBufferS requestSaveData = new UartBufferS(4);
        requestSaveData.headU = UartBufferS.headerTable.get("H_SET");
        requestSaveData.keyU = UartBufferS.controlstatusTable.get("SAVE_TIME");
        requestSaveData.contentArray = new byte[]{0, 0, 0, 0};

        System.out.println("Save data");
        if (CommunicateController.SendDataToController(requestSaveData)) {
            if (CommunicateController.SaveSpecToController(contentArray)) {
                JOptionPane.showMessageDialog(this, "Save Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Error", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        DataFromController.GetTimeFromController();
        
        StartBackgroundRunning();

    }
}
