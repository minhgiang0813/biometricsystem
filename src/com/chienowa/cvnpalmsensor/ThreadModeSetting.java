/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.SerialUartProcess;
import javax.swing.JFrame;

/**
 *
 * @author minhg
 */
public class ThreadModeSetting extends Thread {

    private ModeSettingFrame frame;

    public ThreadModeSetting(ModeSettingFrame mainframe) {
        this.frame = mainframe;
        System.out.println("Start thread of System status");
    }

    @Override
    public void run() {
        frame.currentMode = DataFromController.GetModeSettingFromController();
        System.out.println("Current mode: "+frame.currentMode);
        frame.CheckCurrentMode();
        frame.alarmModeSettingFrame = new ThreadAlarm(frame);
        frame.alarmModeSettingFrame.start();
    }

}
