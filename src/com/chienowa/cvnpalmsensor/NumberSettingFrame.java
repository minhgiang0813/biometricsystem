/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.customui.CvnTextField;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.UartBufferS;
import com.chienowa.ulti.VirtualKeyboard;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author minhg
 */
public class NumberSettingFrame extends CvnFrame implements ActionListener {

    public static short processStep;

    private final ThreadNumberSetting t1;
    CvnButton returnButton = null;
    CvnButton saveButton = null;
    private ClockLabel clockLabel = null;
    private final ManagementMenuFrame mainFrame;
    private final int TXTFIELD_WIDTH = 80;
    private final int TXTFIELD_HEIGHT = 45;
    private final int CX1 = 12;
    private final int CX2 = 300;
    private final int CX3 = 500;
    private final int CX4 = 788;
    private final int CY1 = 20;
    private final int CYS = 60;

    public ThreadAlarm alarmNumberSettingFrame;

    public CvnTextField input01 = null;
    public CvnTextField input02 = null;
    public CvnTextField input03 = null;
    public CvnTextField input04 = null;
    public CvnTextField input05 = null;
    public CvnTextField input06 = null;
    public CvnTextField input07 = null;
    public CvnTextField input08 = null;
    public CvnTextField input09 = null;
    public CvnTextField input10 = null;
//    public CvnTextField input10 = null;
//    public CvnTextField input11 = null;
//    public CvnTextField input12 = null;
//    public CvnTextField input13 = null;
//    public CvnTextField input14 = null;
//    public CvnTextField input15 = null;
//    public CvnTextField input16 = null;
//    public CvnTextField input17 = null;
//    public CvnTextField input18 = null;
//    public CvnTextField input19 = null;

    private JTabbedPane tabbedPanel;
    private JPanel panel1 = new JPanel();
//    private JPanel panel2 = new JPanel();
//    private JPanel panel3 = new JPanel();
//    private JPanel panel4 = new JPanel();

    public NumberSettingFrame(ManagementMenuFrame frame) throws InterruptedException {

        this.mainFrame = frame;

        tabbedPanel = new JTabbedPane();

        tabbedPanel.addTab("Panel 1", panel1);
        panel1.setLayout(null);
//        tabbedPanel.addTab("Panel 2", panel2);
//        panel2.setLayout(null);
//        tabbedPanel.addTab("Panel 3", panel3);
//        tabbedPanel.addTab("Panel 4", panel4);

        JPanel mainPanel = new JPanel();
        mainPanel.setBounds(10, 50, 1004, 470);
        getContentPane().add(mainPanel);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(tabbedPanel, BorderLayout.CENTER);

        txtCaption.setText("Number Setting");

        returnButton = new CvnButton();
        returnButton.setText("Return");
        returnButton.setBounds(12, 530, 200, 57);
        getContentPane().add(returnButton);
        returnButton.addActionListener(this);

        saveButton = new CvnButton();
        saveButton.setText("Save");
        saveButton.setBounds(250, 530, 200, 57);
        getContentPane().add(saveButton);
        saveButton.addActionListener(this);
        
        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);

        Panel1Render();
        VirtualKeyboard.Show();
        
        alarmNumberSettingFrame = new ThreadAlarm(this);

        //Start thread for this frame
        t1 = new ThreadNumberSetting(this);
        t1.start();
    }
    
    public void StartBackgroundRunning() {
        alarmNumberSettingFrame = new ThreadAlarm(this);
        alarmNumberSettingFrame.start();
        clockLabel.timerTimeandDate.start();
    }
    
    public void StopBackgroundRunning() {
        alarmNumberSettingFrame.stop();
        clockLabel.timerTimeandDate.stop();
    }

    private void Panel1Render() {
        CvnLabel label01 = new CvnLabel("[1] Over Voltage 1");
        label01.setBounds(CX1, CY1, 350, 35);
        panel1.add(label01);

        input01 = new CvnTextField();
        input01.setBounds(CX2, CY1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input01);
//        input01.addMouseListener(new java.awt.event.MouseAdapter() {
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//                ShowVirtualKeyboard(input01);
//            }
//        });

        CvnLabel label02 = new CvnLabel("[2] Over Voltage 2 (V)");
        label02.setBounds(CX1, CY1 + CYS * 1, 350, 35);
        panel1.add(label02);

        input02 = new CvnTextField();
        input02.setBounds(CX2, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input02);
//
        CvnLabel label03 = new CvnLabel("[3] Over Voltage 3 (V)");
        label03.setBounds(CX1, CY1 + CYS * 2, 350, 35);
        panel1.add(label03);

        input03 = new CvnTextField();
        input03.setBounds(CX2, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input03);
//
        CvnLabel label04 = new CvnLabel("[4] Under Voltage (V)");
        label04.setBounds(CX1, CY1 + CYS * 3, 350, 35);
        panel1.add(label04);

        input04 = new CvnTextField();
        input04.setBounds(CX2, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input04);

        CvnLabel label05 = new CvnLabel("[5] Over Current (A)");
        label05.setBounds(CX1, CY1 + CYS * 4, 350, 35);
        panel1.add(label05);

        input05 = new CvnTextField();
        input05.setBounds(CX2, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input05);

        CvnLabel label06 = new CvnLabel("[6] Under Current (A)");
        label06.setBounds(CX3, CY1 + CYS * 0, 350, 35);
        panel1.add(label06);

        input06 = new CvnTextField();
        input06.setBounds(CX4, CY1 + CYS * 0, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input06);

        CvnLabel label07 = new CvnLabel("[7] Over Flow (l/min)");
        label07.setBounds(CX3, CY1 + CYS * 1, 350, 35);
        panel1.add(label07);

        input07 = new CvnTextField();
        input07.setBounds(CX4, CY1 + CYS * 1, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input07);

        CvnLabel label08 = new CvnLabel("[8] Under Flow (l/min)");
        label08.setBounds(CX3, CY1 + CYS * 2, 350, 35);
        panel1.add(label08);

        input08 = new CvnTextField();
        input08.setBounds(CX4, CY1 + CYS * 2, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input08);

        CvnLabel label09 = new CvnLabel("[9] Cvcc Current (A)");
        label09.setBounds(CX3, CY1 + CYS * 3, 350, 35);
        panel1.add(label09);

        input09 = new CvnTextField();
        input09.setBounds(CX4, CY1 + CYS * 3, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input09);
        
        CvnLabel label10 = new CvnLabel("[10] Salt pump voltage");
        label10.setBounds(CX3, CY1 + CYS * 4, 350, 35);
        panel1.add(label10);

        input10 = new CvnTextField();
        input10.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
        panel1.add(input10);
//
//        CvnLabel label10 = new CvnLabel("[10] 10");
//        label10.setBounds(CX3, CY1 + CYS * 4, 350, 35);
//        panel1.add(label10);
//
//        input10 = new CvnTextField();
//        input10.setBounds(CX4, CY1 + CYS * 4, TXTFIELD_WIDTH, TXTFIELD_HEIGHT);
//        panel1.add(input10);
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(returnButton)) {
            ReturnMainFrame();
        }
        if (obj.equals(saveButton)) {
            if (CheckValidation()) {
                SaveSpec();
            }
        }
    }

    public void ReturnMainFrame() {
        StopBackgroundRunning();
        VirtualKeyboard.Hide();
        this.dispose();
        this.setVisible(false);
        mainFrame.setVisible(true);
        mainFrame.StartBackgroundRunning();

    }

    private boolean CheckValidation() {

        String inputValue01 = input01.getText();
        String inputValue02 = input02.getText();
        String inputValue03 = input03.getText();
        String inputValue04 = input04.getText();
        String inputValue05 = input05.getText();
        String inputValue06 = input06.getText();
        String inputValue07 = input07.getText();
        String inputValue08 = input08.getText();

        String errorString = "";
        Boolean result = true;

//        if (inputValue01.equals("") || Integer.valueOf(inputValue01) > 30 || Integer.valueOf(inputValue01) < 0) {
//            errorString += "[1] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue02.equals("") || Integer.valueOf(inputValue02) > 30 || Integer.valueOf(inputValue02) < 0) {
//            errorString += "[2] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue03.equals("") || Integer.valueOf(inputValue03) > 720 || Integer.valueOf(inputValue03) < 24) {
//            errorString += "[3] - Valid value from 24-720\n";
//            result = false;
//        }
//        if (inputValue04.equals("") || Integer.valueOf(inputValue04) > 30 || Integer.valueOf(inputValue04) < 0) {
//            errorString += "[4] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue05.equals("") || Integer.valueOf(inputValue05) > 30 || Integer.valueOf(inputValue05) < 0) {
//            errorString += "[5] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue06.equals("") || Integer.valueOf(inputValue06) > 60 || Integer.valueOf(inputValue06) < 0) {
//            errorString += "[6] - Valid value from 0-60\n";
//            result = false;
//        }
//        if (inputValue07.equals("") || Integer.valueOf(inputValue07) > 180 || Integer.valueOf(inputValue07) < 0) {
//            errorString += "[7] - Valid value from 0-180\n";
//            result = false;
//        }
//        if (inputValue08.equals("") || Integer.valueOf(inputValue08) > 30 || Integer.valueOf(inputValue08) < 0) {
//            errorString += "[8] - Valid value from 0-30\n";
//            result = false;
//        }
//        if (inputValue09.equals("") || Integer.valueOf(inputValue09) > 30 || Integer.valueOf(inputValue09) < 0) {
//            errorString += "[9] - Valid value from 0-30\n";
//            result = false;
//        }
//
//        if (!result) {
//            JOptionPane.showMessageDialog(this, errorString, "Error", JOptionPane.ERROR_MESSAGE);
//        }
        return result;
    }

    private void SaveSpec() {
        StopBackgroundRunning();

        String inputValue01 = input01.getText();
        String inputValue02 = input02.getText();
        String inputValue03 = input03.getText();
        String inputValue04 = input04.getText();
        String inputValue05 = input05.getText();
        String inputValue06 = input06.getText();
        String inputValue07 = input07.getText();
        String inputValue08 = input08.getText();
        String inputValue09 = input09.getText();
        String inputValue10 = input10.getText();

        float intVl01 = Float.valueOf(inputValue01);
        float intVl02 = Float.valueOf(inputValue02);
        float intVl03 = Float.valueOf(inputValue03);
        float intVl04 = Float.valueOf(inputValue04);
        float intVl05 = Float.valueOf(inputValue05);
        float intVl06 = Float.valueOf(inputValue06);
        float intVl07 = Float.valueOf(inputValue07);
        float intVl08 = Float.valueOf(inputValue08);
        float intVl09 = Float.valueOf(inputValue09);
        float intVl10 = Float.valueOf(inputValue10);

        System.out.printf("So float dau: %.2f \n", intVl01);

        float[] floatArray = {intVl01, intVl02, intVl03, intVl04, intVl05, intVl06, intVl07, intVl08, intVl09, intVl10};
        byte[] contentArray = ConvertByteInt.ConvertFloatArrayToByteArray(floatArray);
        UartBufferS requestSaveData = new UartBufferS(4);
        requestSaveData.headU = UartBufferS.headerTable.get("H_SET");
        requestSaveData.keyU = UartBufferS.controlstatusTable.get("SAVE_NUMBER");
        requestSaveData.contentArray = new byte[]{0, 0, 0, 0};

        System.out.println("Save data");
        if (CommunicateController.SendDataToController(requestSaveData)) {
            if (CommunicateController.SaveSpecToController(contentArray)) {
                JOptionPane.showMessageDialog(this, "Save Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Error", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        StartBackgroundRunning();

    }
}
