/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.runbackground.AlarmJSONLog;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author minhg
 */
public class AlarmLogFrame extends CvnFrame implements ActionListener {

    private CvnButton btnReturn;

    public ThreadAlarm threadAlarmLogFrame;

    private final short XBTNPOSITION = 250;
    private final short YBTNPOSITION = 80;
    private final short YBTNSPACE = 80;
    private final short XBTNSPACE = 150;
    private final short BTNWIDTH = 100;
    private final short BTNHEIGHT = 50;

    private ManagementMenuFrame mainmenuframe;
    private ClockLabel clockLabel = null;

    private JTable alarmTable;

    public AlarmLogFrame(ManagementMenuFrame frame) throws IOException, ParseException {
        mainmenuframe = frame;
        InitGUI();
        threadAlarmLogFrame = new ThreadAlarm(this);
        threadAlarmLogFrame.start();
    }

    private void InitGUI() throws IOException, ParseException {

        txtCaption.setText("ALARM LOG");

        JScrollPane scrollPanel = new JScrollPane();
        getContentPane().add(scrollPanel);
        alarmTable = new JTable();
        alarmTable.setFillsViewportHeight(true);

        DefaultTableModel alarmData = new DefaultTableModel();
        alarmTable.setModel(alarmData);

        AlarmJSONLog alarmJSONLog = new AlarmJSONLog();

        alarmData.addColumn("Date");
        alarmData.addColumn("Description");
        alarmData.addColumn("Time");
        int sizeArray = alarmJSONLog.alarmList.size();

        for (int i = 0; i < sizeArray; i++) {
            JSONObject aObj = (JSONObject) alarmJSONLog.alarmList.get(sizeArray - i - 1);
            String date = (String) aObj.get("Date");
            String description = (String) aObj.get("Description");
            String time = (String) aObj.get("Time");
//            System.out.println("Date: " + date + " - Time: " + time + " - Description: " + description);
            alarmData.addRow(new Object[]{date, description, time});
        }

        scrollPanel.setBounds(12, 50, 1000, 450);
        scrollPanel.setViewportView(alarmTable);
        if (alarmTable.getColumnModel().getColumnCount() > 0) {
            alarmTable.getColumnModel().getColumn(1).setResizable(false);
        }

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);
        
        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);

        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 200, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(this);

    }

    private void ReturnMainMenuFrame() {
        threadAlarmLogFrame.stop();
        clockLabel.timerTimeandDate.stop();
        this.dispose();
        this.setVisible(false);
        mainmenuframe.setVisible(true);
        mainmenuframe.StartBackgroundRunning();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnReturn)) {
            ReturnMainMenuFrame();
        }

    }

}
