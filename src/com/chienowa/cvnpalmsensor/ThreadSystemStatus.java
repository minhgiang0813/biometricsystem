/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.systemstatusanimation.myFrame;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import java.util.BitSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class ThreadSystemStatus extends Thread {

    private String threadName;
    private myFrame frame;
    private Thread t;

    private boolean AlkalineEmptyLevel;
    private boolean AlkalineLowLevel;
    private boolean AlkalineHighLevel;
    private boolean AcidEmptyLevel;
    private boolean AcidLowLevel;
    private boolean AcidHighLevel;
    private boolean SaltLowLevel;
    private boolean SaltHighLevel;
    private boolean SV1;
    private boolean SV2;
    private boolean SV3;
    private boolean SV4;
    private boolean SV5;
    private boolean SV6;
    private boolean SV7;
    private boolean SV8;
    private boolean SV9;
    private boolean RSVD;
    private boolean Pump1;
    private boolean Pump2;
    private boolean SaltPump;
    private boolean RSVD1;
    public ScheduledExecutorService executor;
    

    private short MODE = 0;

    public ThreadSystemStatus(String name, myFrame frame) {
        this.frame = frame;
        threadName = name;
        System.out.println("Start thread of System status");
    }

    @Override
    public void run() {
        Runnable runUpdateSystemStatus = new Runnable() {
            public void run() {
                UpdateSystemStatus();
            }
        };

        executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(runUpdateSystemStatus, 0, 3, TimeUnit.SECONDS);

    }

    private void UpdateSystemStatus() {
        byte[] receiveData = null;
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("MACHINE_STATUS");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
//
        if (CommunicateController.SendDataToController(sendBuffer)) {
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < 2000) {
                receiveData = SerialUartProcess.ReceiveMessage(18);
                if (receiveData != null) {
                    BitSet bitset = BitSet.valueOf(receiveData);
                    System.out.println(bitset);
                    AlkalineEmptyLevel = bitset.get(0);
                    AlkalineLowLevel = bitset.get(1);
                    AlkalineHighLevel = bitset.get(2);
                    AcidEmptyLevel = bitset.get(3);
                    AcidLowLevel = bitset.get(4);
                    AcidHighLevel = bitset.get(5);
                    SaltLowLevel = bitset.get(6);
                    SaltHighLevel = bitset.get(7);
                    SV1 = bitset.get(8);
                    SV2 = bitset.get(9);
                    SV3 = bitset.get(10);
                    SV4 = bitset.get(11);
                    SV5 = bitset.get(12);
                    SV6 = bitset.get(13);
                    SV7 = bitset.get(14);
                    SV8 = bitset.get(15);
                    SV9 = bitset.get(16);
                    RSVD = bitset.get(17);
                    Pump1 = bitset.get(24);
                    Pump2 = bitset.get(25);
                    SaltPump = bitset.get(26);
                    RSVD1 = bitset.get(27);

                    for (int i = 0; i < bitset.length(); i++) {
                        System.out.println("bit " + i + ": " + bitset.get(i));
                    }
                    if (!Pump1 && !Pump2 && !SV1 && !SV2 && !SV3 && !SV4 && !SV5 && !SV6) {
                        MODE = 0;
                    }
                    if (Pump1 && SV5 && !Pump2 && !SV1 && !SV2 && !SV3 && !SV4 && !SV6) {
                        MODE = 1;
                    }
                    if (Pump2 && SV6 && !Pump1 && !SV1 && !SV2 && !SV3 && !SV4 && !SV5) {
                        MODE = 2;
                    }
                    if (!Pump1 && !Pump2 && !SV1 && SV2 && !SV3 && !SV4 && !SV5 && !SV6) {
                        MODE = 3;
                    }
                    if (!Pump1 && Pump2 && !SV1 && !SV2 && !SV3 && SV4 && !SV5 && !SV6) {
                        MODE = 4;
                    }
                    if (Pump1 && !Pump2 && !SV1 && !SV2 && SV3 && !SV4 && !SV5 && !SV6) {
                        MODE = 5;
                    }
                    if (!Pump1 && !Pump2 && SV1 && !SV2 && !SV3 && !SV4 && !SV5 && !SV6) {
                        MODE = 6;
                    }

                    frame.Mode = MODE;
//                    frame.textTitle.setText("Mode hien tai la: " + MODE);
                    System.out.println("Mode hien tai: " + MODE);

                    short AcidTankLv = 0;
                    if (AcidEmptyLevel && AcidLowLevel && AcidHighLevel) {
                        AcidTankLv = 3;
                    }
                    if (AcidEmptyLevel && AcidLowLevel && !AcidHighLevel) {
                        AcidTankLv = 2;
                    }
                    if (AcidEmptyLevel && !AcidLowLevel && !AcidHighLevel) {
                        AcidTankLv = 1;
                    }
                    if (!AcidEmptyLevel && !AcidLowLevel && !AcidHighLevel) {
                        AcidTankLv = 0;
                    }

                    short AlkalineTanklv = 0;
                    if (AlkalineEmptyLevel && AlkalineLowLevel && AlkalineHighLevel) {
                        AlkalineTanklv = 3;
                    }
                    if (AlkalineEmptyLevel && AlkalineLowLevel && !AlkalineHighLevel) {
                        AlkalineTanklv = 2;
                    }
                    if (AlkalineEmptyLevel && !AlkalineLowLevel && !AlkalineHighLevel) {
                        AlkalineTanklv = 1;
                    }
                    if (!AlkalineEmptyLevel && !AlkalineLowLevel && !AlkalineHighLevel) {
                        AlkalineTanklv = 0;
                    }

                    short SaltTankLv = 0;
                    if (!SaltLowLevel && !SaltHighLevel) {
                        SaltTankLv = 0;
                    }
                    if (SaltLowLevel && !SaltHighLevel) {
                        SaltTankLv = 1;
                    }
                    if (SaltLowLevel && SaltHighLevel) {
                        SaltTankLv = 2;
                    }
//                    System.out.println("AcidTanklV: " + AcidTankLv);
//                    System.out.println("AlkalineTankLv: " + AlkalineTanklv);
//                    System.out.println("SaltTankLv: " + SaltTankLv);
                    frame.SetTankLevel(AcidTankLv, AlkalineTanklv, SaltTankLv);

                    byte[] thongsoArray = new byte[12];
                    System.arraycopy(receiveData, 6, thongsoArray, 0, 12);
                    byte[] thongsoArrayS = new byte[12];
                    for (int i = 0; i < thongsoArray.length; i++) {
                        thongsoArrayS[i] = thongsoArray[thongsoArray.length - i - 1];
                    }
                    StringBuilder sb = new StringBuilder();
                    for (byte b : thongsoArrayS) {
                        sb.append(String.format("%02X ", b));
                    }
                    System.out.println(sb.toString());
                    float[] floatArrays = ConvertByteInt.ConvertByteArrayToFloatArray(thongsoArrayS);
                    System.out.printf("So float 1: %.2f \n", floatArrays[0]);
                    System.out.printf("So float 2: %.2f \n", floatArrays[1]);
                    System.out.printf("So float 3: %.2f \n", floatArrays[2]);

                    frame.ThongSo(0, 0, 0, floatArrays[2], floatArrays[1], floatArrays[0], 0);

                    break;
                }
            }
        } else {
            JOptionPane.showMessageDialog(frame, "Cannot communicate with Controller!!", "Error", JOptionPane.ERROR_MESSAGE);
//            System.exit(0);
            System.out.println("Cannot get data from controller");
        }
    }

    public void start() {
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }

}
