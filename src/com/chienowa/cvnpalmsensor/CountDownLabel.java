/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnLabel;
import java.util.TimerTask;

/**
 *
 * @author minhg
 */
public class CountDownLabel extends TimerTask {

    int countdown;
    CvnLabel label = null;

    public CountDownLabel(CvnLabel label, int countdownTime) {
        this.label = label;
        countdown = countdownTime;
    }

    @Override
    public void run() {
        if (countdown > 0) {
            countdown = countdown - 1;
//        System.out.println(countdown);
            label.setText(String.valueOf(countdown));
        } else {
            label.setText("OK");
            cancel();
        }

    }

}
