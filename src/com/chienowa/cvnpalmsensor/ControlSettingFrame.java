/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.UartBufferS;
import com.fujitsu.frontech.palmsecure_smpl.PsSampleApl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

/**
 *
 * @author minhg
 */
public class ControlSettingFrame extends CvnFrame implements ActionListener {

    private CvnButton btnReturn;

    public ThreadAlarm threadAlarmControlSettingFrame;
    private ThreadControlSettingFrame threadControlSettingFrame;
    private ClockLabel clockLabel = null;

    private final short XBTNPOSITION = 400;
    private final short YBTNPOSITION = 80;
    private final short YBTNSPACE = 80;
    private final short XBTNSPACE = 150;
    private final short BTNWIDTH = 100;
    private final short BTNHEIGHT = 50;
    private final short LBLWIDTH = 350;

    private ManagementMenuFrame mainmenuframe;
    private CvnButton btnDrainageModeON;
    private CvnButton btnDrainageModeOFF;
    private CvnButton btnTestPOMode1ON;
    private CvnButton btnTestPOMode1OFF;
    private CvnButton btnBioModeON;
    private CvnButton btnBioModeOFF;
    private CvnButton btnTestPOMode3ON;
    private CvnButton btnTestPOMode3OFF;
    private CvnButton btnWaterFileterON;
    private CvnButton btnWaterFileterOFF;

    private boolean vlDrainageMode = false;
    private boolean vlTestPowerOnMode = false;
    private boolean vlWaterFilter = false;
    private boolean vlBioMode = false;

    public ControlSettingFrame(ManagementMenuFrame frame) throws InterruptedException {
        mainmenuframe = frame;
        InitGUI();
        threadControlSettingFrame = new ThreadControlSettingFrame(this);
        threadControlSettingFrame.start();

    }

    private void InitGUI() {

        txtCaption.setText("CONTROL SETTING");

        CvnLabel label1 = new CvnLabel("Drainage mode");
        label1.setBounds(20, YBTNPOSITION, LBLWIDTH, 50);
        getContentPane().add(label1);

        btnDrainageModeON = new CvnButton();
        btnDrainageModeON.setText("ON");
        btnDrainageModeON.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnDrainageModeON);
        btnDrainageModeON.addActionListener(this);

        btnDrainageModeOFF = new CvnButton();
        btnDrainageModeOFF.setText("OFF");
        btnDrainageModeOFF.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnDrainageModeOFF);
        btnDrainageModeOFF.addActionListener(this);

        CvnLabel label2 = new CvnLabel("Test power on mode");
        label2.setBounds(20, YBTNPOSITION + YBTNSPACE * 1, LBLWIDTH, 50);
        getContentPane().add(label2);

        btnTestPOMode1ON = new CvnButton();
        btnTestPOMode1ON.setText("ON");
        btnTestPOMode1ON.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnTestPOMode1ON);
        btnTestPOMode1ON.addActionListener(this);

        btnTestPOMode1OFF = new CvnButton();
        btnTestPOMode1OFF.setText("OFF");
        btnTestPOMode1OFF.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnTestPOMode1OFF);
        btnTestPOMode1OFF.addActionListener(this);

        CvnLabel lblBioMode = new CvnLabel("BIOMETRIC SENSOR");
        lblBioMode.setBounds(20, YBTNPOSITION + YBTNSPACE * 2, LBLWIDTH, 50);
        getContentPane().add(lblBioMode);

        btnBioModeON = new CvnButton();
        btnBioModeON.setText("ON");
        btnBioModeON.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnBioModeON);
        btnBioModeON.addActionListener(this);

        btnBioModeOFF = new CvnButton();
        btnBioModeOFF.setText("OFF");
        btnBioModeOFF.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnBioModeOFF);
        btnBioModeOFF.addActionListener(this);

        CvnLabel label4 = new CvnLabel("-");
        label4.setBounds(20, YBTNPOSITION + YBTNSPACE * 3, LBLWIDTH, 50);
        getContentPane().add(label4);

        btnTestPOMode3ON = new CvnButton();
        btnTestPOMode3ON.setText("ON");
        btnTestPOMode3ON.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnTestPOMode3ON);
        btnTestPOMode3ON.addActionListener(this);

        btnTestPOMode3OFF = new CvnButton();
        btnTestPOMode3OFF.setText("OFF");
        btnTestPOMode3OFF.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnTestPOMode3OFF);
        btnTestPOMode3OFF.addActionListener(this);

        CvnLabel label5 = new CvnLabel("Water filter");
        label5.setBounds(20, YBTNPOSITION + YBTNSPACE * 4, LBLWIDTH, 50);
        getContentPane().add(label5);

        btnWaterFileterON = new CvnButton();
        btnWaterFileterON.setText("ON");
        btnWaterFileterON.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 4, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnWaterFileterON);
        btnWaterFileterON.addActionListener(this);

        btnWaterFileterOFF = new CvnButton();
        btnWaterFileterOFF.setText("OFF");
        btnWaterFileterOFF.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 4, BTNWIDTH, BTNHEIGHT);
        getContentPane().add(btnWaterFileterOFF);
        btnWaterFileterOFF.addActionListener(this);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);

        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 200, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(this);
        
        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);

    }
    
    private void StartBackgroundRunning(){
        threadAlarmControlSettingFrame = new ThreadAlarm(this);
        threadAlarmControlSettingFrame.start();
        clockLabel.timerTimeandDate.start();
    }
    
    private void StopBackgroundRunning(){
        threadAlarmControlSettingFrame.stop();
        clockLabel.timerTimeandDate.stop();
    }

    private void ReturnMainMenuFrame() {
        StopBackgroundRunning();
        this.dispose();
        this.setVisible(false);
        mainmenuframe.setVisible(true);
        mainmenuframe.StartBackgroundRunning();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnReturn)) {
            ReturnMainMenuFrame();
        }
        if (obj.equals(btnDrainageModeON)) {
            SetDrainageMode(true);
            SaveDrainageMode();
        }
        if (obj.equals(btnDrainageModeOFF)) {
            SetDrainageMode(false);
            SaveDrainageMode();
        }
        if (obj.equals(btnTestPOMode1ON)) {
            SetTestPowerOnMode(true);
            SaveTestPowerOnMode();
        }
        if (obj.equals(btnTestPOMode1OFF)) {
            SetTestPowerOnMode(false);
            SaveTestPowerOnMode();
        }
        if (obj.equals(btnWaterFileterON)) {
            SetWaterFilter(true);
            SaveWaterFilter();
        }
        if (obj.equals(btnWaterFileterOFF)) {
            SetWaterFilter(false);
            SaveWaterFilter();
        }
        if (obj.equals(btnBioModeON)) {
            JOptionPane.showMessageDialog(this, "App will restart to use new mode.", "Information", JOptionPane.INFORMATION_MESSAGE);
            SetBioMode(true);
            SaveBioMode();
            try {
                Runtime.getRuntime().exec("java -jar PalmSecureSample_Java.jar");
            } catch (IOException ex) {
                Logger.getLogger(ControlSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        }
        if (obj.equals(btnBioModeOFF)) {
            JOptionPane.showMessageDialog(this, "App will restart to use new mode.", "Information", JOptionPane.INFORMATION_MESSAGE);
            SetBioMode(false);
            SaveBioMode();
            try {
                Runtime.getRuntime().exec("java -jar PalmSecureSample_Java.jar");
            } catch (IOException ex) {
                Logger.getLogger(ControlSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        }

    }

    public void SetDrainageMode(boolean mode) {
        if (mode) {
            vlDrainageMode = true;
            btnDrainageModeON.SetActiveButton();
            btnDrainageModeOFF.SetNormalButton();
        } else {
            vlDrainageMode = false;
            btnDrainageModeON.SetNormalButton();
            btnDrainageModeOFF.SetActiveButton();
        }
    }

    public void SetTestPowerOnMode(boolean mode) {
        if (mode) {
            vlTestPowerOnMode = true;
            btnTestPOMode1ON.SetActiveButton();
            btnTestPOMode1OFF.SetNormalButton();
        } else {
            vlTestPowerOnMode = false;
            btnTestPOMode1ON.SetNormalButton();
            btnTestPOMode1OFF.SetActiveButton();
        }
    }

    public void SetWaterFilter(boolean mode) {
        if (mode) {
            vlWaterFilter = true;
            btnWaterFileterON.SetActiveButton();
            btnWaterFileterOFF.SetNormalButton();
        } else {
            vlWaterFilter = false;
            btnWaterFileterON.SetNormalButton();
            btnWaterFileterOFF.SetActiveButton();
        }
    }

    public void SetBioMode(boolean mode) {
        if (mode) {
            vlBioMode = true;
            btnBioModeON.SetActiveButton();
            btnBioModeOFF.SetNormalButton();
        } else {
            vlBioMode = false;
            btnBioModeON.SetNormalButton();
            btnBioModeOFF.SetActiveButton();
        }
    }

    private void SaveDrainageMode() {
        StopBackgroundRunning();
        int intValue;
        if (vlDrainageMode) {
            intValue = 1;
        } else {
            intValue = 0;
        }
        UartBufferS sendUart = new UartBufferS(4);
        sendUart.headU = UartBufferS.headerTable.get("H_SET");
        sendUart.keyU = UartBufferS.controlstatusTable.get("DRAINAGE_MODE_SET");
        sendUart.contentArray = ConvertByteInt.intToByteArray(intValue);
        if (CommunicateController.SendDataToController(sendUart)) {
            System.out.println("Save Drainage mode success");
        } else {
            JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
        }
        StartBackgroundRunning();
    }

    private void SaveTestPowerOnMode() {
        StopBackgroundRunning();
        int intValue;
        if (vlTestPowerOnMode) {
            intValue = 1;
        } else {
            intValue = 0;
        }
        UartBufferS sendUart = new UartBufferS(4);
        sendUart.headU = UartBufferS.headerTable.get("H_SET");
        sendUart.keyU = UartBufferS.controlstatusTable.get("POWER_ON_TEST_SET");
        sendUart.contentArray = ConvertByteInt.intToByteArray(intValue);
        if (CommunicateController.SendDataToController(sendUart)) {
            System.out.println("Save Test Power On mode success");
        } else {
            JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
        }
        StartBackgroundRunning();
    }

    private void SaveWaterFilter() {
        StopBackgroundRunning();
        int intValue;
        if (vlWaterFilter) {
            intValue = 1;
        } else {
            intValue = 0;
        }
        UartBufferS sendUart = new UartBufferS(4);
        sendUart.headU = UartBufferS.headerTable.get("H_SET");
        sendUart.keyU = UartBufferS.controlstatusTable.get("WATER_FILTERE_SET");
        sendUart.contentArray = ConvertByteInt.intToByteArray(intValue);
        if (CommunicateController.SendDataToController(sendUart)) {
            System.out.println("Save Water Filter mode success");
        } else {
            JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
        }
        StartBackgroundRunning();
    }

    private void SaveBioMode() {
        StopBackgroundRunning();
        int intValue;
        if (vlBioMode) {
            intValue = 1;
        } else {
            intValue = 0;
        }
        UartBufferS sendUart = new UartBufferS(4);
        sendUart.headU = UartBufferS.headerTable.get("H_SET");
        sendUart.keyU = UartBufferS.controlstatusTable.get("BIOMECTRIC_SET");
        sendUart.contentArray = ConvertByteInt.intToByteArray(intValue);
        if (CommunicateController.SendDataToController(sendUart)) {
            System.out.println("Save BioMode success");
        } else {
            JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
        }
        StartBackgroundRunning();
    }

}
