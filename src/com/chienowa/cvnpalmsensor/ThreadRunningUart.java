/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import java.awt.Color;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.Timer;

/**
 *
 * @author minhg
 */
public class ThreadRunningUart extends Thread {

    private UartBufferS receiveBuffer;

    private String threadName;
    public boolean stopRunningMain = false;
    protected MachineWorking frame;

    private int currentMode;
    private int acidInt;
    private int baseInt;
    private int waterInt;
    private int acidModeCountdown;
    private int baseModeCountdown;

    public ThreadRunningUart(String name, MachineWorking frame) {
        this.frame = frame;
        threadName = name;
        stopRunningMain = false;
        System.out.println("Start thread of Machine working");
        // Getting data from Controller
//        DataFromController.GetTimeFromController();
//        for (int i = 0; i < DataFromController.dataTimeIntArray.length; i++) {
//            System.out.println("Data time array value: "+DataFromController.dataTimeIntArray[i]);
//        }
        acidInt = DataFromController.dataTimeIntArray[9];
        baseInt = DataFromController.dataTimeIntArray[10];
        waterInt = DataFromController.dataTimeIntArray[8];
        acidModeCountdown = DataFromController.dataTimeIntArray[5];
        baseModeCountdown = DataFromController.dataTimeIntArray[4];
        frame.acidLabel.setText(String.valueOf(acidInt));
        frame.baseLabel.setText(String.valueOf(baseInt));
        frame.waterLabel.setText(String.valueOf(waterInt));

        
    }

    @Override
    public void run() {
        
        // Getting mode from Controller
        currentMode = DataFromController.GetModeSettingFromController();
        
        switch (currentMode) {
            case 1:
                RunningHandMode();
                break;
            case 2: {
                try {
                    RunningWaterMode();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
            case 3: {
                try {
                    RunningAcidMode();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
            case 4: {
                try {
                    RunningBaseMode();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;

            default:
                System.out.println("Mode is not valid");
        }

    }

    private void SetVisiableUI() {
        switch (currentMode) {
            case 1:
                frame.label2.setVisible(true);
                frame.label3.setVisible(true);
                frame.label4.setVisible(true);
                frame.blueSquare.setVisible(true);
                frame.magentaSquare.setVisible(true);
                frame.blackSquare.setVisible(true);
                frame.baseLabel.setVisible(true);
                frame.acidLabel.setVisible(true);
                frame.waterLabel.setVisible(true);
                break;
            case 2:
                frame.label2.setVisible(false);
                frame.label3.setVisible(false);
                frame.label4.setVisible(true);
                frame.blueSquare.setVisible(false);
                frame.magentaSquare.setVisible(false);
                frame.blackSquare.setVisible(true);
                frame.baseLabel.setVisible(false);
                frame.acidLabel.setVisible(false);
                frame.waterLabel.setVisible(true);
                break;
            case 3:
                frame.label2.setVisible(true);
                frame.label3.setVisible(false);
                frame.label4.setVisible(false);
                frame.blueSquare.setVisible(false);
                frame.magentaSquare.setVisible(true);
                frame.blackSquare.setVisible(false);
                frame.baseLabel.setVisible(false);
                frame.acidLabel.setVisible(true);
                frame.waterLabel.setVisible(false);
                break;
            case 4:
                frame.label2.setVisible(false);
                frame.label3.setVisible(true);
                frame.label4.setVisible(false);
                frame.blueSquare.setVisible(true);
                frame.magentaSquare.setVisible(false);
                frame.blackSquare.setVisible(false);
                frame.baseLabel.setVisible(true);
                frame.acidLabel.setVisible(false);
                frame.waterLabel.setVisible(false);
                break;
        }
    }

    private void RunningWaterMode() throws InterruptedException, IOException {
        System.out.println("Start Water mode");
        SetVisiableUI();
        SendIdentifyUserOK();
        StartNextAnimation();
        frame.waterLabel.setText("Running...");
        frame.blackSquare.setBackground(Color.WHITE);
        WaitForNextAnimation();
        frame.waterLabel.setText("FINISH");
        Thread.sleep(2000);
        frame.CloseMachineFrame();
    }

    private void RunningAcidMode() throws InterruptedException, IOException {
        System.out.println("Running Acid Mode");
        SetVisiableUI();
        SendIdentifyUserOK();
        StartNextAnimation();
        CountDownLabel cdAcidLabel = new CountDownLabel(frame.acidLabel, acidModeCountdown);
        Timer timerA = new Timer();
        timerA.schedule(cdAcidLabel, 0, 1000);
        frame.magentaSquare.setBackground(Color.WHITE);
//        Thread.sleep((acidModeCountdown - 1) * 1000);
        WaitForNextAnimation();
        timerA.cancel();
        frame.acidLabel.setText("FINISH");
        Thread.sleep(2000);
        frame.CloseMachineFrame();
    }

    private void RunningBaseMode() throws InterruptedException, IOException {
        System.out.println("Running Base Mode");
        SetVisiableUI();
        SendIdentifyUserOK();
        StartNextAnimation();
        CountDownLabel cdBaseLabel = new CountDownLabel(frame.baseLabel, baseModeCountdown);
        Timer timerB = new Timer();
        timerB.schedule(cdBaseLabel, 0, 1000);
        frame.blueSquare.setBackground(Color.WHITE);
//        Thread.sleep((baseModeCountdown - 1) * 1000);
        WaitForNextAnimation();
        timerB.cancel();
        frame.baseLabel.setText("FINISH");
        Thread.sleep(2000);
        frame.CloseMachineFrame();
    }

    private void RunningHandMode() {
        System.out.println("Start Hand Mode");
        SetVisiableUI();
        System.out.println("Step 0");
        SendIdentifyUserOK();

        System.out.println("Step 1");
        StartNextAnimation();
        // set countdown label
        CountDownLabel cdBaseLabel = new CountDownLabel(frame.baseLabel, baseInt);
        Timer timerB = new Timer();
        timerB.schedule(cdBaseLabel, 0, 1000);
        frame.blueSquare.setBackground(Color.WHITE);

        try {
            Thread.sleep((baseInt - 1) * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Step 2");
        StartNextAnimation();

        CountDownLabel cdAcidLabel = new CountDownLabel(frame.acidLabel, acidInt);
        Timer timerA = new Timer();
        timerA.schedule(cdAcidLabel, 0, 1000);
        frame.magentaSquare.setBackground(Color.WHITE);
        try {
            Thread.sleep((acidInt - 1) * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
        }
//
        System.out.println("Start washing with water");
        StartNextAnimation();
        CountDownLabel cdWaterLabel = new CountDownLabel(frame.waterLabel, waterInt);
        Timer timerC = new Timer();
        timerC.schedule(cdWaterLabel, 0, 1000);
        frame.blackSquare.setBackground(Color.WHITE);
        try {
            Thread.sleep((waterInt - 1) * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Completed. Exit to main frame!!");
        StartNextAnimation();
        try {
            frame.CloseMachineFrame();
        } catch (IOException ex) {
            Logger.getLogger(ThreadRunningUart.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Thread " + threadName + " exiting.");
    }

    private void SendIdentifyUserOK() {
        System.out.println("Send Identify User OK");
        UartBufferS sendData = new UartBufferS(4);
        sendData.headU = UartBufferS.headerTable.get("H_SET");
        sendData.keyU = UartBufferS.controlstatusTable.get("OK_USER");
        sendData.contentArray = new byte[]{0, 0, 0, 0};
        if (!CommunicateController.SendDataToController(sendData)) {
            JOptionPane.showMessageDialog(frame, "Cannot communicate with Controller!!", "Error", JOptionPane.ERROR_MESSAGE);
//            System.exit(0);
        }
    }

    private void StartNextAnimation() {
        UartBufferS checkNextStep = CommunicateController.ReceiveDataFromController(5000);
        if (checkNextStep.headU == UartBufferS.headerTable.get("H_SET") && checkNextStep.keyU == UartBufferS.controlstatusTable.get("NEXT_ANIMATION")) {
            System.out.println("OK step animation");
        } else {
//            JOptionPane.showMessageDialog(frame, "Cannot connect with Controller", "Error", JOptionPane.ERROR_MESSAGE);
//            System.exit(0);
            System.err.println("Next Animation Error!!");
        }
    }

    private void WaitForNextAnimation() {
        UartBufferS checkNextStep = CommunicateController.WaitDataFromController();
        if (checkNextStep.headU == UartBufferS.headerTable.get("H_SET") && checkNextStep.keyU == UartBufferS.controlstatusTable.get("NEXT_ANIMATION")) {
            System.out.println("OK step animation");
        } else {
            JOptionPane.showMessageDialog(frame, "Cannot connect with Controller", "Error", JOptionPane.ERROR_MESSAGE);
//            System.exit(0);
        }
    }

}
