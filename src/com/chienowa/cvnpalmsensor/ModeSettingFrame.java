/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.UartBufferS;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

/**
 *
 * @author minhg
 */
public class ModeSettingFrame extends CvnFrame implements ActionListener {

    private CvnButton btnBaseMode;
    private CvnButton btnAcidMode;
    private CvnButton btnWatterMode;
    private CvnButton btnHandMode;
    private CvnButton btnReturn;
    private CvnButton btnSave;
    private final ManagementMenuFrame mainframe;

    public ThreadAlarm alarmModeSettingFrame;
    private ClockLabel clockLabel = null;

    public int currentMode;

    private final short XBTNPOSITION = 400;
    private final short YBTNPOSITION = 140;
    private final short YBTNSPACE = 80;

    public JLabel labelMode;

    public ModeSettingFrame(ManagementMenuFrame frame) {
        mainframe = frame;
        InitGUI();
        CheckCurrentMode();
        ThreadModeSetting t1 = new ThreadModeSetting(this);
        t1.start();
    }

    private void InitGUI() {

        txtCaption.setText("SETTING MODE FOR MACHINE");

        CvnLabel label1 = new CvnLabel("Current Mode: ");
        label1.setBounds(XBTNPOSITION, 60, 200, 60);
        getContentPane().add(label1);

        //Create buttons
        labelMode = new JLabel();
        labelMode.setFont(new Font("MS UI Gothic", Font.BOLD, 25));
        labelMode.setForeground(Color.red);
        labelMode.setBounds(XBTNPOSITION + 200, 60, 300, 60);
        labelMode.setText("ACID");
        getContentPane().add(labelMode);

        btnHandMode = new CvnButton();
        btnHandMode.setText("Hand Mode");
        btnHandMode.setBounds(XBTNPOSITION, YBTNPOSITION + YBTNSPACE * 0, 224, 60);
        getContentPane().add(btnHandMode);
        btnHandMode.addActionListener(this);

        btnWatterMode = new CvnButton();
        btnWatterMode.setText("Water Mode");
        btnWatterMode.setBounds(XBTNPOSITION, YBTNPOSITION + YBTNSPACE * 1, 224, 60);
        getContentPane().add(btnWatterMode);
        btnWatterMode.addActionListener(this);

        btnAcidMode = new CvnButton();
        btnAcidMode.setText("Acid Mode");
        btnAcidMode.setBounds(XBTNPOSITION, YBTNPOSITION + YBTNSPACE * 2, 224, 60);
        getContentPane().add(btnAcidMode);
        btnAcidMode.addActionListener(this);

        btnBaseMode = new CvnButton();
        btnBaseMode.setText("Base Mode");
        btnBaseMode.setBounds(XBTNPOSITION, YBTNPOSITION + YBTNSPACE * 3, 224, 60);
        getContentPane().add(btnBaseMode);
        btnBaseMode.addActionListener(this);

        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 200, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(this);

        btnSave = new CvnButton();
        btnSave.setText("Save");
        btnSave.setBounds(250, 530, 200, 57);
        getContentPane().add(btnSave);
        btnSave.addActionListener(this);

        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);
    }

    public void CheckCurrentMode() {
        switch (currentMode) {
            case 1:
                labelMode.setText("HAND");
                labelMode.setForeground(Color.PINK);
                SetActiveButton(btnHandMode);
                break;
            case 2:
                labelMode.setText("WATER");
                labelMode.setForeground(Color.GREEN);
                SetActiveButton(btnWatterMode);
                break;
            case 3:
                labelMode.setText("ACID");
                labelMode.setForeground(Color.RED);
                SetActiveButton(btnAcidMode);
                break;
            case 4:
                labelMode.setText("ALKALINE");
                labelMode.setForeground(Color.BLUE);
                SetActiveButton(btnBaseMode);
                break;
            default:
                System.out.println("Mode is not valid");
        }
    }

    private void SetActiveButton(JButton button) {
        Color normalBg = new Color(238, 238, 238);

        //set normal background for button
        btnBaseMode.setBackground(normalBg);
        btnBaseMode.setForeground(Color.BLACK);
        btnAcidMode.setBackground(normalBg);
        btnAcidMode.setForeground(Color.BLACK);
        btnWatterMode.setBackground(normalBg);
        btnWatterMode.setForeground(Color.BLACK);
        btnHandMode.setBackground(normalBg);
        btnHandMode.setForeground(Color.BLACK);

        button.setBackground(Color.GRAY);
        button.setForeground(Color.WHITE);
    }

    private void SaveCurrentMode() {
        StopBackgroundRunning();
        if (currentMode > 0) {
            UartBufferS sendUart = new UartBufferS(4);
            sendUart.headU = UartBufferS.headerTable.get("H_SET");
            sendUart.keyU = UartBufferS.controlstatusTable.get("WASHING_MODE");
            sendUart.contentArray = ConvertByteInt.intToByteArray(currentMode);
            if (CommunicateController.SendDataToController(sendUart)) {
                JOptionPane.showMessageDialog(this, "Save Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Error! Cannot save data to Controller", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Error! Mode is not valid", "Error", JOptionPane.ERROR_MESSAGE);
        }
        StartBackgroundRunning();
    }

    public void ReturnMainFrame() {
        StopBackgroundRunning();
        this.dispose();
        this.setVisible(false);
        mainframe.setVisible(true);
        mainframe.StartBackgroundRunning();
    }

    public void StartBackgroundRunning() {
        alarmModeSettingFrame = new ThreadAlarm(this);
        alarmModeSettingFrame.start();
        clockLabel.timerTimeandDate.start();
    }

    public void StopBackgroundRunning() {
        alarmModeSettingFrame.stop();
        clockLabel.timerTimeandDate.stop();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnHandMode)) {
            currentMode = 1;
            CheckCurrentMode();
        }
        if (obj.equals(btnWatterMode)) {
            currentMode = 2;
            CheckCurrentMode();
        }
        if (obj.equals(btnAcidMode)) {
            currentMode = 3;
            CheckCurrentMode();
        }
        if (obj.equals(btnBaseMode)) {
            currentMode = 4;
            CheckCurrentMode();
        }
        if (obj.equals(btnSave)) {
            SaveCurrentMode();
        }
        if (obj.equals(btnReturn)) {
            ReturnMainFrame();
        }
    }

}