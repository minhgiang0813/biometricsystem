/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.UartBufferS;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class ThreadTimeSettingFrame extends Thread {

    private boolean stopRunning = false;
    private UartBufferS data;

    private TimeSettingFrame frame;
  

    public ThreadTimeSettingFrame(TimeSettingFrame mainframe) {
      
        this.frame = mainframe;
        data = new UartBufferS(4);
        
        System.out.println("Init thread number setting");
        
//        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
//        System.out.println(threadSet);

    }

    @Override
    public void run() {
        DataFromController.GetTimeFromController();
//        
        int[] initValueR = DataFromController.dataTimeIntArray;
        int[] initValue = new int[initValueR.length];
        for (int i = 0; i < initValueR.length; i++) {
            initValue[i] = initValueR[initValueR.length - 1 - i];
        }
//
        frame.input01.setText(String.valueOf(initValue[0]));
        frame.input02.setText(String.valueOf(initValue[1]));
        frame.input03.setText(String.valueOf(initValue[2]));
        frame.input04.setText(String.valueOf(initValue[3]));
        frame.input05.setText(String.valueOf(initValue[4]));
        frame.input06.setText(String.valueOf(initValue[5]));
        frame.input07.setText(String.valueOf(initValue[6]));
        frame.input08.setText(String.valueOf(initValue[7]));
        frame.input09.setText(String.valueOf(initValue[8]));
        frame.input10.setText(String.valueOf(initValue[9]));
        frame.input11.setText(String.valueOf(initValue[10]));
        frame.input12.setText(String.valueOf(initValue[11]));
        frame.input13.setText(String.valueOf(initValue[12]));
        frame.input14.setText(String.valueOf(initValue[13]));
        frame.input15.setText(String.valueOf(initValue[14]));
        frame.input16.setText(String.valueOf(initValue[15]));
        frame.input17.setText(String.valueOf(initValue[16]));
        frame.input18.setText(String.valueOf(initValue[17]));
        frame.input19.setText(String.valueOf(initValue[18]));
        frame.input20.setText(String.valueOf(initValue[19]));
        frame.input21.setText(String.valueOf(initValue[20]));
        frame.input22.setText(String.valueOf(initValue[21]));
        frame.input23.setText(String.valueOf(initValue[22]));
        frame.input24.setText(String.valueOf(initValue[23]));
        frame.input25.setText(String.valueOf(initValue[24]));
        frame.input26.setText(String.valueOf(initValue[25]));
        frame.input27.setText(String.valueOf(initValue[26]));
        frame.input28.setText(String.valueOf(initValue[27]));
        frame.input29.setText(String.valueOf(initValue[28]));
        frame.input30.setText(String.valueOf(initValue[29]));
        frame.input31.setText(String.valueOf(initValue[30]));
        frame.input32.setText(String.valueOf(initValue[31]));
        frame.input33.setText(String.valueOf(initValue[32]));
        frame.input34.setText(String.valueOf(initValue[33]));
        frame.input35.setText(String.valueOf(initValue[34]));
        frame.input36.setText(String.valueOf(initValue[35]));
        frame.input37.setText(String.valueOf(initValue[36]));
//        frame.input38.setText(String.valueOf(initValue[37]));
//        frame.input39.setText(String.valueOf(initValue[38]));
//        frame.input40.setText(String.valueOf(initValue[39]));

        frame.alarmSystemManagement = new ThreadAlarm(frame);
        frame.alarmSystemManagement.start();

    }

    public void stopListeningThread() {
        SerialUartProcess.ClosePort();
        this.stopRunning = true;
    }

}
