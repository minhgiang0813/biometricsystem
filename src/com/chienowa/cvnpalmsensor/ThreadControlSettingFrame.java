/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.DataFromController;

/**
 *
 * @author minhg
 */
public class ThreadControlSettingFrame extends Thread {

    private ControlSettingFrame frame;

    public ThreadControlSettingFrame(ControlSettingFrame mainframe) {
        frame = mainframe;
        System.out.println("Init value");
    }

    @Override
    public void run() {
        GetCurrentDataFromController();
        frame.threadAlarmControlSettingFrame = new ThreadAlarm(frame);
        frame.threadAlarmControlSettingFrame.start();
    }

    public void GetCurrentDataFromController() {
        int[]initData = DataFromController.GetControlSettingFromController();
        
        int intDrantMode = initData[0];
        if (intDrantMode == 1) {
            frame.SetDrainageMode(true);
        } else {
            frame.SetDrainageMode(false);
        }
        int intTestPowerMode = initData[1];
        if (intTestPowerMode == 1) {
            frame.SetTestPowerOnMode(true);
        } else {
            frame.SetTestPowerOnMode(false);
        }
        int intWaterFilter = initData[2];
        if (intWaterFilter == 1) {
            frame.SetWaterFilter(true);
        } else {
            frame.SetWaterFilter(false);
        }
        int intBioMode = initData[3];
        if (intBioMode == 1) {
            frame.SetBioMode(true);
        } else {
            frame.SetBioMode(false);
        }
    }

}
