/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.UartBufferS;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import java.awt.Color;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.BitSet;
import javax.swing.JButton;
import javax.swing.JSeparator;

/**
 *
 * @author minhg
 */
public class TestModeFrame extends CvnFrame implements ActionListener {

    private CvnButton btnSV1;
    private boolean vlSV1 = false;
    private CvnButton btnSV2;
    private boolean vlSV2 = false;
    private CvnButton btnSV3;
    private boolean vlSV3 = false;
    private CvnButton btnSV4;
    private boolean vlSV4 = false;
    private CvnButton btnSV5;
    private boolean vlSV5 = false;
    private CvnButton btnSV6;
    private boolean vlSV6 = false;
    private CvnButton btnSV7;
    private boolean vlSV7 = false;
    private CvnButton btnSV8;
    private boolean vlSV8 = false;
    private CvnButton btnSV9;
    private boolean vlSV9 = false;
    private CvnButton btnRSVD;
    private boolean vlRSVD = false;
    private CvnButton btnRSVD1;
    private boolean vlRSVD1 = false;
    private CvnButton btnPump1;
    private boolean vlPump1 = false;
    private CvnButton btnPump2;
    private boolean vlPump2 = false;
    private CvnButton btnSaltPump;
    private boolean vlSaltPump = false;
    private CvnButton btnCVCC_ON;
    private boolean vlCVCC_ON = false;
//    private final PsMainFrame mainframe;
    private CvnButton btnReturn;
    private CvnButton btnTestIndividual;

    private boolean testIndividualMode;

    private final short XBTNPOSITION = 10;
    private final short YBTNPOSITION = 10;
    private final short YBTNSPACE = 70;
    private final short XBTNSPACE = 250;
    private final short BTNWIDTH = 150;
    private final short BTNHEIGHT = 55;

    private Panel panelTestingIndividual;
    private ManagementMenuFrame mainframe;

    public TestModeFrame(ManagementMenuFrame frame) {
        mainframe = frame;
        InitGUI();
        testIndividualMode = false;
        panelTestingIndividual.setVisible(false);
        StartTestingMode();

    }

    private void InitGUI() {

        txtCaption.setText("TESTING MODE");

        panelTestingIndividual = new Panel();
        panelTestingIndividual.setBounds(0, 50, 1024, 449);
        panelTestingIndividual.setLayout(null);
        getContentPane().add(panelTestingIndividual);

        btnSV1 = new CvnButton();
        btnSV1.setText("SV1");
        btnSV1.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnSV1.addActionListener(this);
        panelTestingIndividual.add(btnSV1);

        btnSV2 = new CvnButton();
        btnSV2.setText("SV2");
        btnSV2.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnSV2.addActionListener(this);
        panelTestingIndividual.add(btnSV2);

        btnSV3 = new CvnButton();
        btnSV3.setText("SV3");
        btnSV3.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        btnSV3.addActionListener(this);
        panelTestingIndividual.add(btnSV3);

        btnSV4 = new CvnButton();
        btnSV4.setText("SV4");
        btnSV4.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        btnSV4.addActionListener(this);
        panelTestingIndividual.add(btnSV4);

        btnSV5 = new CvnButton();
        btnSV5.setText("SV5");
        btnSV5.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 4, BTNWIDTH, BTNHEIGHT);
        btnSV5.addActionListener(this);
        panelTestingIndividual.add(btnSV5);

        btnSV6 = new CvnButton();
        btnSV6.setText("SV6");
        btnSV6.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 5, BTNWIDTH, BTNHEIGHT);
        btnSV6.addActionListener(this);
        panelTestingIndividual.add(btnSV6);

        btnSV7 = new CvnButton();
        btnSV7.setText("SV7");
        btnSV7.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnSV7.addActionListener(this);
        panelTestingIndividual.add(btnSV7);

        btnSV8 = new CvnButton();
        btnSV8.setText("SV8");
        btnSV8.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnSV8.addActionListener(this);
        panelTestingIndividual.add(btnSV8);

        btnSV9 = new CvnButton();
        btnSV9.setText("SV9");
        btnSV9.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        btnSV9.addActionListener(this);
        panelTestingIndividual.add(btnSV9);

        btnRSVD = new CvnButton();
        btnRSVD.setText("RSVD");
        btnRSVD.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        btnRSVD.addActionListener(this);
        panelTestingIndividual.add(btnRSVD);

        btnRSVD1 = new CvnButton();
        btnRSVD1.setText("RSVD1");
        btnRSVD1.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 4, BTNWIDTH, BTNHEIGHT);
        btnRSVD1.addActionListener(this);
        panelTestingIndividual.add(btnRSVD1);

        btnPump1 = new CvnButton();
        btnPump1.setText("Pump1");
        btnPump1.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 5, BTNWIDTH, BTNHEIGHT);
        btnPump1.addActionListener(this);
        panelTestingIndividual.add(btnPump1);

        btnPump2 = new CvnButton();
        btnPump2.setText("Pump2");
        btnPump2.setBounds(XBTNPOSITION + XBTNSPACE * 2, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnPump2.addActionListener(this);
        panelTestingIndividual.add(btnPump2);

        btnSaltPump = new CvnButton();
        btnSaltPump.setText("SaltPump");
        btnSaltPump.setBounds(XBTNPOSITION + XBTNSPACE * 2, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnSaltPump.addActionListener(this);
        panelTestingIndividual.add(btnSaltPump);

        btnCVCC_ON = new CvnButton();
        btnCVCC_ON.setText("CVCC_ON");
        btnCVCC_ON.setBounds(XBTNPOSITION + XBTNSPACE * 2, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        btnCVCC_ON.addActionListener(this);
        panelTestingIndividual.add(btnCVCC_ON);

//
        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 200, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(this);

        btnTestIndividual = new CvnButton();
        btnTestIndividual.setText("Test Individual");
        btnTestIndividual.setBounds(250, 530, 200, 57);
        SetNormalButton(btnTestIndividual);
        getContentPane().add(btnTestIndividual);
        btnTestIndividual.addActionListener(this);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);
    }

    private void SwitchTestIndividual() {
        if (!testIndividualMode) {
            testIndividualMode = true;
            panelTestingIndividual.setVisible(true);
            SetActiveButton(btnTestIndividual);
        } else {
            testIndividualMode = false;
            panelTestingIndividual.setVisible(false);

            System.out.println("Stop Test Individual");
            SetNormalButton(btnTestIndividual);

        }

    }

    private void StartTestingMode() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TESTING_MODE_START");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            System.out.println("Enter testing mode");
        } else {
            System.out.println("Problem when exit testing mode");
            System.exit(0);
        }
    }

    private void StopTestingMode() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TESTING_MODE_STOP");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            System.out.println("Exit testing mode");
        } else {
            System.out.println("Problem when enter testing mode");
            System.exit(0);
        }
    }

    private void SetNormalButton(JButton button) {
        Color normalBg = new Color(238, 238, 238);
        button.setBackground(normalBg);
        button.setForeground(Color.BLACK);
    }

    private void SetActiveButton(JButton button) {
        button.setBackground(Color.GRAY);
        button.setForeground(Color.WHITE);
    }

    public void ReturnMainFrame() {
        StopTestingMode();
        this.dispose();
        this.setVisible(false);
        mainframe.setVisible(true);
        mainframe.StartBackgroundRunning();
    }

    private void SendDataToController() {
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_SET");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("TEST_INDIVIDUAL");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};
        if (CommunicateController.SendDataToController(sendBuffer)) {
            BitSet bitset = new BitSet(25);
            bitset.set(0, vlSV1);
            bitset.set(1, vlSV2);
            bitset.set(2, vlSV3);
            bitset.set(3, vlSV4);
            bitset.set(4, vlSV5);
            bitset.set(5, vlSV6);
            bitset.set(6, vlSV7);
            bitset.set(7, vlSV8);
            bitset.set(8, vlSV9);
            bitset.set(9, false);
            bitset.set(10, false);
            bitset.set(11, false);
            bitset.set(12, false);
            bitset.set(13, false);
            bitset.set(14, false);
            bitset.set(15, vlRSVD);
            bitset.set(16, vlPump1);
            bitset.set(17, vlPump2);
            bitset.set(18, vlSaltPump);
            bitset.set(19, vlCVCC_ON);
            bitset.set(20, false);
            bitset.set(21, false);
            bitset.set(22, false);
            bitset.set(23, vlRSVD1);
            bitset.set(24, true);
            bitset.set(25, true);
            bitset.set(26, true);
            bitset.set(27, true);
//            bitset.set(15,false);

            byte[] sendArray = bitset.toByteArray();

            SerialUartProcess.SendByteArray(sendArray);

            BitSet bitsetS = BitSet.valueOf(sendArray);
            for (int i = 0; i < bitsetS.length(); i++) {
                System.out.print("| Value " + i + ":" + bitset.get(i));
            }

        } else {
            System.out.println("Cannot get respond from Controller");
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnTestIndividual)) {
            SwitchTestIndividual();
        }
        if (obj.equals(btnReturn)) {
            ReturnMainFrame();
        }
        if (obj.equals(btnSV1)) {
            vlSV1 = !vlSV1;
            if (vlSV1) {
                SetActiveButton(btnSV1);
            } else {
                SetNormalButton(btnSV1);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV2)) {
            vlSV2 = !vlSV2;
            if (vlSV2) {
                SetActiveButton(btnSV2);
            } else {
                SetNormalButton(btnSV2);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV3)) {
            vlSV3 = !vlSV3;
            if (vlSV3) {
                SetActiveButton(btnSV3);
            } else {
                SetNormalButton(btnSV3);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV4)) {
            vlSV4 = !vlSV4;
            if (vlSV4) {
                SetActiveButton(btnSV4);
            } else {
                SetNormalButton(btnSV4);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV5)) {
            vlSV5 = !vlSV5;
            if (vlSV5) {
                SetActiveButton(btnSV5);
            } else {
                SetNormalButton(btnSV5);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV6)) {
            vlSV6 = !vlSV6;
            if (vlSV6) {
                SetActiveButton(btnSV6);
            } else {
                SetNormalButton(btnSV6);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV7)) {
            vlSV7 = !vlSV7;
            if (vlSV7) {
                SetActiveButton(btnSV7);
            } else {
                SetNormalButton(btnSV7);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV8)) {
            vlSV8 = !vlSV8;
            if (vlSV8) {
                SetActiveButton(btnSV8);
            } else {
                SetNormalButton(btnSV8);
            }
            SendDataToController();
        }
        if (obj.equals(btnSV9)) {
            vlSV9 = !vlSV9;
            if (vlSV9) {
                SetActiveButton(btnSV9);
            } else {
                SetNormalButton(btnSV9);
            }
            SendDataToController();
        }
        if (obj.equals(btnRSVD)) {
            vlRSVD = !vlRSVD;
            if (vlRSVD) {
                SetActiveButton(btnRSVD);
            } else {
                SetNormalButton(btnRSVD);
            }
            SendDataToController();
        }
        if (obj.equals(btnRSVD1)) {
            vlRSVD1 = !vlRSVD1;
            if (vlRSVD1) {
                SetActiveButton(btnRSVD1);
            } else {
                SetNormalButton(btnRSVD1);
            }
            SendDataToController();
        }
        if (obj.equals(btnPump1)) {
            vlPump1 = !vlPump1;
            if (vlPump1) {
                SetActiveButton(btnPump1);
            } else {
                SetNormalButton(btnPump1);
            }
            SendDataToController();
        }
        if (obj.equals(btnPump2)) {
            vlPump2 = !vlPump2;
            if (vlPump2) {
                SetActiveButton(btnPump2);
            } else {
                SetNormalButton(btnPump2);
            }
            SendDataToController();
        }
        if (obj.equals(btnSaltPump)) {
            vlSaltPump = !vlSaltPump;
            if (vlSaltPump) {
                SetActiveButton(btnSaltPump);
            } else {
                SetNormalButton(btnSaltPump);
            }
            SendDataToController();
        }
        if (obj.equals(btnCVCC_ON)) {
            vlCVCC_ON = !vlCVCC_ON;
            if (vlCVCC_ON) {
                SetActiveButton(btnCVCC_ON);
            } else {
                SetNormalButton(btnCVCC_ON);
            }
            SendDataToController();
        }
    }

}
