/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.nonbio.NonSensorMainFrame;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.SerialUartProcess;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import com.fujitsu.frontech.palmsecure_smpl.PsSampleApl;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author minhg
 */
public class MachineWorking extends CvnFrame {

    public static int washingStep;

    private static final int PS_BTN_WIDTH = 121;
    private static final int PS_BTN_HEIGHT = 57;
    private static final int LABEL_WIDTH = 100;
    private static final int LABEL_HEIGHT = 57;

//    private CvnButton quitButton = null;
    private CvnLabel label1;
    public CvnLabel label2;
    public CvnLabel label3;
    public CvnLabel label4;

    public CvnLabel acidLabel;
    public CvnLabel baseLabel;
    public CvnLabel waterLabel;

    public JPanel blueSquare;
    public JPanel magentaSquare;
    public JPanel blackSquare;
    public ThreadRunningUart T1;

//    public final PsMainFrame mainFrame;
    public MachineWorking() {
        washingStep = 0;
        System.out.println("Enter maching working screen");
        InitUI();

//        mainFrame = frame;
        StartThread();

    }

    public void StartThread() {
        T1 = new ThreadRunningUart("Connect with controller by UART", this);
        T1.start();
    }

    private void InitUI() {
        // Set label
        label1 = new CvnLabel("START WASHING YOUR HAND - (follow instruction)");
        label1.setBounds(50, 55, 400, LABEL_HEIGHT);
        getContentPane().add(label1);

        label3 = new CvnLabel("Washing with base liquid");
        label3.setBounds(150, 150, 300, LABEL_HEIGHT);
        getContentPane().add(label3);

        label2 = new CvnLabel("Washing with acid liquid");
        label2.setBounds(150, 250, 300, LABEL_HEIGHT);
        getContentPane().add(label2);

        label4 = new CvnLabel("Washing with water");
        label4.setBounds(150, 350, 300, LABEL_HEIGHT);
        getContentPane().add(label4);

        baseLabel = new CvnLabel("15");
        baseLabel.setBounds(400, 150, LABEL_WIDTH, LABEL_HEIGHT);
        getContentPane().add(baseLabel);

        acidLabel = new CvnLabel("15");
        acidLabel.setBounds(400, 250, LABEL_WIDTH, LABEL_HEIGHT);
        getContentPane().add(acidLabel);

        waterLabel = new CvnLabel("15");
        waterLabel.setBounds(400, 350, LABEL_WIDTH, LABEL_HEIGHT);
        getContentPane().add(waterLabel);

        // Set square
        blueSquare = new JPanel();
        blueSquare.setBounds(50, 150, 50, 50);
        blueSquare.setBackground(Color.blue);
        blueSquare.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f), Color.BLUE));
        getContentPane().add(blueSquare);

        magentaSquare = new JPanel();
        magentaSquare.setBounds(50, 250, 50, 50);
        magentaSquare.setBackground(Color.MAGENTA);
        magentaSquare.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f), Color.MAGENTA));
        getContentPane().add(magentaSquare);

        blackSquare = new JPanel();
        blackSquare.setBounds(50, 350, 50, 50);
        blackSquare.setBackground(Color.BLACK);
        blackSquare.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f), Color.BLACK));
        getContentPane().add(blackSquare);
    }

    public void CloseMachineFrame() throws IOException {

        if (PsSampleApl.bioMode == 1) {

            PsMainFrame mainFrame = new PsMainFrame();
            boolean result = mainFrame.Ps_Sample_Apl_Java();
            if (result != true) {
                System.exit(0);
            }
            mainFrame.setVisible(true);

        } else {
            new NonSensorMainFrame().setVisible(true);
        }
        this.dispose();
        this.setVisible(false);

    }

}
