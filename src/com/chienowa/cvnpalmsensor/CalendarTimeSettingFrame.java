/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.customui.CvnLabel;
import com.chienowa.customui.CvnTextField;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.ulti.CvnCurrentDateTime;
import com.chienowa.ulti.VirtualKeyboard;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSeparator;
import javax.swing.Timer;

/**
 *
 * @author minhg
 */
public class CalendarTimeSettingFrame extends CvnFrame implements ActionListener {

    private ManagementMenuFrame mainmenuframe;
    private ThreadAlarm threadAlarmCalendarTimeSetting;
    private Timer timerTimeandDate;
    private CvnButton btnReturn;
    private CvnButton btnApply;
    private CvnLabel lblCurrentDate = null;
    private CvnLabel lblCurrentTime = null;
    
    private CvnCurrentDateTime current;

    private CvnTextField tfYYYY = null;
    private CvnTextField tfMM = null;
    private CvnTextField tfDD = null;
    private CvnTextField tfHour = null;
    private CvnTextField tfMinute = null;

    private static final String TIME_FORMATTER = "HH:mm:ss";
    private static final String DATE_FORMATTER = "yyyy-MM-dd";

    private final short XPOSITION = 100;
    private final short YPOSITION = 70;
    private final short YSPACE = 70;
    private final short XSPACE = 350;
    private final short LBLWIDTH = 300;
    private final short LBLHEIGHT = 60;

    private final short TFWIDTH = 55;
    private final short TFHEIGHT = 60;
    private final short XTFSPACE = 60;
    
    

    public CalendarTimeSettingFrame(ManagementMenuFrame frame) throws IOException {
        mainmenuframe = frame;
        InitGUI();
        threadAlarmCalendarTimeSetting = new ThreadAlarm(this);
        threadAlarmCalendarTimeSetting.start();

        timerTimeandDate = new Timer(1000, this);
        timerTimeandDate.start();
    }

    private void InitGUI() throws IOException {
        
        Font labelFont = new Font("MS UI Gothic", Font.PLAIN, 30);
        txtCaption.setText("CALENDAR TIME SETTING");
        VirtualKeyboard.Show();

        CvnLabel lblDate = new CvnLabel("Current Date");
        lblDate.setFont(labelFont);
        lblDate.setBounds(XPOSITION + XSPACE * 0, YPOSITION + YSPACE * 0, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblDate);

        lblCurrentDate = new CvnLabel("Date");
        lblCurrentDate.setFont(labelFont);
        lblCurrentDate.setBounds(XPOSITION + XSPACE * 0, YPOSITION + YSPACE * 1, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblCurrentDate);

        CvnLabel lblTime = new CvnLabel("Current Time");
        lblTime.setFont(labelFont);
        lblTime.setBounds(XPOSITION + XSPACE * 1, YPOSITION + YSPACE * 0, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblTime);

        lblCurrentTime = new CvnLabel("Time");
        lblCurrentTime.setFont(labelFont);
        lblCurrentTime.setBounds(XPOSITION + XSPACE * 1, YPOSITION + YSPACE * 1, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblCurrentTime);

        CvnLabel lblChangeTo = new CvnLabel("New Date");
        lblChangeTo.setFont(labelFont);
        lblChangeTo.setBounds(XPOSITION + XSPACE * 0, YPOSITION + YSPACE * 2, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblChangeTo);

        tfYYYY = new CvnTextField();
        tfYYYY.setFont(labelFont);
        tfYYYY.setBounds(XPOSITION + XSPACE * 0 + XTFSPACE * 0, YPOSITION + YSPACE * 3, TFWIDTH * 2, TFHEIGHT);
        getContentPane().add(tfYYYY);

        tfMM = new CvnTextField();
        tfMM.setFont(labelFont);
        tfMM.setBounds(XPOSITION + XSPACE * 0 + XTFSPACE * 1 + TFWIDTH, YPOSITION + YSPACE * 3, TFWIDTH, TFHEIGHT);
        getContentPane().add(tfMM);

        tfDD = new CvnTextField();
        tfDD.setFont(labelFont);
        tfDD.setBounds(XPOSITION + XSPACE * 0 + XTFSPACE * 2 + TFWIDTH, YPOSITION + YSPACE * 3, TFWIDTH, TFHEIGHT);
        getContentPane().add(tfDD);
        
        current = new CvnCurrentDateTime();

        String txtYear = current.GetYear();
        String txtMonth = current.GetMonth();
        String txtDate = current.GetDate();

        tfYYYY.setText(txtYear);
        tfMM.setText(txtMonth);
        tfDD.setText(txtDate);

        CvnLabel lblChangeTo1 = new CvnLabel("New Time");
        lblChangeTo1.setFont(labelFont);
        lblChangeTo1.setBounds(XPOSITION + XSPACE * 1, YPOSITION + YSPACE * 2, LBLWIDTH, LBLHEIGHT);
        getContentPane().add(lblChangeTo1);

        tfHour = new CvnTextField();
        tfHour.setFont(labelFont);
        tfHour.setBounds(XPOSITION + XSPACE * 1 + XTFSPACE * 0, YPOSITION + YSPACE * 3, TFWIDTH, TFHEIGHT);
        getContentPane().add(tfHour);

        tfMinute = new CvnTextField();
        tfMinute.setFont(labelFont);
        tfMinute.setBounds(XPOSITION + XSPACE * 1 + XTFSPACE * 1, YPOSITION + YSPACE * 3, TFWIDTH, TFHEIGHT);
        getContentPane().add(tfMinute);
        
        String txtHour = current.GetHour();
        String txtMinute = current.GetMinutes();

                
        tfHour.setText(txtHour);
        tfMinute.setText(txtMinute);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);

        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 200, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ReturnMainMenuFrame();
            }
        });
        
        btnApply = new CvnButton();
        btnApply.setText("Apply");
        btnApply.setBounds(250, 530, 200, 57);
        getContentPane().add(btnApply);
        btnApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ApplyNewDateTime();
                } catch (ParseException ex) {
                    Logger.getLogger(CalendarTimeSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CalendarTimeSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    private void ApplyNewDateTime() throws ParseException, IOException{
        String txtYear = tfYYYY.getText();
        String txtMonth = tfMM.getText();
        String txtDate = tfDD.getText();
        String txtHour = tfHour.getText();
        String txtMinute = tfMinute.getText();
        
        String stringF = txtYear+"-"+txtMonth+"-"+txtDate+" "+txtHour+":"+txtMinute+":00";
        System.out.println(stringF);
        
        Date dateNew=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(stringF);
        
        long adjustNumber = dateNew.getTime() - System.currentTimeMillis();
        
        current.SetNewAdjustTime(adjustNumber);
   
    }

    private void ReturnMainMenuFrame() {
        VirtualKeyboard.Hide();
        threadAlarmCalendarTimeSetting.stop();
        timerTimeandDate.stop();
        this.dispose();
        this.setVisible(false);
        mainmenuframe.setVisible(true);
        mainmenuframe.StartBackgroundRunning();
        timerTimeandDate.stop();
        System.out.println("Stop timer - Return to Menu screen");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            GetCurrentDateTime();
        } catch (IOException ex) {
            Logger.getLogger(CalendarTimeSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void GetCurrentDateTime() throws IOException {
        
        current = new CvnCurrentDateTime();

        LocalDateTime localDateTime = current.GetLocalDateTime();
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern(TIME_FORMATTER);
        String formatTime = localDateTime.format(stringTime_formatter);
        lblCurrentTime.setText(formatTime);
        
        DateTimeFormatter stringDate_formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDate = localDateTime.format(stringDate_formatter);
        lblCurrentDate.setText(formatDate);
        
    }

}
