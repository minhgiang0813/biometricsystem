/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.customui.CvnButton;
import com.chienowa.customui.CvnFrame;
import com.chienowa.nonbio.NonSensorMainFrame;
import com.chienowa.runbackground.ClockLabel;
import com.chienowa.runbackground.ThreadAlarm;
import com.chienowa.systemstatusanimation.TestingModeAniFrame;
import com.chienowa.systemstatusanimation.myFrame;
import com.fujitsu.frontech.palmsecure_smpl.PsMainFrame;
import com.fujitsu.frontech.palmsecure_smpl.PsSampleApl;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSeparator;
import org.json.simple.parser.ParseException;

/**
 *
 * @author minhg
 */
public class ManagementMenuFrame extends CvnFrame implements ActionListener {

    private CvnButton btnReturn;

    public ThreadAlarm threadAlarmManagementMenuFrame;
    private CvnButton btnSystemStatus;
    private CvnButton btnTestMode;
    private CvnButton btnModeSetting;
    private CvnButton btnTimeSetting;
    private CvnButton btnNumberSetting;
    private CvnButton btnControlSetting;
    private CvnButton btnAlarmLog;
    private CvnButton btnRestartMachine;
    private CvnButton btnShutdownMachine;
    private CvnButton btnQuitApp;
    private CvnButton btnCalendarTimerSetting;
    private ClockLabel clockLabel = null;

    private final short XBTNPOSITION = 100;
    private final short YBTNPOSITION = 70;
    private final short YBTNSPACE = 100;
    private final short XBTNSPACE = 500;
    private final short BTNWIDTH = 350;
    private final short BTNHEIGHT = 70;

    private PsMainFrame bioFrame = null;
    private NonSensorMainFrame nonBioFrame = null;

    public ManagementMenuFrame() {
        InitGUI();
        threadAlarmManagementMenuFrame = new ThreadAlarm(this);
        threadAlarmManagementMenuFrame.start();
    }
    
    public void StartBackgroundRunning() {
        threadAlarmManagementMenuFrame = new ThreadAlarm(this);
        threadAlarmManagementMenuFrame.start();
        clockLabel.timerTimeandDate.start();
    }
    
    public void StopBackgroundRunning(){
        threadAlarmManagementMenuFrame.stop();
        clockLabel.timerTimeandDate.stop();
    }

    public void SetBioMainFrame(PsMainFrame frame) {
        bioFrame = frame;
    }

    public void SetNonBioMainFrame(NonSensorMainFrame frame) {
        nonBioFrame = frame;
    }

    private void InitGUI() {

        txtCaption.setText("SYSTEM MANAGEMENT");
        
        clockLabel = new ClockLabel();
        getContentPane().add(clockLabel);

        btnSystemStatus = new CvnButton();
        btnSystemStatus.setText("System status");
        btnSystemStatus.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnSystemStatus.addActionListener(this);
        getContentPane().add(btnSystemStatus);

        btnTestMode = new CvnButton();
        btnTestMode.setText("Test Mode");
        btnTestMode.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnTestMode.addActionListener(this);
        getContentPane().add(btnTestMode);

        btnModeSetting = new CvnButton();
        btnModeSetting.setText("Mode Setting");
        btnModeSetting.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        btnModeSetting.addActionListener(this);
        getContentPane().add(btnModeSetting);

        btnTimeSetting = new CvnButton();
        btnTimeSetting.setText("Time Setting");
        btnTimeSetting.setBounds(XBTNPOSITION + XBTNSPACE * 0, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        btnTimeSetting.addActionListener(this);
        getContentPane().add(btnTimeSetting);

        btnNumberSetting = new CvnButton();
        btnNumberSetting.setText("Number Setting");
        btnNumberSetting.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 0, BTNWIDTH, BTNHEIGHT);
        btnNumberSetting.addActionListener(this);
        getContentPane().add(btnNumberSetting);

        btnControlSetting = new CvnButton();
        btnControlSetting.setText("Control Setting");
        btnControlSetting.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 1, BTNWIDTH, BTNHEIGHT);
        btnControlSetting.addActionListener(this);
        getContentPane().add(btnControlSetting);

        btnAlarmLog = new CvnButton();
        btnAlarmLog.setText("Alarm Log");
        btnAlarmLog.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 2, BTNWIDTH, BTNHEIGHT);
        btnAlarmLog.addActionListener(this);
        getContentPane().add(btnAlarmLog);
        
        btnCalendarTimerSetting = new CvnButton();
        btnCalendarTimerSetting.setText("Calendar Setting");
        btnCalendarTimerSetting.setBounds(XBTNPOSITION + XBTNSPACE * 1, YBTNPOSITION + YBTNSPACE * 3, BTNWIDTH, BTNHEIGHT);
        btnCalendarTimerSetting.addActionListener(this);
        getContentPane().add(btnCalendarTimerSetting);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 515, 1004, 2);
        getContentPane().add(separator);

        btnReturn = new CvnButton();
        btnReturn.setText("Return");
        btnReturn.setBounds(12, 530, 250, 57);
        getContentPane().add(btnReturn);
        btnReturn.addActionListener(this);
        
        btnRestartMachine = new CvnButton();
        btnRestartMachine.setText("Restart Raspi");
        btnRestartMachine.setBounds(300, 530, 250, 57);
        getContentPane().add(btnRestartMachine);
        btnRestartMachine.addActionListener(this);
        
//        btnShutdownMachine = new CvnButton();
//        btnShutdownMachine.setText("Shutdown Raspi");
//        btnShutdownMachine.setBounds(488, 530, 200, 57);
//        getContentPane().add(btnShutdownMachine);
//        btnShutdownMachine.addActionListener(this);
        
        btnQuitApp = new CvnButton();
        btnQuitApp.setText("Quit App");
        btnQuitApp.setBounds(588, 530, 250, 57);
        getContentPane().add(btnQuitApp);
        btnQuitApp.addActionListener(this);
//
//        btnTestIndividual = new CvnButton();
//        btnTestIndividual.setText("Test Individual");
//        btnTestIndividual.setBounds(250, 530, 200, 57);
//        SetNormalButton(btnTestIndividual);
//        getContentPane().add(btnTestIndividual);
//        btnTestIndividual.addActionListener(this);
    }
    
    private void RestartMachine(){
        try {
            Runtime.getRuntime().exec("sudo reboot");
        } catch (IOException ex) {
            Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void ShutdownMachine(){
        try {
            Runtime.getRuntime().exec("sudo shutdown now");
        } catch (IOException ex) {
            Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ReturnMainFrame() {
        StopBackgroundRunning();
        this.dispose();
        this.setVisible(false);
        if (PsSampleApl.bioMode == 1) {
            bioFrame.setVisible(true);
            bioFrame.StartBackgroundRunning();
        } else {
            nonBioFrame.setVisible(true);
            nonBioFrame.StartThread();
        }

    }

    private void OpenSystemStatusFrame() throws HeadlessException, InterruptedException {
        StopBackgroundRunning();
        this.setVisible(false);

        myFrame ex = new myFrame(this);
        ex.initUI();
        ex.SetMode(1);
        ex.SetTankLevel(1, 1, 0);
//        ex.ThongSo(3000, 3000, 3000, 50, 20, 20, 30);
        ex.setLayout(null);
        ex.setUndecorated(true);
        ex.setVisible(true);
    }

    private void OpenTestModeFrame() {
        StopBackgroundRunning();
        this.setVisible(false);

        TestingModeAniFrame testModeFrame = new TestingModeAniFrame(this);
        testModeFrame.setVisible(true);
    }

    private void OpenModeSettingFrame() {
        StopBackgroundRunning();
        this.setVisible(false);

        ModeSettingFrame modeFrame = new ModeSettingFrame(this);
        modeFrame.setVisible(true);
    }

    private void OpenTimeSettingFrame() throws InterruptedException {
        StopBackgroundRunning();
        this.setVisible(false);
        TimeSettingFrame systemManagementFrame = new TimeSettingFrame(this);
        systemManagementFrame.setVisible(true);
    }

    private void OpenNumberSettingFrame() throws InterruptedException {
        StopBackgroundRunning();
        this.setVisible(false);
        NumberSettingFrame numberSettingFrame = new NumberSettingFrame(this);
        numberSettingFrame.setVisible(true);
    }

    private void OpenControlSettingFrame() throws InterruptedException {
        StopBackgroundRunning();
        this.setVisible(false);
        ControlSettingFrame controlSettingFrame = new ControlSettingFrame(this);
        controlSettingFrame.setVisible(true);
    }

    private void OpenAlarmLogFrame() throws IOException, ParseException {
        StopBackgroundRunning();
        this.setVisible(false);
        AlarmLogFrame alarmLogFrame = new AlarmLogFrame(this);
        alarmLogFrame.setVisible(true);
    }
    
    private void OpenCalendarTimeSettingFrame() throws IOException {
        StopBackgroundRunning();
        this.setVisible(false);
        CalendarTimeSettingFrame calendarTimeSettingFrame = new CalendarTimeSettingFrame(this);
        calendarTimeSettingFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(btnReturn)) {
            ReturnMainFrame();
        }
        if (obj.equals(btnSystemStatus)) {
            try {
                OpenSystemStatusFrame();
            } catch (HeadlessException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnTestMode)) {
            OpenTestModeFrame();
        }
        if (obj.equals(btnModeSetting)) {
            OpenModeSettingFrame();
        }
        if (obj.equals(btnTimeSetting)) {
            try {
                OpenTimeSettingFrame();
            } catch (InterruptedException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnNumberSetting)) {
            try {
                OpenNumberSettingFrame();
            } catch (InterruptedException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnControlSetting)) {
            try {
                OpenControlSettingFrame();
            } catch (InterruptedException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnAlarmLog)) {
            try {
                OpenAlarmLogFrame();
            } catch (IOException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj.equals(btnRestartMachine)) {
            RestartMachine();
        }
        if (obj.equals(btnShutdownMachine)) {
            ShutdownMachine();
        }    
        if (obj.equals(btnQuitApp)) {
            System.exit(0);
        }       
        if (obj.equals(btnCalendarTimerSetting)) {
            try {
                OpenCalendarTimeSettingFrame();
            } catch (IOException ex) {
                Logger.getLogger(ManagementMenuFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
