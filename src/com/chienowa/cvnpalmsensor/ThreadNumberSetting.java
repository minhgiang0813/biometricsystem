/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.cvnpalmsensor;

import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.CommunicateController;
import com.chienowa.ulti.ConvertByteInt;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.UartBufferS;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class ThreadNumberSetting extends Thread {

    private boolean stopRunning = false;
//    private UartBufferS data;

    private NumberSettingFrame frame;
    private float[] floatValue;

    public ThreadNumberSetting(NumberSettingFrame mainframe) {
        this.frame = mainframe;
//        data = new UartBufferS(4);
//        

    }

    @Override
    public void run() {
        
        floatValue = DataFromController.GetNumberSettingFromController();

        float[] floatArray = new float[floatValue.length];
        for (int i = 0; i < floatValue.length; i++) {
            floatArray[i] = floatValue[floatValue.length - 1 - i];
        }

//        System.out.printf("%.2g%n \n", floatValue[7]);
        
        frame.input01.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[0]));
        frame.input02.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[1]));
        frame.input03.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[2]));
        frame.input04.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[3]));
        frame.input05.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[4]));
        frame.input06.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[5]));
        frame.input07.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[6]));
        frame.input08.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[7]));
        frame.input09.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[8]));
        frame.input10.setText(String.format(java.util.Locale.US, "%.2g%n", floatArray[9]));
//        frame.input09.setText(String.valueOf(initValue[8]));
//        frame.input10.setText(String.valueOf(initValue[9]));
//        frame.input11.setText(String.valueOf(initValue[10]));
//        frame.input12.setText(String.valueOf(initValue[11]));
//        frame.input13.setText(String.valueOf(initValue[12]));
//        frame.input14.setText(String.valueOf(initValue[13]));
//        frame.input15.setText(String.valueOf(initValue[14]));
//        frame.input16.setText(String.valueOf(initValue[15]));
//        frame.input17.setText(String.valueOf(initValue[16]));
//        frame.input18.setText(String.valueOf(initValue[17]));
//        frame.input19.setText(String.valueOf(initValue[18]));
        
        frame.alarmNumberSettingFrame.start();

    }

    public void stopListeningThread() {
        SerialUartProcess.ClosePort();
        this.stopRunning = true;
    }

}
