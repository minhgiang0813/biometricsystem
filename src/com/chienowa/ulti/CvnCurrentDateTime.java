/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localTime;
import java.util.Date;

/**
 *
 * @author minhg
 */
public class CvnCurrentDateTime {

    private final String FILENAME = "CvnAdjustDateTime.au";
    private Date cvnCurrentDate;
    private LocalDateTime localTime;

    public CvnCurrentDateTime() throws IOException {
        String stringFromFile = GetAdjustNumberFromFile();
        long adjustNumber = Long.parseLong(stringFromFile);
        long currentNumber = System.currentTimeMillis() + adjustNumber;
        cvnCurrentDate = new Date(currentNumber);
        localTime = GetLocalDateTime();
    }

    public String GetDate() {
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern("dd");
        String txtDate = localTime.format(stringTime_formatter);
        return txtDate;
    }

    public String GetMonth() {
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern("MM");
        String txtMonth = localTime.format(stringTime_formatter);
        return txtMonth;
    }

    public String GetYear() {
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern("yyyy");
        String txtYear = localTime.format(stringTime_formatter);
        return txtYear;
    }

    public String GetHour() {
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern("HH");
        String txtHour = localTime.format(stringTime_formatter);
        return txtHour;
    }

    public String GetMinutes() {
        DateTimeFormatter stringTime_formatter = DateTimeFormatter.ofPattern("mm");
        String txtMinute = localTime.format(stringTime_formatter);
        return txtMinute;
    }

    public LocalDateTime GetLocalDateTime() {
        LocalDateTime ldt = LocalDateTime.ofInstant(cvnCurrentDate.toInstant(), ZoneId.systemDefault());
        return ldt;
    }

    private String GetAdjustNumberFromFile() throws FileNotFoundException, IOException {
        File f = new File(FILENAME);
        FileReader fr = new FileReader(f);

        BufferedReader br = new BufferedReader(fr);
        String line = null;
        line = br.readLine();
        if (line != null) {
            return line;
        }

        fr.close();
        br.close();
        return line;
    }

    public void SetNewAdjustTime(long newValue) {
        try {
            String str = String.valueOf(newValue);
            BufferedWriter writer = new BufferedWriter(new FileWriter(FILENAME));
            writer.write(str);

            writer.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
