/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

import com.chienowa.cvnpalmsensor.ControlSettingFrame;
import com.pi4j.wiringpi.Serial;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author minhg
 */
public class CommunicateController {

    private static final int RECEIVE_LENGTH = 149;
    private static final int RECEIVE_LENGTH_NUM = 41;
    private static final int WAITING_RESPOND_TIME = 600;

    public CommunicateController() {
    }

    // Send Uart Buffer Object to Controller
    public static boolean SendDataToController(UartBufferS uartBufferS) {
        boolean result = false;
        boolean stopLoop = false;
        byte[] sendArray = uartBufferS.ByteBuffer();
        byte[] respondArray = null;
        short sendTimes = 0;

        while (stopLoop == false && sendTimes < 5) {
            SerialUartProcess.SendByteArray(sendArray);
//            System.out.print("Goi goi xuong: ");
//            for (int i = 0; i < sendArray.length; i++) {
//                System.out.print(sendArray[0] + " |");
//            }
//            System.out.println(".");
            sendTimes++;
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < WAITING_RESPOND_TIME) {
                respondArray = SerialUartProcess.ReceiveOriginalData(6);
                if (respondArray != null) {
                    System.out.println("Co repond nhung ko chinh xac");
//                    System.out.println("Respond array data: ");
//                    for (int i = 0; i < respondArray.length; i++) {
//                        System.out.print(respondArray[i] + " | ");
//                    }
                    if (Arrays.equals(respondArray, sendArray)) {
                        System.out.println("Repond ok");
                        stopLoop = true;
                        result = true;
                        break;
                    }
                }
            }
        }
        if (!result) {
            System.out.println("LOST CONNECTION WITH CONTROLLER. RESTART APP!!!");
            try {
                Runtime.getRuntime().exec("java -jar PalmSecureSample_Java.jar");
            } catch (IOException ex) {
                Logger.getLogger(ControlSettingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        }

        return result;
    }

    public static boolean SaveSpecToController(byte[] dataArray) {
        boolean result = false;
        boolean stopLoop = false;
        short sendTimes = 0;

        byte[] sendArray = new byte[dataArray.length + 1];
        System.arraycopy(dataArray, 0, sendArray, 0, dataArray.length);

        byte crc8 = CRC8.calc(dataArray, dataArray.length);
        System.out.println("Crc8 value: " + crc8);
        sendArray[dataArray.length] = crc8;
        byte[] wArray = new byte[2];
        wArray[0] = UartBufferS.headerTable.get("H_SET");
        wArray[1] = UartBufferS.controlstatusTable.get("OK_ALL");
        while (stopLoop == false && sendTimes < 5) {
            SerialUartProcess.SendByteArray(sendArray);
            System.out.println("Do dai Array goi: " + sendArray.length);
            for (int i = 0; i < sendArray.length; i++) {
                System.out.print(sendArray[i] + " | ");
            }
            sendTimes++;
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < WAITING_RESPOND_TIME) {
                byte[] respondArray = SerialUartProcess.ReceiveMessage(6);
                if (respondArray != null) {
                    System.out.println("Array confirm save respond:");
                    for (int i = 0; i < 3; i++) {
                        System.out.print(respondArray[i] + " | ");
                    }
                    if (respondArray[0] == wArray[0] && respondArray[1] == wArray[1]) {
                        System.out.println("Confirm respond ok");
                        stopLoop = true;
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }

    // Receive Uart Buffer Object from Controller
    public static UartBufferS ReceiveDataFromController(int waitTime) {
        UartBufferS receiveUB = new UartBufferS(4);
        byte[] receiveData = null;
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < waitTime) {
            receiveData = SerialUartProcess.ReceiveMessage(6);

            if (receiveData != null) {
                receiveUB.ConvertToUartBuffers(receiveData);
                if (receiveData.length == 6) {
                    SerialUartProcess.SendByteArray(receiveData);
                    System.out.println("Receive 6 bytes data and replied!!");
                }
                break;
            }
        }
        if (receiveData == null) {
            System.out.println("Cannot connect to Controller");
//            System.exit(0);
        }
        return receiveUB;
    }

    // Wait data from controller
    public static UartBufferS WaitDataFromController() {
        UartBufferS receiveUB = new UartBufferS(4);
        byte[] receiveData = null;
        while (true) {
            receiveData = SerialUartProcess.ReceiveMessage(6);

            if (receiveData != null) {
                receiveUB.ConvertToUartBuffers(receiveData);
                if (receiveData.length == 6) {
                    SerialUartProcess.SendByteArray(receiveData);
                }
                break;
            }
        }
        if (receiveData == null) {
            System.out.println("Cannot connect to Controller");
//            System.exit(0);
        }
        return receiveUB;
    }

    // Wait data from controller
    public static boolean WaitWashingCommandFromController() {
        boolean result = false;
        byte[] receiveDataA = null;
        while (true) {
            receiveDataA = SerialUartProcess.ReceiveOriginalData(6);

            if (receiveDataA != null) {
                if (receiveDataA[0] == UartBufferS.headerTable.get("H_SET") && receiveDataA[1] == UartBufferS.controlstatusTable.get("START_WASHING")) {
                    SerialUartProcess.SendByteArray(receiveDataA);
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public static boolean SplashScreenConfirm() {
        boolean result = false;
        byte[] receiveData = null;
        while (true) {
            receiveData = SerialUartProcess.ReceiveOriginalData(6);
            if (receiveData != null) {
                if (receiveData[0] == UartBufferS.headerTable.get("H_SET") && receiveData[1] == UartBufferS.controlstatusTable.get("OK_ALL")) {
                    System.out.println("SplashScreen confirm");
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public static int[] GetTimeSettingFromController() {
        int[] result = null;
        byte[] receiveData = null;
        byte[] revertData = new byte[RECEIVE_LENGTH - 1];
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < 5000) {
            receiveData = SerialUartProcess.ReceiveMessage(RECEIVE_LENGTH);

            if (receiveData != null) {
                System.out.print("Receive data : " + receiveData);
                byte crc8 = CRC8.calc(receiveData, RECEIVE_LENGTH - 1);
                System.out.println("CRC calculated: " + crc8);
                System.out.println("CRC received: " + receiveData[RECEIVE_LENGTH - 1]);
                if (crc8 == receiveData[RECEIVE_LENGTH - 1]) {

                    for (int i = 0; i < RECEIVE_LENGTH - 1; i++) {
                        revertData[i] = receiveData[RECEIVE_LENGTH - i - 2];
                    }
                    result = ConvertByteInt.convertByteArrayToIntArray(revertData);
                    break;
                } else {
                    System.out.println("Cannot receive data");
//                    System.exit(0);
                }

            }

        }
        return result;

    }

    public static float[] GetNumberSettingFromController() {
        float[] result = null;
        byte[] receiveData = null;
        byte[] revertData = new byte[RECEIVE_LENGTH_NUM - 1];
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < 5000) {
            receiveData = SerialUartProcess.ReceiveMessage(RECEIVE_LENGTH_NUM);
            if (receiveData != null) {
                byte crc8 = CRC8.calc(receiveData, RECEIVE_LENGTH_NUM - 1);
                System.out.println("CRC calculated: " + crc8);
                System.out.println("CRC received: " + receiveData[RECEIVE_LENGTH_NUM - 1]);
                if (crc8 == receiveData[RECEIVE_LENGTH_NUM - 1]) {

                    for (int i = 0; i < RECEIVE_LENGTH_NUM - 1; i++) {
                        revertData[i] = receiveData[RECEIVE_LENGTH_NUM - i - 2];

                    }
                    result = ConvertByteInt.ConvertByteArrayToFloatArray(revertData);

                    break;
                } else {
                    System.out.println("Cannot receive data");
//                    System.exit(0);
                }

            }

        }
        return result;

    }
}
