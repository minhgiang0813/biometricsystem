/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

/**
 *
 * @author minhg
 */
public class VirtualKeyboard {

    public static void Show() {
        try {
            Runtime.getRuntime().exec("onboard -x 112 -y 390 -s 800x120");
//            input.grabFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Hide() {
        try {
            Runtime.getRuntime().exec("killall onboard");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SetNumpad() {
        try {
            Runtime.getRuntime().exec("gsettings set org.onboard layout '/home/pi/.local/share/onboard/layouts/Numpad.onboard'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SetMiniSize() {
        try {
            Runtime.getRuntime().exec("gsettings set org.onboard layout '/usr/share/onboard/layouts/Compact.onboard'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
