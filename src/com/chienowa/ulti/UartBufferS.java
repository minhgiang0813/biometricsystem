/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;
import java.lang.Byte;

/**
 *
 * @author minhg
 */
public class UartBufferS {

    public int aSize;
    public byte headU;
    public byte keyU;
    public byte[] contentArray;

    public static Hashtable<String, Byte> controlstatusTable
            = new Hashtable<>() {
        {
            put("OK_ALL", (byte) 0x00);
            put("OK_USER", (byte) 0x01);
            put("READ_TIME", (byte) 0x02);
            put("READ_NUMBER", (byte) 0x03);
            put("FLOW_SENSOR_ERROR", (byte) 0x04);
            put("OVER_VOLTAGE_1", (byte) 0x05);
            put("OVER_VOLTAGE_2", (byte) 0x06);
            put("OVER_VOLTAGE_3", (byte) 0x07);
            put("UNDER_VOLTAGE", (byte) 0x08);
            put("CURRENT_ABNORMAL", (byte) 0x09);
            put("OVER_CURRENT", (byte) 0x0A);
            put("SOLENOID_VALVE_ERROR", (byte) 0x0B);
            put("SALT_WATER_FULL_ERROR", (byte) 0x0C);
            put("SALT_WATER_EMPTY_ERROR", (byte) 0x0D);
            put("ACID_ERROR", (byte) 0x0E);
            put("ALKALINE_ERROR", (byte) 0x0F);
            put("WATER_FULL_ERROR", (byte) 0x10);
            put("WATER_EMPTY_ERROR", (byte) 0x11);
            put("CVCC_ALARM", (byte) 0x12);
            put("NEXT_ANIMATION", (byte) 0x13);
            put("SAVE_TIME", (byte) 0x14);
            put("SAVE_NUMBER", (byte) 0x15);
            put("SAVE_ERROR", (byte) 0x16);
            put("MACHINE_STATUS", (byte) 0x17);
            put("WASHING_MODE", (byte) 0x18);
            put("GET_MODE", (byte) 0x19);
            put("TESTING_MODE_START", (byte) 0x1A);
            put("TESTING_DATA", (byte) 0x1B);
            put("TESTING_MODE_STOP", (byte) 0x1C);
            put("MID_NIGHT", (byte) 0x1D);
            put("TEST_POWER_ON", (byte) 0x1E);
            put("TEST_FLOW_RATE", (byte) 0x1F);
            put("TEST_CURRENT", (byte) 0x20);
            put("TEST_INDIVIDUAL", (byte) 0x21);
            put("TEST_ELECTROLYTIC", (byte) 0x22);
            put("TEST_RUN_ADJUSTMENT", (byte) 0x23);
            put("FILTER_REPLACEMENT_E1", (byte) 0x24);
            put("FILTER_REPLACEMENT_E2", (byte) 0x25);
            put("CALLAN_MODE_SET", (byte) 0x26);
            put("DRAINAGE_MODE_SET", (byte) 0x27);
            put("BIOMECTRIC_SET", (byte) 0x28);
            put("POWER_ON_TEST_SET", (byte) 0x29);
            put("WATER_FILTERE_SET", (byte) 0x2A);
            put("START_WASHING", (byte) 0x2B);
            put("CONTROL_SETTING", (byte) 0x2C);
        }
    };

    public static Hashtable<String, Byte> headerTable
            = new Hashtable<>() {
        {
            put("H_READ", (byte) 0x52);
            put("H_SET", (byte) 0x53);
            put("H_ALARM", (byte) 0x41);
            put("H_ERROR", (byte) 0x45);
            put("H_CLEAR", (byte) 0x43);
        }
    };


    public UartBufferS(int size) {
        this.aSize = size;
        contentArray = new byte[aSize];
    }

    //Get int array from Content
    public int[] IntArrayValue() {

        byte[] data = new byte[aSize];
        for (int i = 0; i < aSize; i++) {
            data[i] = contentArray[aSize - i - 1];
        }

        int[] result = new int[aSize / 4];
        result = ConvertByteInt.convertByteArrayToIntArray(data);
        return result;

    }

    //Convert UartBuffer Ob to byte array
    public byte[] ByteBuffer() {
        byte[] result = new byte[aSize + 2];
        result[0] = headU;
        result[1] = keyU;
        System.arraycopy(contentArray, 0, result, 2, aSize);
        return result;
    }

    //Convert to UartBuffer Ob from byte array
    public void ConvertToUartBuffers(byte[] inputArray) {

        if (inputArray.length == aSize + 2) {
            headU = inputArray[0];
            keyU = inputArray[1];
            for (int i = 0; i < aSize; i++) {
                contentArray[i] = inputArray[i + 2];
            }

        } else {
            System.out.println("Array length is not equal to convert to int");
            for (int i = 0; i < inputArray.length; i++) {
                System.out.print(inputArray[i] + " ");
            }
            System.exit(0);
        }

    }

}
