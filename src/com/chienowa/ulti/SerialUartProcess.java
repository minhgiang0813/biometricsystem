/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.chienowa.ulti;

/**
 *
 * @author minhg
 */
import com.pi4j.wiringpi.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minhg
 */
public class SerialUartProcess {

    public static List<Byte> iList;

    private static int serialPort = 0;

    public static void StartSerialUartProcess() {
        // TODO code application logic here
        serialPort = Serial.serialOpen(Serial.DEFAULT_COM_PORT, 9600);
        if (serialPort == 1) {
            System.out.println("Serial Port Failed");
//            return;
        } else {
            System.out.println("Default Serial port initialized.");
            iList = new ArrayList<>();
        }

    }

    public static void SendMessage(String message) {
        Serial.serialPuts(serialPort, message); //Writing to Serial Port
    }

    synchronized public static byte[] ReceiveOriginalData(int i) {
        byte[] byteArray = null;
        if (Serial.serialDataAvail(serialPort) > 0) {
            byte[] inChar = Serial.serialGetBytes(serialPort, i);
            if (inChar != null) {
                byteArray = inChar;
            }
        }
        return byteArray;
    }

    synchronized public static byte[] ReceiveMessage(int i) {

        byte[] byteArray = null;

        if (Serial.serialDataAvail(serialPort) > 0) {

            byte[] inChar = Serial.serialGetBytes(serialPort, i);

            if (inChar != null) {

//                System.out.println("Do dai array nhan: " + inChar.length);

                if (iList.size() < i) {
                    for (byte j : inChar) {
                        iList.add(j);
//                        System.out.println(j);
                    }
                }
                if (iList.size() == i) {

                    byteArray = new byte[i];
                    for (int k = 0; k < i; k++) {
                        byteArray[k] = iList.get(k);
//                        System.out.print(byteArray[k] + " ");
                    }

                    iList.clear();
                }
            }

        }
        

        return byteArray;
    }

    public static void ClosePort() {
        Serial.serialClose(serialPort);
    }

    public static void SendByteArray(byte[] data) {
        Serial.serialPutBytes(serialPort, data, data.length);

    }
    
}
