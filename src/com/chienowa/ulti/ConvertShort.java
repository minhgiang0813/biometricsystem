/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author minhg
 */
public class ConvertShort {
    public static short[] ByteArrayToShortArray(byte[] bytes){
        short[] shorts = new short[bytes.length/2];
        ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
        return shorts;
    }
    
    public static byte[] ShortArrayToByteArray(short[] shortArray) {
        byte[] bytes = new byte[shortArray.length*2];
        ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(shortArray);
        return bytes;
    }
}
