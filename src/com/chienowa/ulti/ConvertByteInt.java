/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.chienowa.ulti;

import static com.chienowa.ulti.SerialUartProcess.iList;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minhg
 */
public class ConvertByteInt {

    public static int byteArrayToInt(byte[] b) {

        return byteArrayToInt(b, 0);

    }

    public static int byteArrayToInt(byte[] b, int offset) {

        int value = 0;

        for (int i = 0; i < 4; i++) {
            int shift;
            shift = (4 - 1 - i) * 8;

            value += (b[i + offset] & 0x000000FF) << shift;

        }

        return value;

    }

    public static byte[] intToByteArray(int value) {

        byte[] b = new byte[4];

        for (int i = 0; i < 4; i++) {

            int offset;

            offset = (b.length - 1 - i) * 8;

            b[i] = (byte) ((value >>> offset) & 0xFF);

        }

        return b;

    }

    public static byte[] intToBytes(int[] values) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        DataOutputStream dos = new DataOutputStream(baos);

        for (int i = 0; i < values.length; ++i) {

            try {

                dos.writeInt(values[i]);

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

        return baos.toByteArray();

    }

    public static int[] convertByteArrayToIntArray(byte[] bytes) {
        ArrayList integers = new ArrayList();
        for (int index = 0; index < bytes.length; index += 4) {
            byte[] fourBytes = new byte[4];
            fourBytes[0] = bytes[index];
            fourBytes[1] = bytes[index + 1];
            fourBytes[2] = bytes[index + 2];
            fourBytes[3] = bytes[index + 3];
            int integer = byteArrayToInt(fourBytes);
            integers.add(new Integer(integer));
        }
        int[] ints = new int[bytes.length / 4];
        for (int index = 0; index < integers.size(); index++) {
            ints[index] = ((Integer) integers.get(index)).intValue();
        }
        return ints;
    }

    public static float[] ConvertByteArrayToFloatArray(byte[] bytes) {

        float[] floatArray = new float[bytes.length / 4];

        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        for (int i = 0; i < bytes.length; i += 4) {
            floatArray[i / 4] = buffer.getFloat(i);
        }

        return floatArray;
    }

    public static byte[] ConvertFloatArrayToByteArray(float[] floatArray) {

//        float[] floatArray = new float[floatArrayRV.length];
//        for (int i = 0; i < floatArrayRV.length; i++) {
//            floatArray[i] = floatArrayRV[floatArrayRV.length - 1 - i];
//        }

        List<Byte> iResult;

        iResult = new ArrayList<>();

        byte[] byteArray = new byte[floatArray.length * 4];
        for (int i = 0; i < floatArray.length; i++) {
            byte[] byteA = ByteBuffer.allocate(4).putFloat(floatArray[i]).array();
//            for (int j = 0; j < byteA.length; j++) {
//                iResult.add(byteA[3-j]);
//            }
            for (byte a : byteA) {
                iResult.add(a);
            }

        }
        for (int i = 0; i < byteArray.length; i++) {
            byteArray[i] = iResult.get(i);
        }
        return byteArray;
    }

}
