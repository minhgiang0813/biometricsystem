/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chienowa.ulti;

import com.chienowa.cvnpalmsensor.TimeSettingFrame;
import java.util.BitSet;

/**
 *
 * @author minhg
 */
public class DataFromController {

    public static int[] dataTimeIntArray = new int[35];

    public static void GetTimeFromController() {

        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("READ_TIME");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            System.out.println("Send request time table OK");
            dataTimeIntArray = CommunicateController.GetTimeSettingFromController();
        } else {
            System.out.println("Can't recheck package - System out");
//            System.exit(0);
        }

    }

    public static float[] GetNumberSettingFromController() {

        float[] returnFloatArray = null;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("READ_NUMBER");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
//            System.out.println("Send request OK");
            returnFloatArray = CommunicateController.GetNumberSettingFromController();
//            System.out.println(dataTimeIntArray[6]);
        } else {
            System.out.println("Can't recheck package - System out");
//            System.exit(0);
        }

        return returnFloatArray;

    }

    public static int GetModeSettingFromController() {
        int result = -1;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("WASHING_MODE");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            UartBufferS receiveBuffer = CommunicateController.ReceiveDataFromController(1500);
//            System.out.println("Respond for mode setting");
//            System.out.println("Respond [0]" + receiveBuffer.headU);
//            System.out.println("Respond [1]" + receiveBuffer.keyU);
//            System.out.println("Respond [2]" + receiveBuffer.contentArray[0]);
//            System.out.println("Respond [3]" + receiveBuffer.contentArray[1]);
//            System.out.println("Respond [4]" + receiveBuffer.contentArray[2]);
//            System.out.println("Respond [5]" + receiveBuffer.contentArray[3]);
            if (receiveBuffer.headU == UartBufferS.headerTable.get("H_READ") && receiveBuffer.keyU == UartBufferS.controlstatusTable.get("WASHING_MODE")) {
                result = ConvertByteInt.byteArrayToInt(receiveBuffer.contentArray);
                System.out.println("Received Mode info!!");
            }
        }

        return result;
    }

    public static int GetDrainageModeFromController() {
        int result = -1;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("DRAINAGE_MODE_SET");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            UartBufferS receiveBuffer = CommunicateController.ReceiveDataFromController(1500);
            if (receiveBuffer.headU == UartBufferS.headerTable.get("H_READ") && receiveBuffer.keyU == UartBufferS.controlstatusTable.get("DRAINAGE_MODE_SET")) {
                result = ConvertByteInt.byteArrayToInt(receiveBuffer.contentArray);
                System.out.println("Received Mode info!!");
            }
        }

        return result;
    }

    public static int GetTestPowerOnModeFromController() {
        int result = -1;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("POWER_ON_TEST_SET");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            UartBufferS receiveBuffer = CommunicateController.ReceiveDataFromController(1500);
            if (receiveBuffer.headU == UartBufferS.headerTable.get("H_READ") && receiveBuffer.keyU == UartBufferS.controlstatusTable.get("POWER_ON_TEST_SET")) {
                result = ConvertByteInt.byteArrayToInt(receiveBuffer.contentArray);
                System.out.println("Received Mode info!!");
            }
        }

        return result;
    }

    public static int GetWaterFilterFromController() {
        int result = -1;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("WATER_FILTERE_SET");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            UartBufferS receiveBuffer = CommunicateController.ReceiveDataFromController(1500);
            if (receiveBuffer.headU == UartBufferS.headerTable.get("H_READ") && receiveBuffer.keyU == UartBufferS.controlstatusTable.get("WATER_FILTERE_SET")) {
                result = ConvertByteInt.byteArrayToInt(receiveBuffer.contentArray);
                System.out.println("Received Mode info!!");
            }
        }

        return result;
    }

    public static int GetBiometricModeFromController() {
        int result = -1;
        UartBufferS sBuffer = new UartBufferS(4);
        sBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sBuffer.keyU = UartBufferS.controlstatusTable.get("BIOMECTRIC_SET");
        sBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sBuffer)) {
            UartBufferS receiveBuffer = CommunicateController.ReceiveDataFromController(1500);
            if (receiveBuffer.headU == UartBufferS.headerTable.get("H_READ") && receiveBuffer.keyU == UartBufferS.controlstatusTable.get("BIOMECTRIC_SET")) {
                result = ConvertByteInt.byteArrayToInt(receiveBuffer.contentArray);
                System.out.println("Received Mode info!!");
            }
        }

        return result;
    }

    public static int[] GetControlSettingFromController() {
        int[] result = new int[4];
        byte[] receiveData = null;
        UartBufferS sendBuffer = new UartBufferS(4);
        sendBuffer.headU = UartBufferS.headerTable.get("H_READ");
        sendBuffer.keyU = UartBufferS.controlstatusTable.get("CONTROL_SETTING");
        sendBuffer.contentArray = new byte[]{0, 0, 0, 0};

        if (CommunicateController.SendDataToController(sendBuffer)) {
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < 2000) {
                receiveData = SerialUartProcess.ReceiveMessage(6);
                if (receiveData != null) {
                    System.out.println("Receive data: " + receiveData);
                    if (receiveData[0] == UartBufferS.headerTable.get("H_READ") && receiveData[1] == UartBufferS.controlstatusTable.get("CONTROL_SETTING")) {
                        System.out.println("Data[5]: " + receiveData[5]);
                        byte[] dataArray = new byte[1];
                        dataArray[0] = receiveData[5];
                        BitSet bitset = BitSet.valueOf(dataArray);
                        for (int i = 0; i < bitset.length(); i++) {
                            System.out.println("Data " + i + ":" + bitset.get(i));
                        }
//                        System.out.println(bitset);
                        boolean drain = bitset.get(0);
                        boolean power_on = bitset.get(1);
                        boolean filter = bitset.get(2);
                        boolean biometric = bitset.get(3);

                        result[0] = ConvertBooleanToInt(drain);
                        result[1] = ConvertBooleanToInt(power_on);
                        result[2] = ConvertBooleanToInt(filter);
                        result[3] = ConvertBooleanToInt(biometric);
                    }
                }
            }
        }

        return result;
    }

    public static int ConvertBooleanToInt(boolean value) {
        if (value) {
            return 1;
        } else {
            return 0;
        }
    }

}
