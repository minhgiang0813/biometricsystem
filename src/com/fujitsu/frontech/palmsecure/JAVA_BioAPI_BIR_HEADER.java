package com.fujitsu.frontech.palmsecure;

public class JAVA_BioAPI_BIR_HEADER {

	public long Length;

    public short HeaderVersion;

    public short Type;

	public JAVA_BioAPI_BIR_BIOMETRIC_DATA_FORMAT Format;

	public byte Quality;

	public short Purpose;

	public long FactorsMask;

}
