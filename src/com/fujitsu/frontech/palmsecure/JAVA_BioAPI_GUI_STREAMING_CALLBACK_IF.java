package com.fujitsu.frontech.palmsecure;

public interface JAVA_BioAPI_GUI_STREAMING_CALLBACK_IF {

	public long JAVA_BioAPI_GUI_STREAMING_CALLBACK(
		Object GuiStreamingCallbackCtx,
		JAVA_BioAPI_GUI_BITMAP Bitmap);
}
