package com.fujitsu.frontech.palmsecure;

class LOCAL_GUI_STATE_CB {

	private JAVA_BioAPI_GUI_STATE_CALLBACK_IF stateCB = null;

	private Object stateCBCtx = null;

	void setCallbackObject(JAVA_BioAPI_GUI_STATE_CALLBACK_IF cb)
	{
		stateCB = cb;
	}

	void setCallbackContext(Object ctx)
	{
		stateCBCtx = ctx;
	}

	long CALLBACK(
			long GuiState,
			short Response,
			long Message,
			short Progress,
			long Width,
			long Height,
			byte[] Bitmap) {
		
		long ret = 0;

		if(stateCB == null)
		{
			return ret;
		}

		JAVA_BioAPI_GUI_BITMAP SampleBuffer = null;

		if (Bitmap != null)
		{
			SampleBuffer = new JAVA_BioAPI_GUI_BITMAP();
			SampleBuffer.Width = Width;
			SampleBuffer.Height = Height;

			SampleBuffer.Bitmap = new JAVA_BioAPI_DATA();
			SampleBuffer.Bitmap.Length = Bitmap.length;
			SampleBuffer.Bitmap.Data = Bitmap;
		}

		try
		{
			ret = stateCB.JAVA_BioAPI_GUI_STATE_CALLBACK(
					stateCBCtx,
					GuiState,
					Response,
					Message,
					Progress,
					SampleBuffer);
		}
		catch(Exception e)
		{

		}


		return ret;
	}

}
