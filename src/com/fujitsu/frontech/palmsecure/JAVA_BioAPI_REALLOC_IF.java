package com.fujitsu.frontech.palmsecure;

public interface JAVA_BioAPI_REALLOC_IF {

	public Object JAVA_BioAPI_REALLOC(
		Object Memblock,
		long Size,
		Object Allocref);
}
