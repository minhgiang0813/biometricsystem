package com.fujitsu.frontech.palmsecure.util;

public class PalmSecureConstant {

	public final static int SYSTEM_EXCEPTION										= 100;
	public final static int ARGUMENT_EXCEPTION										= 200;
	public final static int MEMORY_EXCEPTION										= 300;
	public final static int ELSE_EXCEPTION											= 400;

	public final static long JAVA_BioAPI_BIR_SIZE									= 16;

	/* return code */
	public final static long JAVA_BioAPI_OK											= 0x00;
	public final static long JAVA_BioAPI_ERRCODE_FUNCTION_FAILED					= 0x0A;

	/* BioAPI_GUI_STATE */
	public final static long JAVA_BioAPI_SAMPLE_AVAILABLE							= 0x0001;
	public final static long JAVA_BioAPI_MESSAGE_PROVIDED							= 0x0002;
	public final static long JAVA_BioAPI_PROGRESS_PROVIDED							= 0x0004;

	/* BIR purpose value */
	public final static byte JAVA_BioAPI_PURPOSE_VERIFY								= 1;
	public final static byte JAVA_BioAPI_PURPOSE_IDENTIFY							= 2;
	public final static byte JAVA_BioAPI_PURPOSE_ENROLL								= 3;
	public final static byte JAVA_BioAPI_PURPOSE_ENROLL_FOR_VERIFICATION_ONLY		= 4;
	public final static byte JAVA_BioAPI_PURPOSE_ENROLL_FOR_IDENTIFICATION_ONLY		= 5;
	public final static byte JAVA_BioAPI_PURPOSE_AUDIT								= 6;

	/* BioAPI_INPUT_BIR Form value */
	public final static short JAVA_BioAPI_DATABASE_ID_INPUT							= 1;
	public final static short JAVA_BioAPI_BIR_HANDLE_INPUT							= 2;
	public final static short JAVA_BioAPI_FULLBIR_INPUT								= 3;

	/* BioAPI Bool value */
	public final static int JAVA_BioAPI_FALSE										= 0;
	public final static int JAVA_BioAPI_TRUE										= 1;

	/* Population Type */
	public final static byte JAVA_BioAPI_DB_TYPE									= 1;
	public final static byte JAVA_BioAPI_ARRAY_TYPE									= 2;

	/* MaxFRRRequested Matching level */
	public final static int JAVA_PvAPI_MATCHING_LEVEL_NORMAL						= 0;
	public final static int JAVA_PvAPI_MATCHING_LEVEL_HIGHEST						= 1;
	public final static int JAVA_PvAPI_MATCHING_LEVEL_HIGH							= 2;
	public final static int JAVA_PvAPI_MATCHING_LEVEL_LOW							= 3;
	public final static int JAVA_PvAPI_MATCHING_LEVEL_LOWEST						= 4;

	/* GuiState */
	public final static long JAVA_PvAPI_APPEND_STREAMING = 0x0008;

	/* Guide state message provided state */
	/* Guide message - Guide start. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_START						= 0x02030000;
	/* Guide message - Move your hand away from the sensor. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_BADIMAGE					= 0x02030200;
	/* Guide message - Stop moving your hand. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_MOVING						= 0x02030203;
	/* Guide message - Place your hand correctly. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_LESSINFO					= 0x02030204;
	/* (Optional) Guide message - Move left. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_RIGHT						= 0x02030205;
	/* (Optional) Guide message - Move right. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_LEFT						= 0x02030206;
	/* (Optional) Guide message - Move backward. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_DOWN						= 0x02030207;
	/* (Optional) Guide message - Move forward. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_UP							= 0x02030208;
	/* Guide message - Bring your hand close. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_FAR							= 0x02030209;
	/* Guide message - Keep away your hand. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_NEAR						= 0x0203020a;
	/* Guide message - Now capturing. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_CAPTURING					= 0x0203020b;
	/* (Not Support) Guide message - Your hand has inclined. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_LEAN						= 0x0203020C;
	/* (Optional) Guide message - Your hand has pitched down. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_PITCH_DOWN					= 0x02030210;
	/* (Optional) Guide message - Your hand has pitched up. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_PITCH_UP					= 0x02030211;
	/* (Optional) Guide message - Your hand has rolled right. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_ROLL_RIGHT					= 0x02030212;
	/* (Optional) Guide message - Your hand has rolled left. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_ROLL_LEFT					= 0x02030213;
	/* (Optional) Guide message - Your hand has yaw right. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_YAW_RIGHT					= 0x02030214;
	/* (Optional) Guide message - Your hand has yaw left.  */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_YAW_LEFT					= 0x02030215;
	/* (Optional) Guide message - Your fingers are closed.  */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_CLOSE_FINGER				= 0x02030216;
	/* (Optional) Guide message - Adjust gain and retry. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_ADJUST_LIGHT				= 0x02030220;
	/* (Optional) Guide message - Failed adjusting gain. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_ADJUST_NG					= 0x02030221;
	/* Guide message - Guide phase end. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_PHASE_END					= 0x02030300;
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_NO_HANDS					= 0x02030201;
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_ROUND						= 0x02030217;
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_CAPTURE_IMAGE				= 0x02030222;
	/* (Optional) Guide message - The lighting environment is normal. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_LIGHT_NORMAL				= 0x02030223;
	/* (Optional) Guide message - The lighting environment is strong. */
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_LIGHT_STRONG				= 0x02030224;

	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_TOO_FAR						= 0x02030225;
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_TOO_NEAR					= 0x02030226;

	public final static long JAVA_PvAPI_NOTIFY_SHUTTER								= 0x02040000;
	public final static long JAVA_PvAPI_NOTIFY_CAP_GUID_SHUTTER						= 0x02040000;
	public final static long JAVA_PvAPI_NOTIFY_WAIT_FOR_TRIGGER						= 0x02040000;

	public final static long JAVA_PvAPI_NOTIFY_API_KIND								= 0x03000100;
	public final static long JAVA_PvAPI_NOTIFY_API_END								= 0x03000200;
	public final static long JAVA_PvAPI_NOTIFY_MATCH_RESULT							= 0x04000000;
	public final static long JAVA_PvAPI_NOTIFY_REGIST_SCORE							= 0x05000000;

	public final static long JAVA_PvAPI_NOTIFY_API_KIND_CAPTURE						= 0x01;
	public final static long JAVA_PvAPI_NOTIFY_API_KIND_VERIFYMATCH					= 0x02;
	public final static long JAVA_PvAPI_NOTIFY_API_KIND_IDENTIFYMATCH				= 0x03;
	public final static long JAVA_PvAPI_NOTIFY_API_KIND_ENROLL						= 0x04;
	public final static long JAVA_PvAPI_NOTIFY_API_KIND_VERIFY						= 0x05;
	public final static long JAVA_PvAPI_NOTIFY_API_KIND_IDENTIFY					= 0x06;

	public final static long JAVA_PvAPI_NOTIFY_API_END_NORMAL						= 0x00;
	public final static long JAVA_PvAPI_NOTIFY_API_END_CANCEL						= 0x10;
	public final static long JAVA_PvAPI_NOTIFY_API_END_ERROR						= 0x20;

	public final static int JAVA_PvAPI_REGIST_SCORE_QUALITY_1						= 1;
	public final static int JAVA_PvAPI_REGIST_SCORE_QUALITY_2						= 2;
	public final static int JAVA_PvAPI_REGIST_SCORE_QUALITY_3						= 3;

	/* SetProfile Flag value */
	public final static long JAVA_PvAPI_PROFILE_DISPLAY_KIND						= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_DIRECTION					= 0x00000002;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE							= 0x00000003;
	public final static long JAVA_PvAPI_PROFILE_REGIST_DATA_TYPE					= 0x00000004;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_TYPE							= 0x00000005;
	public final static long JAVA_PvAPI_PROFILE_REGIST_TEMPERATURE					= 0x00000006;
	public final static long JAVA_PvAPI_PROFILE_MATCH_TEMPERATURE					= 0x00000007;
	public final static long JAVA_PvAPI_PROFILE_MATCH_EXTRACT_KIND					= 0x00000008;
	public final static long JAVA_PvAPI_PROFILE_CARD_INFO							= 0x00000009;
	public final static long JAVA_PvAPI_PROFILE_CR_INFO								= 0x0000000A;
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_MODE						= 0x0000000B;
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_TRUNCATION_MODE			= 0x0000000C;
	public final static long JAVA_PvAPI_PROFILE_ENROLL_INDEX_MODE					= 0x0000000D;
	public final static long JAVA_PvAPI_PROFILE_IMAGE								= 0x0000000E;
	public final static long JAVA_PvAPI_PROFILE_REGIST_EXTRACT_KIND					= 0x0000000F;
	public final static long JAVA_PvAPI_PROFILE_TRACE_MESSAGE						= 0x00000010;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_MODE							= 0x00000011;
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_DATA_MODE					= 0x00000013;
	public final static long JAVA_PvAPI_PROFILE_SCORE_NOTIFICATIONS					= 0x00000014;
	public final static long JAVA_PvAPI_PROFILE_GETGMODE							= 0x00000015;
	public final static long JAVA_PvAPI_PROFILE_RI_INFO								= 0x00000016;
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_TIMES						= 0x00000017;
	public final static long JAVA_PvAPI_PROFILE_SL_EXTEND_MODE						= 0x00000018;
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_INV_SENSE					= 0x0000001A;
	public final static long JAVA_PvAPI_PROFILE_EXDATA_MODE							= 0x0000001B;
	public final static long JAVA_PvAPI_PROFILE_SHARED_CAPTURE_DATA					= 0x0000001E;
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_MODE						= 0x00000020;
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_SL_EXT_MODE				= 0x00000021;

	/* SetProfile display kind dwParam value */
	public final static long JAVA_PvAPI_PROFILE_DISPLAY_KIND_APL					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_DISPLAY_KIND_LIB					= 0x00000001;

	/* SetProfile sensor direction dwParam value */
	public final static long JAVA_PvAPI_PROFILE_SENSOR_DIRECTION_0					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_DIRECTION_90					= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_DIRECTION_180				= 0x00000002;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_DIRECTION_270				= 0x00000003;

	/* SetProfile guide type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_NORMAL					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_NO_GUITE					= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_ATM1						= 0x00000100;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_ATM2						= 0x00000101;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_USER1					= 0x00000200;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_TYPE_USER2					= 0x00000201;

	/* SetProfile regist data type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_REGIST_DATA_TYPE_NO_COMPRESS_2		= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_REGIST_DATA_TYPE_COMPRESS_2			= 0x00000003;

	/* SetProfile guide type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_SENSOR_TYPE_INI						= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_TYPE_AUTO					= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_SENSOR_TYPE_NO_SENSOR				= 0x00000002;

	/* SetProfile extract kind dwParam value */
	public final static long JAVA_PvAPI_PROFILE_EXTRACT_KIND_1						= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_EXTRACT_KIND_2						= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_EXTRACT_KIND_3						= 0x00000002;

	/* SetProfile CR type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_CR_KIND_2							= 0x00000002;
	public final static long JAVA_PvAPI_PROFILE_CR_KIND_3							= 0x00000003;

	/* Identify Mode type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_MODE_OLDCOMPATIBLE			= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_MODE_INDEX					= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_MODE_INDEX_2				= 0x00000002;

	/* Not Support */
	public final static long JAVA_PvAPI_PROFILE_IDENTIFY_TRUNCATION_MODE_NOTHING	= 0x00000000;

	/* SetProfile enroll-index-mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_INDEX_MODE_1						= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_INDEX_MODE_2						= 0x00000002;

	/* SetProfile capture-data-mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_DATA_MODE_0					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_DATA_MODE_2					= 0x00000002;

	/* SetProfile image type dwParam value */
	public final static long JAVA_PvAPI_PROFILE_IMAGE_OFF							= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_IMAGE_SILHOUETTE					= 0x00000001;

	public final static long JAVA_PvAPI_PROFILE_GUIDE_MODE_NO_GUIDE					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_GUIDE_MODE_GUIDE					= 0x00000001;

	/* SetProfile Match-score dwParam value */
	public final static long JAVA_PvAPI_PROFILE_SCORE_NOTIFICATIONS_OFF				= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_SCORE_NOTIFICATIONS_ON				= 0x00000001;
	public final static long JAVA_PvAPI_PROFILE_SCORE_NOTIFICATIONS_ON_MODE2		= 0x00000002;

	/* SetProfile SL-Extend-Mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_SL_EXTEND_MODE_OLD					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_SL_EXTEND_MODE_EX1					= 0x00000001;

	/* SetProfile CaptureInvSense dwParam value */
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_INV_SENSE_OFF				= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_CAPTURE_INV_SENSE_ON				= 0x00000001;

	/* SetProfile shared capture data dwParam value */
	public final static long JAVA_PvAPI_PROFILE_SHARED_MODE_OFF						= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_SHARED_MODE_ON						= 0x00000001;

	/* SetProfile MultiData-Mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_MODE_0					= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_MODE_1					= 0x00000001;

	/* SetProfile MultiData-SL-Ext-Mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_SL_EXT_MODE_0			= 0x00000000;
	public final static long JAVA_PvAPI_PROFILE_MULTIDATA_SL_EXT_MODE_1			= 0x00000001;

	/* PresetProfile */
	/* PresetProfile flag value */
	public final static long JAVA_PvAPI_PRE_PROFILE_IDENTIFYSENSOR					= 0x00000001;
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_AES256					= 0x00000002;
	public final static long JAVA_PvAPI_PRE_PROFILE_CERTIFY_INFO					= 0x00000003;
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_CONNECT					= 0x00000007;
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_EXTEND					= 0x00000008;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURE_TIMES					= 0x00000009;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURE_COMPRESS				= 0x0000000A;
	public final static long JAVA_PvAPI_PRE_PROFILE_BHG_MODE						= 0x0000000B;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURING_RANGE					= 0x0000000C;
	public final static long JAVA_PvAPI_PRE_PROFILE_MAX_MATCHTHREAD_NUM				= 0x0000000D;
	public final static long JAVA_PvAPI_PRE_PROFILE_G_EXTENDED_MODE					= 0x0000000E;
	public final static long JAVA_PvAPI_PRE_PROFILE_USB_POWER_MODE					= 0x0000000F;
	public final static long JAVA_PvAPI_PRE_PROFILE_SLG_EXTENDED_MODE			= 0x00000011;

	/* PresetProfile sensor connect lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_CONNECT_0				= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_CONNECT_1				= 0x00000001;

	/* PresetProfile sensor extend lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_EXTEND_OFF				= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_SENSOR_EXTEND_ON				= 0x00000001;

	/* PresetProfile capture compress lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURE_COMPRESS_OFF			= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURE_COMPRESS_ON				= 0x00000001;

	/* PresetProfile BHG use mode lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_BHG_MODE_OFF					= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_BHG_MODE_ON						= 0x00000001;

	/* PresetProfile capturing range lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURING_RANGE_NEW				= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURING_RANGE_OLD				= 0x00000001;
	public final static long JAVA_PvAPI_PRE_PROFILE_CAPTURING_RANGE_CHG				= 0x00000002;

	/* PreSetProfile G-Extended-Mode lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_G_EXTENDED_MODE_OFF				= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_G_EXTENDED_MODE_1				= 0x00000001;
	public final static long JAVA_PvAPI_PRE_PROFILE_G_EXTENDED_MODE_2				= 0x00000002;

	/* PreSetProfile USBHighPowerMode lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_USB_POWER_MODE_NORMAL			= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_USB_POWER_MODE_HIGH				= 0x00000001;

	/* PreSetProfile SLG-Extended-Mode lpvParamData value */
	public final static long JAVA_PvAPI_PRE_PROFILE_SLG_EXTENDED_MODE_OFF		= 0x00000000;
	public final static long JAVA_PvAPI_PRE_PROFILE_SLG_EXTENDED_MODE_1			= 0x00000001;

	/* Packet */
	public final static long JAVA_PvAPI_SIZE_PACKET_CRCINFO							= 0x00000010;
	public final static long JAVA_PvAPI_SIZE_PACKET_VEINBUFFER						= 0x00001000;

	/* Population Type */
	public final static byte JAVA_PvAPI_PRESET_ARRAY_TYPE							= 3;

	/* GetTemplateInfo Type */
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_1							= 0x00000000;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_2							= 0x00000001;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_3							= 0x00000002;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_4							= 0x00000003;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_8							= 0x00000007;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_9							= 0x00000008;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_A							= 0x00000009;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_B							= 0x0000000A;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_C							= 0x0000000B;
	public final static long JAVA_PvAPI_INFO_SENSOR_TYPE_D							= 0x0000000C;

	/* SetProfile enroll-index-mode dwParam value */
	public final static long JAVA_PvAPI_PROFILE_INDEX_MODE_0						= 0x00000000;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_0							= 0x00000000;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_1							= 0x00000001;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_2							= 0x00000002;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_5							= 0x00000005;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_10							= 0x0000000A;
	public final static long JAVA_PvAPI_INFO_INDEX_MODE_11							= 0x0000000B;

	public final static long JAVA_PvAPI_INFO_M2_EXTEND_OFF							= 0x00000000;
	public final static long JAVA_PvAPI_INFO_M2_EXTEND_ON							= 0x00000001;

	public final static long JAVA_PvAPI_INFO_G_EXTENDED_MODE_OFF					= 0x00000000;
	public final static long JAVA_PvAPI_INFO_G_EXTENDED_MODE_1						= 0x00000001;
	public final static long JAVA_PvAPI_INFO_G_EXTENDED_MODE_2						= 0x00000002;

	public final static long JAVA_PvAPI_INFO_GUIDE_MODE_NO_GUIDE					= 0x00000000;
	public final static long JAVA_PvAPI_INFO_GUIDE_MODE_GUIDE						= 0x00000001;
	public final static long JAVA_PvAPI_INFO_GUIDE_MODE_BHG							= 0x00000002;

	public final static long JAVA_PvAPI_INFO_MULTI_DATA_G						= 0x00000000;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_G_GUIDE					= 0x00000010;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_I						= 0x00000001;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_I_GUIDE					= 0x00000011;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_I_BHG					= 0x00000021;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_Iex						= 0x00000101;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_I33						= 0x00010001;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_I33_GUIDE				= 0x00010011;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_R						= 0x00020001;
	public final static long JAVA_PvAPI_INFO_MULTI_DATA_R_GUIDE					= 0x00020011;

	/* GuiStateCallback Return */
	public final static long JAVA_PvAPI_WAIT										= 16;

	/* PvAPI_GetConnectSensorInfo MaxNum */
	public final static long JAVA_PvAPI_GET_SENSOR_INFO_MAX							= 8;

	/* PvAPI_GetLibrary Sensor Extend Mode */
	public final static long JAVA_PvAPI_INFO_SENSOR_MODE_COMPATIBLE					= 0x00000000;
	public final static long JAVA_PvAPI_INFO_SENSOR_MODE_EXTEND						= 0x00000001;

	/* PvAPI_GetLibraryInfo loop-mode dwParam value */
	public final static long JAVA_PvAPI_INFO_LOOP_MODE_OFF							= 0x00000000;
	public final static long JAVA_PvAPI_INFO_LOOP_MODE_ON							= 0x00000001;

	/* PvAPI_GetLibraryInfo compress-mode dwParam value */
	public final static long JAVA_PvAPI_INFO_COMPRESS_MODE_OFF						= 0x00000000;
	public final static long JAVA_PvAPI_INFO_COMPRESS_MODE_ON						= 0x00000001;

	/* GetLibraryInfo driver-kind value */
	public final static long JAVA_PvAPI_INFO_DRIVER_KIND_EXTENDED					= 0x00000000;
	public final static long JAVA_PvAPI_INFO_DRIVER_KIND_CONVENTIONAL				= 0x00000001;
	public final static long JAVA_PvAPI_INFO_DRIVER_KIND_MP							= 0x00000002;
	public final static long JAVA_PvAPI_INFO_DRIVER_KIND_UUD						= 0x00000004;

	/* GetLibraryInfo Edition value */
	public final static long JAVA_PvAPI_INFO_EDITION_INVALID						= 0x00000000;
	public final static long JAVA_PvAPI_INFO_EDITION_PE								= 0x00000002;
	public final static long JAVA_PvAPI_INFO_EDITION_EE								= 0x00000003;
	public final static long JAVA_PvAPI_INFO_EDITION_F_PE							= 0x00010002;
	public final static long JAVA_PvAPI_INFO_EDITION_F_EE							= 0x00010003;

	/* PvAPI_GetExData Get Extend Data Type */
	public final static long JAVA_PvAPI_EX_DATA_TYPE_0001							= 0x00000000;
	public final static long JAVA_PvAPI_EX_DATA_TYPE_0002							= 0x00000001;
	public final static long JAVA_PvAPI_EX_DATA_TYPE_1001							= 0x00001000;
	public final static long JAVA_PvAPI_EX_DATA_TYPE_1002							= 0x00001001;

	/* PvAPI_GetExData Get Extend Data Size */
	public final static long JAVA_PvAPI_EX_DATA_0001_SIZE							= 4096;
	public final static long JAVA_PvAPI_EX_DATA_1001_SIZE							= 5242880;

}
