package com.fujitsu.frontech.palmsecure.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.fujitsu.frontech.palmsecure.JAVA_BioAPI_BIR;
import com.fujitsu.frontech.palmsecure.JAVA_BioAPI_BIR_BIOMETRIC_DATA_FORMAT;
import com.fujitsu.frontech.palmsecure.JAVA_BioAPI_BIR_HEADER;


public class PalmSecureHelper {

	/**
	 * convert from BIR(BioAPI_BIR) to byte array
	 * @param bir
	 * @return byte array
	 * @throws PalmSecureException
	 * @throws IOException
	 */
	public static byte[] convertBIRToByte (
			JAVA_BioAPI_BIR bir) throws PalmSecureException, IOException {

		byte[] retByte;

		ByteArrayOutputStream bout = null;
		DataOutputStream out = null;

		try
		{
			bout = new ByteArrayOutputStream();
			out = new DataOutputStream(bout);

			// convert byte array
			ConvertEndian.convertEndianInt(out, bir.Header.Length);
			out.writeByte((byte)bir.Header.HeaderVersion);
			out.writeByte((byte)bir.Header.Type);
			ConvertEndian.convertEndianShort(out, bir.Header.Format.FormatOwner);
			ConvertEndian.convertEndianShort(out, bir.Header.Format.FormatID);
			out.writeByte((byte)bir.Header.Quality);
			out.writeByte((byte)bir.Header.Purpose);
			ConvertEndian.convertEndianInt(out, bir.Header.FactorsMask);

			// data address
			out.writeInt(0);

			// signature
			ConvertEndian.convertEndianInt(out, bir.Signature);

			// BIR data
			out.write(bir.BiometricData, 0,
					(int)(bir.Header.Length - PalmSecureConstant.JAVA_BioAPI_BIR_SIZE));

			retByte = bout.toByteArray();

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (out != null)
			{
				out.close();
				out = null;
			}

			if (bout != null)
			{
				bout.close();
				bout = null;
			}
		}

		return retByte;
	}

	/**
	 * convert from byte array to BIR(BioAPI_BIR)
	 * @param data
	 * @return BioAPI_BIR
	 * @throws PalmSecureException
	 * @throws IOException
	 */
	public static JAVA_BioAPI_BIR convertByteToBIR (
			byte[] data) throws PalmSecureException, IOException {

		if(null == data)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw ex;
		}

		JAVA_BioAPI_BIR retBIR = new JAVA_BioAPI_BIR();

		DataInputStream in = null;
		try
		{

			// read BIR data
			in = new DataInputStream(new ByteArrayInputStream(data));

			retBIR.Header = new JAVA_BioAPI_BIR_HEADER();
			retBIR.Header.Format = new JAVA_BioAPI_BIR_BIOMETRIC_DATA_FORMAT();

			// set BIR header
			retBIR.Header.Length = ConvertEndian.convertEndianInt(in);
			retBIR.Header.HeaderVersion = (short)(0x000000FF & in.readByte());
			retBIR.Header.Type = (short)(0x000000FF & in.readByte());
			retBIR.Header.Format.FormatOwner = ConvertEndian.convertEndianShort(in);
			retBIR.Header.Format.FormatID = ConvertEndian.convertEndianShort(in);
			retBIR.Header.Quality = in.readByte();
			retBIR.Header.Purpose = (short)(0x000000FF & in.readByte());
			retBIR.Header.FactorsMask = ConvertEndian.convertEndianInt(in);

			// skip data address
			in.skipBytes(4);

			// signature
			retBIR.Signature = ConvertEndian.convertEndianInt(in);

			// BIR data
			retBIR.BiometricData = new byte[(int)(retBIR.Header.Length - PalmSecureConstant.JAVA_BioAPI_BIR_SIZE)];
			in.read(retBIR.BiometricData, 0,
					(int)(retBIR.Header.Length - PalmSecureConstant.JAVA_BioAPI_BIR_SIZE));


		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (in != null)
			{
				in.close();
				in = null;
			}
		}

		return retBIR;
	}

	/**
	 * convert from byte array to BIR Header(BioAPI_BIR_HEADER)
	 * @param data
	 * @return BioAPI_BIR_HEADER
	 * @throws PalmSecureException
	 * @throws IOException
	 */
	public static JAVA_BioAPI_BIR_HEADER convertByteToBirHeader (
			byte[] data ) throws PalmSecureException, IOException{

		if(null == data)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw ex;
		}

		JAVA_BioAPI_BIR_HEADER retHeader = new JAVA_BioAPI_BIR_HEADER();

		DataInputStream in = null;
		try
		{
			// read BIR Header data
			in = new DataInputStream(new ByteArrayInputStream(data));

			retHeader.Format = new JAVA_BioAPI_BIR_BIOMETRIC_DATA_FORMAT();

			// set BIR header
			retHeader.Length = ConvertEndian.convertEndianInt(in);
			retHeader.HeaderVersion = (short)(0x000000FF & in.readByte());
			retHeader.Type = (short)(0x000000FF & in.readByte());
			retHeader.Format.FormatOwner = (int)ConvertEndian.convertEndianShort(in);
			retHeader.Format.FormatID = (int)ConvertEndian.convertEndianShort(in);
			retHeader.Quality = in.readByte();
			retHeader.Purpose = (short)(0x000000FF & in.readByte());
			retHeader.FactorsMask = ConvertEndian.convertEndianInt(in);

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if ( in != null )
			{
				in.close();
				in = null;
			}
		}


		return retHeader;
	}

}
