package com.fujitsu.frontech.palmsecure.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;

class ConvertEndian {
	// convert from little endian to big endian(for int)
	static long convertEndianInt(DataInputStream in) throws Exception, PalmSecureException {

		long data = 0;
		long tempData = 0;

		try
		{
			tempData = (long)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData;

			tempData = (long)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData * 256;

			tempData = (long)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData * 256 * 256;

			tempData = (long)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData * 256 * 256 * 256;

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		return data;

	}

	// convert from little endian to big endian(for short)
	static int convertEndianShort(DataInputStream in) throws Exception, PalmSecureException {

		int data = 0;
		int tempData = 0;

		try
		{
			tempData = (int)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData;

			tempData = (int)in.readByte();
			if(tempData < 0)
			{
				tempData = 256 + tempData;
			}
			data += tempData * 256;

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return data;
	}

	// convert from big endian to little endian(for int)
	static void convertEndianInt(DataOutputStream out, long data) throws Exception, PalmSecureException {

		byte setData = 0;

		try
		{
			setData = (byte)(data % 256);
			out.writeByte(setData);

			setData = (byte)((data / 256) % 256);
			out.writeByte(setData);

			setData = (byte)((data / (256 * 256)) % 256);
			out.writeByte(setData);

			setData = (byte)((data / (256 * 256 * 256)) % 256);
			out.writeByte(setData);

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
	}

	// convert from big endian to little endian(for short)
	static void convertEndianShort(DataOutputStream out, int data) throws Exception, PalmSecureException {

		byte setData = 0;

		try
		{
			setData = (byte)(data % 256);
			out.writeByte(setData);

			setData = (byte)((data / 256) % 256);
			out.writeByte(setData);

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return;
	}


}
