package com.fujitsu.frontech.palmsecure.util;

public class PalmSecureException extends java.lang.Throwable{

	/** serial */
	private static final long serialVersionUID = 1L;

	/** err number */
	public int ErrNumber;

	/** detail exception */
	public Exception ErrCause;

}
