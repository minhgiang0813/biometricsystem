package com.fujitsu.frontech.palmsecure;

public interface JAVA_BioAPI_GUI_STATE_CALLBACK_IF {

	public long JAVA_BioAPI_GUI_STATE_CALLBACK(
		Object GuiStateCallbackCtx,
		long GuiState,
		short Response,
		long Message,
		short Progress,
		JAVA_BioAPI_GUI_BITMAP SampleBuffer );
}
