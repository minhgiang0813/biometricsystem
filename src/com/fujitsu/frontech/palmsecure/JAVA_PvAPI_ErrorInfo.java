package com.fujitsu.frontech.palmsecure;

public class JAVA_PvAPI_ErrorInfo {

	public long ErrorLevel;

	public long ErrorCode;

	public long ErrorDetail;

	public long ErrorModule;

	public long ErrorOptional1;

	public long ErrorOptional2;

	public long[] APIInfo;

	public long ErrorInfo1;

	public long ErrorInfo2;

	public long[] ErrorInfo3;

}
