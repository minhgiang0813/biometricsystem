package com.fujitsu.frontech.palmsecure;

public class JAVA_PvAPI_LBINFO {

	public long uiLibVersion;

	public long uiLibLevel;

	public long uiLibSubCounter;

	public String szDrvVersion;

	public long uiFwVersion;

	public long uiFwLevel;

	public long uiSensorKind;

	public long uiSensorExtKind;

	public long uiSerialNo;

	public String szUnitNo;

	public long uiLoopMode;

	public long uiCompressMode;

	public long uiDriverKind;

	public long uiEdition;

	public byte[] szReserve;

}
