package com.fujitsu.frontech.palmsecure;

public class JAVA_PvAPI_TemplateInfoEx {

	public long uiVersion;

	public long uiSensor;

	public long uiGuideMode;

	public long uiCompressMode;

	public long uiExtractKind;

	public long uiIndexKind;

	public long uiSensorExtKind;

	public long uiM2ExtInfo;

	public long uiDataExtInfo;

	public long uiGExtendedMode;

	public long[] auiReserve;

	public long[] auiMultiDataMode;

	public byte[] szReserve;

}
