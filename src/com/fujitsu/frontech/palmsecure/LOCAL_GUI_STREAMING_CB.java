package com.fujitsu.frontech.palmsecure;

class LOCAL_GUI_STREAMING_CB {

	private JAVA_BioAPI_GUI_STREAMING_CALLBACK_IF streamingCB = null;

	private Object streamingCBCtx = null;

	void setCallbackObject(JAVA_BioAPI_GUI_STREAMING_CALLBACK_IF cb)
	{
		streamingCB = cb;
	}

	void setCallbackContext(Object ctx)
	{
		streamingCBCtx = ctx;
	}

	long CALLBACK(
			long Width,
			long Height,
			byte[] Bitmap) {
		
		long ret = 0;

		if (streamingCB == null)
		{
			return ret;
		}

		JAVA_BioAPI_GUI_BITMAP SampleBuffer = null;

		if (Bitmap != null)
		{
			SampleBuffer = new JAVA_BioAPI_GUI_BITMAP();
			SampleBuffer.Width = Width;
			SampleBuffer.Height = Height;

			SampleBuffer.Bitmap = new JAVA_BioAPI_DATA();
			SampleBuffer.Bitmap.Length = Bitmap.length;
			SampleBuffer.Bitmap.Data = Bitmap;
		}

		try
		{
			streamingCB.JAVA_BioAPI_GUI_STREAMING_CALLBACK(
					streamingCBCtx,
					SampleBuffer);
		}
		catch(Exception e)
		{

		}

		return ret;
	}

}
