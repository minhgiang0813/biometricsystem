package com.fujitsu.frontech.palmsecure;

public interface JAVA_BioAPI_CALLOC_IF {

	public Object JAVA_BioAPI_CALLOC(
			long Num,
			long Size,
			Object Allocref);
}
