package com.fujitsu.frontech.palmsecure;

public interface JAVA_BioAPI_ModuleEventHandler_IF {

	public long JAVA_BioAPI_ModuleEventHandler(
		byte[] BSPUuid,
		Object AppNotifyCallbackCtx,
		long DeviceID,
		long Reserved,
		long EventType);
}
