package com.fujitsu.frontech.palmsecure;

import java.io.IOException;

import com.fujitsu.frontech.palmsecure.util.PalmSecureConstant;
import com.fujitsu.frontech.palmsecure.util.PalmSecureException;
import com.fujitsu.frontech.palmsecure.util.PalmSecureHelper;


public class PalmSecureIf {

	/** event listener for GUI_STREAMING_CALLBACK */
	private LOCAL_GUI_STREAMING_CB localStreamingCB;

	/** event listener for GUI_STATE_CALLBACK */
	private LOCAL_GUI_STATE_CB localStateCB;

	/** area used in jni area management for multi thread */
	private final static int THREAD_NUM_MAX = 512;
	private static boolean[] NativeAreaHandleAry = new boolean[THREAD_NUM_MAX];
	private int NativeAreaHandle = 0;

	private final static int NATIVE_AREA_HANDLE_GET = 1;
	private final static int NATIVE_AREA_HANDLE_FREE = 2;

	private final static int TEMPLATE_SIZE_MAX = 8192*10;
	private final static int MAX_NUMBER_OF_RESULTS = 30;

	native long BioAPI_ModuleLoad(
			int NativeAreaHandle,
			byte[] ModuleGuid) throws PalmSecureException;

	native long BioAPI_ModuleUnload(
			int NativeAreaHandle,
			byte[] ModuleGuid) throws PalmSecureException;

	native long BioAPI_ModuleAttach(
			int NativeAreaHandle,
			byte[] ModuleGuid,
			LOCAL_LONG NewModuleHandle) throws PalmSecureException;

	native long BioAPI_ModuleDetach(
			int NativeAreaHandle,
			long ModuleHandle) throws PalmSecureException;

	native long BioAPI_FreeBIRHandle(
			int NativeAreaHandle,
			long ModuleHandle,
			int BIRHandle) throws PalmSecureException;

	native long BioAPI_GetBIRFromHandle(
			int NativeAreaHandle,
			long ModuleHandle,
			int BIRHandle,
			byte[] BIR) throws PalmSecureException;

	native long BioAPI_SetGUICallbacks(
			int NativeAreaHandle,
			long ModuleHandle,
			LOCAL_GUI_STREAMING_CB StreamingCB,
			LOCAL_GUI_STATE_CB StateCB) throws PalmSecureException;

	native long BioAPI_Capture(
			int NativeAreaHandle,
			long ModuleHandle,
			short Purpose,
			LOCAL_INT CapturedBIR) throws PalmSecureException;

	native long BioAPI_VerifyMatch(
			int NativeAreaHandle,
			long ModuleHandle,
			int MaxFRRRequested,
			short ProcessedBIRType,
			byte[] ProcessedBIR,
			short StoredTemplateType,
			byte[] StoredTemplate,
			LOCAL_LONG Result,
			LOCAL_INT FARAchieved) throws PalmSecureException;

	native long BioAPI_IdentifyMatch(
			int NativeAreaHandle,
			long ModuleHandle,
			int MaxFARRequested,
			short ProcessedBIRType,
			byte[] ProcessedBIR,
			short PopulationType,
			int PopulationBirCount,
			byte[][] PopulationBirData,
			long MaxNumberOfResults,
			LOCAL_LONG NumberOfResults,
			short[] CandiTypeAry,
			long[] CandiBIRAry,
			int[] CandiFARAry) throws PalmSecureException;

	native long BioAPI_Enroll(
			int NativeAreaHandle,
			long ModuleHandle,
			short Purpose,
			LOCAL_INT NewTemplate) throws PalmSecureException;

	native long BioAPI_Verify(
			int NativeAreaHandle,
			long ModuleHandle,
			int MaxFRRRequested,
			short StoredTemplateType,
			byte[] StoredTemplate,
			LOCAL_LONG Result,
			LOCAL_INT FARAchieved) throws PalmSecureException;

	native long BioAPI_Identify(
			int NativeAreaHandle,
			long ModuleHandle,
			int MaxFRRRequested,
			short PopulationType,
			int PopulationBirCount,
			byte[][] PopulationBirData,
			long MaxNumberOfResults,
			LOCAL_LONG NumberOfResults,
			short[] CandiTypeAry,
			long[] CandiBIRAry,
			int[] CandiFARAry) throws PalmSecureException;

	native long PvAPI_ApAuthenticate(
			int NativeAreaHandle,
			byte[] Key) throws PalmSecureException;

	native long PvAPI_SetProfile(
			int NativeAreaHandle,
			long ModuleHandle,
			long dwFlag,
			long dwParam1) throws PalmSecureException;

	native long PvAPI_SetProfile(
			int NativeAreaHandle,
			long ModuleHandle,
			long dwFlag,
			byte[] dwParam1) throws PalmSecureException;

	native long PvAPI_SetProfile(
			int NativeAreaHandle,
			long ModuleHandle,
			long dwFlag,
			long uiSharedMode,
			byte[] szCaptureSharedKey) throws PalmSecureException;

	native long PvAPI_GetExData(
			int 		NativeAreaHandle,
			long 		ModuleHandle,
			long 		uiExDataType,
			LOCAL_LONG 	Length,
			byte[] 		Data) throws PalmSecureException;

	native long PvAPI_GetErrorInfo(
			int NativeAreaHandle,
			LOCAL_LONG ErrorLevel,
			LOCAL_LONG ErrorCode,
			LOCAL_LONG ErrorDetail,
			LOCAL_LONG ErrorModule,
			LOCAL_LONG ErrorOptional1,
			LOCAL_LONG ErrorOptional2,
			long[] APIInfo,
			LOCAL_LONG ErrorInfo1,
			LOCAL_LONG ErrorInfo2,
			long[] ErrorInfo3) throws PalmSecureException;

	native long PvAPI_Sense(
			int NativeAreaHandle,
			long ModuleHandle,
			long Timeout,
			long Interval,
			long CheckRetryInterval,
			long CheckRetryCount) throws PalmSecureException;

	native long PvAPI_Cancel(
			int NativeAreaHandle,
			long ModuleHandle,
			LOCAL_LONG ErrorLevel,
			LOCAL_LONG ErrorCode,
			LOCAL_LONG ErrorDetail,
			LOCAL_LONG ErrorModule,
			LOCAL_LONG ErrorOptional1,
			LOCAL_LONG ErrorOptional2,
			long[] APIInfo,
			LOCAL_LONG ErrorInfo1,
			LOCAL_LONG ErrorInfo2,
			long[] ErrorInfo3) throws PalmSecureException;

	native long PvAPI_PreSetProfile_1(
			int NativeAreaHandle,
			long uiFlag,
			long serialNo,
			byte[] unitNo) throws PalmSecureException;

	native long PvAPI_PreSetProfile_2(
			int NativeAreaHandle,
			long uiFlag,
			long lpvParamData) throws PalmSecureException;

	native long PvAPI_PresetIdentifyPopulation(
			int NativeAreaHandle,
			long ModuleHandle,
			short PopulationType,
			int PopulationBirCount,
			byte[][] PopulationBirData) throws PalmSecureException;

	native long PvAPI_GetConnectSensorInfoEx(
			int NativeAreaHandle,
			LOCAL_LONG lpuiSensorNum,
			long[] uiSerialNo,
			byte[][] szUnitNo,
			long[] uiSensor,
			long[] uiFwVersion,
			long[] uiFwLevel,
			byte[][] szReserve) throws PalmSecureException;

	native long PvAPI_GetLibraryInfo(
			int NativeAreaHandle,
			LOCAL_LONG uiLibVersion,
			LOCAL_LONG uiLibLevel,
			LOCAL_LONG uiLibSubCounter,
			byte[] szDrvVersion,
			LOCAL_LONG uiFwVersion,
			LOCAL_LONG uiFwLevel,
			LOCAL_LONG uiSensorKind,
			LOCAL_LONG uiSensorExtKind,
			LOCAL_LONG uiSerialNo,
			byte[] szUnitNo,
			LOCAL_LONG uiLoopMode,
			LOCAL_LONG uiCompressMode,
			LOCAL_LONG uiDriverKind,
			LOCAL_LONG uiEdition,
			byte[] szReserve) throws PalmSecureException;

	native long PvAPI_GetTemplateInfoEx(
			int NativeAreaHandle,
			long ModuleHandle,
			short StoredTemplateType,
			byte[] StoredBIR,
			LOCAL_LONG uiVersion,
			LOCAL_LONG uiSensor,
			LOCAL_LONG uiGuideMode,
			LOCAL_LONG uiCompressMode,
			LOCAL_LONG uiExtractKind,
			LOCAL_LONG uiIndexKind,
			LOCAL_LONG uiSensorExtKind,
			LOCAL_LONG uiM2ExtInfo,
			LOCAL_LONG uiDataExtInfo,
			LOCAL_LONG uiGExtendedMode,
			long[] auiReserve,
			long[] auiMultiDataMode,
			byte[] lReserve) throws PalmSecureException;


	/** constractor */
	public PalmSecureIf() throws PalmSecureException {

		System.loadLibrary("f3bc4jni");

		this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
				NATIVE_AREA_HANDLE_GET,
				0);
		if (this.NativeAreaHandle == 0)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			throw e;
		}

	}
	public PalmSecureIf(String lib) throws PalmSecureException {

		System.loadLibrary(lib);

		this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
				NATIVE_AREA_HANDLE_GET,
				0);
		if (this.NativeAreaHandle == 0)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			throw e;
		}
	}

	/** finalize */
	public void finalize()  {
		if (this.NativeAreaHandle != 0)
		{
			PalmSecureIf.getAndFreeNativeAreaHandle(
					NATIVE_AREA_HANDLE_FREE,
					this.NativeAreaHandle);
		}
	}

	/** this method used in jni area management */
	private synchronized static int getAndFreeNativeAreaHandle(int kind, int FreeHndl) {
		int GetHndl = 0;

		if (kind == NATIVE_AREA_HANDLE_FREE)
		{
			if (FreeHndl >= 1 && FreeHndl <= THREAD_NUM_MAX)
			{
				NativeAreaHandleAry[FreeHndl-1] = false;
				return 0;
			}
		}
		else if(kind == NATIVE_AREA_HANDLE_GET)
		{
			for (int i = 1; i <= THREAD_NUM_MAX; i++)
			{
				if (NativeAreaHandleAry[i-1] == false)
				{
					NativeAreaHandleAry[i-1] = true;
					GetHndl = i;
					break;
				}
			}
		}

		return GetHndl;
	}

	/**
	 * This method registers Authentication library modules
	 */
	public long JAVA_BioAPI_ModuleLoad(
			byte[] ModuleGuid,
			JAVA_uint32 Reserved,
			JAVA_BioAPI_ModuleEventHandler_IF AppNotifyCallback,
			Object AppNotifyCallbackCtx) throws PalmSecureException {

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;

		if ( ModuleGuid == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( this.NativeAreaHandle == 0 )
		{
			this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
					NATIVE_AREA_HANDLE_GET,
					0);
		}

		try
		{
			// call JNI
			jni_ret = BioAPI_ModuleLoad(
					this.NativeAreaHandle,
					ModuleGuid);
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}

		return jni_ret;
	}

	/**
	 * This method releases Authentication library modules
	 */
	public long JAVA_BioAPI_ModuleUnload(
			byte[] ModuleGuid,
			JAVA_BioAPI_ModuleEventHandler_IF AppNotifyCallback,
			Object AppNotifyCallbackCtx) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;

		if ( ModuleGuid == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			// call JNI
			jni_ret = BioAPI_ModuleUnload(
					this.NativeAreaHandle,
					ModuleGuid);
			if (jni_ret == PalmSecureConstant.JAVA_BioAPI_OK)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}

		return jni_ret;
	}


	/**
	 * This method attaches Authentication library modules
	 */
	public long JAVA_BioAPI_ModuleAttach(
			byte[] ModuleGuid,
			JAVA_BioAPI_VERSION Version,
			JAVA_BioAPI_MEMORY_FUNCS MemoryFuncs,
			JAVA_uint32 DeviceID,
			JAVA_uint32 Reserved1,
			JAVA_uint32 Reserved2,
			JAVA_uint32 Reserved3,
			JAVA_BioAPI_FUNC_NAME_ADDR FunctionTable,
			JAVA_uint32 NumFunctionTable,
			Object Reserved4,
			JAVA_uint32 NewModuleHandle) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleGuid == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( NewModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_LONG localMHandle = new LOCAL_LONG();

			// call JNI
			jni_ret = BioAPI_ModuleAttach(
					this.NativeAreaHandle,
					ModuleGuid,
					localMHandle);

			NewModuleHandle.value = localMHandle.value;
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}


		return jni_ret;
	}




	/**
	 * This method detaches Authentication library modules
	 */
	public long JAVA_BioAPI_ModuleDetach(
			JAVA_uint32 ModuleHandle) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}


		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			// call JNI
			jni_ret = BioAPI_ModuleDetach(
					this.NativeAreaHandle,
					ModuleHandle.value);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	/**
	 * This method releases BIR handle
	 */
	public long JAVA_BioAPI_FreeBIRHandle(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 BIRHandle) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( BIRHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			// call JNI
			jni_ret = BioAPI_FreeBIRHandle(
					this.NativeAreaHandle,
					ModuleHandle.value,
					BIRHandle.value);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	/**
	 * This method acquires BIR associated with BIR handle
	 */
	public long JAVA_BioAPI_GetBIRFromHandle(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 BIRHandle,
			JAVA_BioAPI_BIR BIR) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( BIRHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( BIR == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] birData = new byte[TEMPLATE_SIZE_MAX];

			// call JNI
			jni_ret = BioAPI_GetBIRFromHandle(
					this.NativeAreaHandle,
					ModuleHandle.value,
					BIRHandle.value,
					birData);

			if (jni_ret == PalmSecureConstant.JAVA_BioAPI_OK)
			{
				// convert from array byte to BIR
				convertByteToBioAPI_BIR(birData, BIR);
			}
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;

	}


	/**
	 * This method sets event listener for status notification and for guidance image notification
	 */
	public long JAVA_BioAPI_SetGUICallbacks(
			JAVA_uint32 ModuleHandle,
			JAVA_BioAPI_GUI_STREAMING_CALLBACK_IF GuiStreamingCallback,
			Object GuiStreamingCallbackCtx,
			JAVA_BioAPI_GUI_STATE_CALLBACK_IF GuiStateCallback,
			Object GuiStateCallbackCtx) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( GuiStateCallback == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			if(localStreamingCB != null)
			{
				localStreamingCB.setCallbackObject(null);
				localStreamingCB.setCallbackContext(null);
			}
			if(localStateCB != null)
			{
				localStateCB.setCallbackObject(null);
				localStateCB.setCallbackContext(null);
			}

			localStreamingCB = new LOCAL_GUI_STREAMING_CB();
			localStateCB = new LOCAL_GUI_STATE_CB();

			// call JNI
			jni_ret = BioAPI_SetGUICallbacks(
					this.NativeAreaHandle,
					ModuleHandle.value,
					localStreamingCB,
					localStateCB);
			if(jni_ret == PalmSecureConstant.JAVA_BioAPI_OK)
			{
				// set callback
				localStreamingCB.setCallbackObject(GuiStreamingCallback);
				localStreamingCB.setCallbackContext(GuiStreamingCallbackCtx);

				localStateCB.setCallbackObject(GuiStateCallback);
				localStateCB.setCallbackContext(GuiStateCallbackCtx);
			}

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	/**
	 * This method captures palm vein of one hand and creates authentication data
	 */
	public long JAVA_BioAPI_Capture(
			JAVA_uint32 ModuleHandle,
			JAVA_uint8 Purpose,
			JAVA_sint32 CapturedBIR,
			JAVA_sint32 Timeout,
			JAVA_sint32 AuditData) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Purpose == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( CapturedBIR == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_INT localCBIR = new LOCAL_INT();

			// call JNI
			jni_ret = BioAPI_Capture(
					this.NativeAreaHandle,
					ModuleHandle.value,
					Purpose.value,
					localCBIR);

			CapturedBIR.value = localCBIR.value;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method performs verification between captured authentication data and enrollment data
	 */
	public long JAVA_BioAPI_VerifyMatch(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 MaxFARRequested,
			JAVA_sint32 MaxFRRRequested,
			JAVA_uint32 FARPrecedence,
			JAVA_BioAPI_INPUT_BIR ProcessedBIR,
			JAVA_BioAPI_INPUT_BIR StoredTemplate,
			JAVA_sint32 AdaptedBIR,
			JAVA_uint32 Result,
			JAVA_sint32 FARAchieved,
			JAVA_sint32 FRRAchieved,
			JAVA_BioAPI_DATA Payload) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxFRRRequested == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( ProcessedBIR == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( StoredTemplate == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Result == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( FARAchieved == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		byte[] lProcessedBIR = null;
		byte[] lStoredBIR = null;

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			if (ProcessedBIR.Form == PalmSecureConstant.JAVA_BioAPI_FULLBIR_INPUT)
			{
				if (ProcessedBIR.BIR != null)
				{
					lProcessedBIR = PalmSecureHelper.convertBIRToByte(ProcessedBIR.BIR);
				}
				else
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}
			}

			if (StoredTemplate.Form == PalmSecureConstant.JAVA_BioAPI_FULLBIR_INPUT)
			{
				if (StoredTemplate.BIR != null)
				{
					lStoredBIR = PalmSecureHelper.convertBIRToByte(StoredTemplate.BIR);
				}
				else
				{
					PalmSecureException ex = new PalmSecureException();
					ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw ex;
				}
			}

			LOCAL_LONG localResult = new LOCAL_LONG();
			LOCAL_INT localFAR = new LOCAL_INT();

			// call JNI
			jni_ret = BioAPI_VerifyMatch(
					this.NativeAreaHandle,
					ModuleHandle.value,
					MaxFRRRequested.value,
					ProcessedBIR.Form,
					lProcessedBIR,
					StoredTemplate.Form,
					lStoredBIR,
					localResult,
					localFAR);

			Result.value = localResult.value;
			FARAchieved.value = localFAR.value;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}



	/**
	 * This method performs identification process by searching entire enrollment data
	 */
	public long JAVA_BioAPI_IdentifyMatch(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 MaxFARRequested,
			JAVA_sint32 MaxFRRRequested,
			JAVA_uint32 FARPrecedence,
			JAVA_BioAPI_INPUT_BIR ProcessedBIR,
			JAVA_BioAPI_IDENTIFY_POPULATION Population,
			JAVA_uint32 Binning,
			JAVA_uint32 MaxNumberOfResults,
			JAVA_uint32 NumberOfResults,
			JAVA_BioAPI_CANDIDATE[] Candidates,
			JAVA_sint32 Timeout) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxFRRRequested == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( ProcessedBIR == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Population == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxNumberOfResults == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( NumberOfResults == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( (Candidates == null) || (Candidates.length < MaxNumberOfResults.value) )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] lProcessedBIR = null;

			if (ProcessedBIR.Form == PalmSecureConstant.JAVA_BioAPI_FULLBIR_INPUT)
			{
				if (ProcessedBIR.BIR != null)
				{
					lProcessedBIR = PalmSecureHelper.convertBIRToByte(ProcessedBIR.BIR);
				}
				else
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}
			}


			byte[][] lPopulation = null;
			int lPopulationCount = 0;

			if ( Population.Type ==  PalmSecureConstant.JAVA_BioAPI_ARRAY_TYPE)
			{
				if ((Population.BIRArray == null) || (Population.BIRArray.Members == null) ||
				(Population.BIRArray.NumberOfMembers <= 0) ||
				(Population.BIRArray.NumberOfMembers > Population.BIRArray.Members.length))
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}

				for (int i = 0; i < Population.BIRArray.NumberOfMembers; i++)
				{
					if ((Population.BIRArray.Members[i] == null) ||
					(Population.BIRArray.Members[i] instanceof JAVA_BioAPI_BIR != true))
					{
						PalmSecureException e = new PalmSecureException();
						e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
						throw e;
					}
				}

				lPopulationCount = (int)Population.BIRArray.NumberOfMembers;
				lPopulation = new byte[lPopulationCount][];

				for (int i=0; i < lPopulationCount; i++)
				{
					/* Set BIR Data */
					lPopulation[i] =
							PalmSecureHelper.convertBIRToByte(Population.BIRArray.Members[i]);

				}
			}

			LOCAL_LONG localResults = new LOCAL_LONG();
			short[] localCandiType = new short[MAX_NUMBER_OF_RESULTS];
			long[] localCandiBIR = new long[MAX_NUMBER_OF_RESULTS];
			int[] localCandiFAR = new int[MAX_NUMBER_OF_RESULTS];

			// call JNI
			jni_ret = BioAPI_IdentifyMatch(
					this.NativeAreaHandle,
					ModuleHandle.value,
					MaxFRRRequested.value,
					ProcessedBIR.Form,
					lProcessedBIR,
					Population.Type,
					lPopulationCount,
					lPopulation,
					MaxNumberOfResults.value,
					localResults,
					localCandiType,
					localCandiBIR,
					localCandiFAR);

			NumberOfResults.value = localResults.value;

			for(int i = 0; i < localResults.value; i++)
			{
				Candidates[i] = new JAVA_BioAPI_CANDIDATE();
				Candidates[i].Type = localCandiType[i];
				Candidates[i].BIRInArray = localCandiBIR[i];
				Candidates[i].FARAchieved = localCandiFAR[i];
			}
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	/**
	 * This method captures palm vein on a single hand and creates enrollment data
	 */
	public long JAVA_BioAPI_Enroll(
			JAVA_uint32 ModuleHandle,
			JAVA_uint8 Purpose,
			JAVA_BioAPI_INPUT_BIR StoredTemplate,
			JAVA_sint32 NewTemplate,
			JAVA_BioAPI_DATA Payload,
			JAVA_sint32 Timeout,
			JAVA_sint32 AuditData) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Purpose == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( NewTemplate == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}


		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_INT localNTemplate = new LOCAL_INT();

			// call JNI
			jni_ret = BioAPI_Enroll(
					this.NativeAreaHandle,
					ModuleHandle.value,
					Purpose.value,
					localNTemplate);

			NewTemplate.value = localNTemplate.value;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}


		return jni_ret;
	}



	/**
	 * This method captures palm vein and performs verification
	 * between captured palm vein and enrollment data
	 */
	public long JAVA_BioAPI_Verify(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 MaxFARRequested,
			JAVA_sint32 MaxFRRRequested,
			JAVA_uint32 FARPrecedence,
			JAVA_BioAPI_INPUT_BIR StoredTemplate,
			JAVA_sint32 AdaptedBIR,
			JAVA_uint32 Result,
			JAVA_sint32 FARAchieved,
			JAVA_sint32 FRRAchieved,
			JAVA_BioAPI_DATA Payload,
			JAVA_sint32 Timeout,
			JAVA_sint32 AuditData) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxFRRRequested == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( StoredTemplate == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Result == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( FARAchieved == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] lStoredBIR = null;

			if (StoredTemplate.Form == PalmSecureConstant.JAVA_BioAPI_FULLBIR_INPUT)
			{
				if (StoredTemplate.BIR != null)
				{
					lStoredBIR = PalmSecureHelper.convertBIRToByte(StoredTemplate.BIR);
				}
				else
				{
					PalmSecureException ex = new PalmSecureException();
					ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw ex;
				}
			}

			LOCAL_LONG localResult = new LOCAL_LONG();
			LOCAL_INT localFAR = new LOCAL_INT();

			// call JNI
			jni_ret = BioAPI_Verify(
					this.NativeAreaHandle,
					ModuleHandle.value,
					MaxFRRRequested.value,
					StoredTemplate.Form,
					lStoredBIR,
					localResult,
					localFAR);

			Result.value = localResult.value;
			FARAchieved.value = localFAR.value;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}



	/**
	 * This method captures palm vein and performs identification process
	 * by searching entire enrollment data
	 */
	public long JAVA_BioAPI_Identify(
			JAVA_uint32 ModuleHandle,
			JAVA_sint32 MaxFARRequested,
			JAVA_sint32 MaxFRRRequested,
			JAVA_uint32 FARPrecedence,
			JAVA_BioAPI_IDENTIFY_POPULATION Population,
			JAVA_uint32 Binning,
			JAVA_uint32 MaxNumberOfResults,
			JAVA_uint32 NumberOfResults,
			JAVA_BioAPI_CANDIDATE[] Candidates,
			JAVA_sint32 Timeout,
			JAVA_sint32 AuditData) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxFRRRequested == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Population == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( MaxNumberOfResults == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( NumberOfResults == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( (Candidates == null) || (Candidates.length < MaxNumberOfResults.value))
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try {
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[][] lPopulation = null;
			int lPopulationCount = 0;

			if ( Population.Type ==  PalmSecureConstant.JAVA_BioAPI_ARRAY_TYPE)
			{
				if ( (Population.BIRArray == null) || (Population.BIRArray.Members == null) ||
				(Population.BIRArray.NumberOfMembers <= 0) ||
				(Population.BIRArray.NumberOfMembers > Population.BIRArray.Members.length))
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}

				for ( int i = 0; i < Population.BIRArray.NumberOfMembers; i++ )
				{
					if ( (Population.BIRArray.Members[i] == null) ||
					(Population.BIRArray.Members[i] instanceof JAVA_BioAPI_BIR != true) )
					{
						PalmSecureException e = new PalmSecureException();
						e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
						throw e;
					}
				}

				lPopulationCount = (int)Population.BIRArray.NumberOfMembers;
				lPopulation = new byte[lPopulationCount][];

				for (int i=0; i < lPopulationCount; i++)
				{
					/* Set BIR Data */
					lPopulation[i] =
							PalmSecureHelper.convertBIRToByte(Population.BIRArray.Members[i]);
				}
			}

			LOCAL_LONG localResults = new LOCAL_LONG();
			short[] localCandiType = new short[MAX_NUMBER_OF_RESULTS];
			long[] localCandiBIR = new long[MAX_NUMBER_OF_RESULTS];
			int[] localCandiFAR = new int[MAX_NUMBER_OF_RESULTS];

			jni_ret = BioAPI_Identify(
					this.NativeAreaHandle,
					ModuleHandle.value,
					MaxFRRRequested.value,
					Population.Type,
					lPopulationCount,
					lPopulation,
					MaxNumberOfResults.value,
					localResults,
					localCandiType,
					localCandiBIR,
					localCandiFAR);

			NumberOfResults.value = localResults.value;

			for(int i = 0; i < localResults.value; i++)
			{
				Candidates[i] = new JAVA_BioAPI_CANDIDATE();
				Candidates[i].Type = localCandiType[i];
				Candidates[i].BIRInArray = localCandiBIR[i];
				Candidates[i].FARAchieved = localCandiFAR[i];
			}

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}



	/**
	 * This method authenticates an application using specified key
	 */
	public long JAVA_PvAPI_ApAuthenticate(
			String Key) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;

		if ( Key == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			byte[] localKey = null;

			// convert from utf-8 to shift jis
			localKey = Key.getBytes("US-ASCII");

			// call JNI
			jni_ret = PvAPI_ApAuthenticate(
					this.NativeAreaHandle,
					localKey);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return jni_ret;
	}

	/**
	 * This method sets operation mode of Authentication library
	 */
	public long JAVA_PvAPI_SetProfile(
			JAVA_uint32 ModuleHandle,
			JAVA_uint32 dwFlag,
			JAVA_uint32 dwParam1,
			JAVA_uint32 dwParam2,
			JAVA_uint32 dwReserve) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( dwFlag == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( dwParam1 == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			// call JNI
			jni_ret = PvAPI_SetProfile(
					this.NativeAreaHandle,
					ModuleHandle.value,
					dwFlag.value,
					dwParam1.value);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method sets operation mode of Authentication library
	 */
	public long JAVA_PvAPI_SetProfile(
			JAVA_uint32 ModuleHandle,
			JAVA_uint32 dwFlag,
			String dwParam1,
			JAVA_uint32 dwParam2,
			JAVA_uint32 dwReserve) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( dwFlag == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

//		if ( dwParam1 == null )
//		{
//			PalmSecureException e = new PalmSecureException();
//			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
//			throw e;
//		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] localParam1 = null;

			// convert from utf-8 to shift jis
//			localParam1 = dwParam1.getBytes("Shift_JIS");

			if ( dwParam1 != null )
			{
				int len = dwParam1.length();
				localParam1 = new byte[len + 1];
				System.arraycopy(dwParam1.getBytes("Shift_JIS"), 0, localParam1, 0, len);
			}

			// call JNI
			jni_ret = PvAPI_SetProfile(
					this.NativeAreaHandle,
					ModuleHandle.value,
					dwFlag.value,
					localParam1);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method sets operation mode of Authentication library
	 */
	public long JAVA_PvAPI_SetProfile(
			JAVA_uint32                      ModuleHandle,
			JAVA_uint32                      dwFlag,
			JAVA_PvAPI_SharedCaptureDataInfo dwParam1,
			JAVA_uint32                      dwParam2,
			JAVA_uint32                      dwReserve) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( dwFlag == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( dwParam1 == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] localParam1 = null;

			// convert from utf-8 to shift jis
			//localParam1 = dwParam1.getBytes("Shift_JIS");

			if ( dwParam1.szCaptureSharedKey != null)
			{
				int len = dwParam1.szCaptureSharedKey.length();
				localParam1 = new byte[len + 1];
				System.arraycopy(dwParam1.szCaptureSharedKey.getBytes("Shift_JIS"), 0, localParam1, 0, len);
			}

			// call JNI
			jni_ret = PvAPI_SetProfile(
					this.NativeAreaHandle,
					ModuleHandle.value,
					dwFlag.value,
					dwParam1.uiSharedMode,
					localParam1);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method acquires I-format data
	 */
	public long JAVA_PvAPI_GetExData(
			JAVA_uint32                      ModuleHandle,
			JAVA_uint32                      uiExDataType,
			JAVA_BioAPI_DATA                 ptExData) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( (uiExDataType == null) || (ptExData == null))
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_LONG localLength = new LOCAL_LONG();
			byte[] localData = new byte[TEMPLATE_SIZE_MAX];

			// call JNI
			jni_ret = PvAPI_GetExData(
					this.NativeAreaHandle,
					ModuleHandle.value,
					uiExDataType.value,
					localLength,
					localData);

			ptExData.Length = localLength.value;
			if ( ptExData.Length != 0 )
			{
				byte[] retData = new byte[(int)localLength.value];
				System.arraycopy(localData, 0, retData, 0, (int)localLength.value);
				ptExData.Data = retData;
			}
			else
			{
				ptExData.Data = null;
			}
		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method acquires error information
	 */
	public void JAVA_PvAPI_GetErrorInfo(
			JAVA_PvAPI_ErrorInfo ErrorInfo) throws PalmSecureException{

		if ( ErrorInfo == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			LOCAL_LONG errorLevel = new LOCAL_LONG();
			LOCAL_LONG errorCode = new LOCAL_LONG();
			LOCAL_LONG errorDetail = new LOCAL_LONG();
			LOCAL_LONG errorModule = new LOCAL_LONG();
			LOCAL_LONG errorOptional1 = new LOCAL_LONG();
			LOCAL_LONG errorOptional2 = new LOCAL_LONG();
			long[] apiInfo = new long[4];
			LOCAL_LONG errorInfo1 = new LOCAL_LONG();
			LOCAL_LONG errorInfo2 = new LOCAL_LONG();
			long[] errorInfo3 = new long[4];

			// call JNI
			PvAPI_GetErrorInfo(
					this.NativeAreaHandle,
					errorLevel,
					errorCode,
					errorDetail,
					errorModule,
					errorOptional1,
					errorOptional2,
					apiInfo,
					errorInfo1,
					errorInfo2,
					errorInfo3);

			ErrorInfo.ErrorLevel = errorLevel.value;
			ErrorInfo.ErrorCode = errorCode.value;
			ErrorInfo.ErrorDetail = errorDetail.value;
			ErrorInfo.ErrorModule = errorModule.value;
			ErrorInfo.ErrorOptional1 = errorOptional1.value;
			ErrorInfo.ErrorOptional2 = errorOptional2.value;
			ErrorInfo.APIInfo = apiInfo;
			ErrorInfo.ErrorInfo1 = errorInfo1.value;
			ErrorInfo.ErrorInfo2 = errorInfo2.value;
			ErrorInfo.ErrorInfo3 = errorInfo3;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return;
	}

	/**
	 * This method detects if a hand is placed over sensor
	 */
	public long JAVA_PvAPI_Sense(
			JAVA_uint32 ModuleHandle,
			JAVA_uint32 Timeout,
			JAVA_uint32 Interval,
			JAVA_uint32 CheckRetryInterval,
			JAVA_uint32 CheckRetryCount) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Timeout == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Interval == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( CheckRetryInterval == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( CheckRetryCount == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			// call JNI
			jni_ret = PvAPI_Sense(
					this.NativeAreaHandle,
					ModuleHandle.value,
					Timeout.value,
					Interval.value,
					CheckRetryInterval.value,
					CheckRetryCount.value);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method cancels a process
	 */
	public long JAVA_PvAPI_Cancel(
			JAVA_uint32 ModuleHandle,
			JAVA_PvAPI_ErrorInfo ErrorInfo) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( ErrorInfo == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			LOCAL_LONG errorLevel = new LOCAL_LONG();
			LOCAL_LONG errorCode = new LOCAL_LONG();
			LOCAL_LONG errorDetail = new LOCAL_LONG();
			LOCAL_LONG errorModule = new LOCAL_LONG();
			LOCAL_LONG errorOptional1 = new LOCAL_LONG();
			LOCAL_LONG errorOptional2 = new LOCAL_LONG();
			long[] apiInfo = new long[4];
			LOCAL_LONG errorInfo1 = new LOCAL_LONG();
			LOCAL_LONG errorInfo2 = new LOCAL_LONG();
			long[] errorInfo3 = new long[4];

			// call JNI
			jni_ret = PvAPI_Cancel(
					this.NativeAreaHandle,
					ModuleHandle.value,
			 		errorLevel,
			   		errorCode,
			   		errorDetail,
			   		errorModule,
			   		errorOptional1,
			   		errorOptional2,
			   		apiInfo,
			   		errorInfo1,
			   		errorInfo2,
			   		errorInfo3);

			ErrorInfo.ErrorLevel = errorLevel.value;
			ErrorInfo.ErrorCode = errorCode.value;
			ErrorInfo.ErrorDetail = errorDetail.value;
			ErrorInfo.ErrorModule = errorModule.value;
			ErrorInfo.ErrorOptional1 = errorOptional1.value;
			ErrorInfo.ErrorOptional2 = errorOptional2.value;
			ErrorInfo.APIInfo = apiInfo;
			ErrorInfo.ErrorInfo1 = errorInfo1.value;
			ErrorInfo.ErrorInfo2 = errorInfo2.value;
			ErrorInfo.ErrorInfo3 = errorInfo3;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return jni_ret;
	}

	/**
	 * This method sets information for sensor initialization
	 */
	public long JAVA_PvAPI_PreSetProfile(
			JAVA_uint32 uiFlag,
			Object lpvParamData,
			JAVA_uint32 uiParamDataSize,
			Object lpvReserve) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( uiFlag == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( lpvParamData == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			if ( uiFlag.value == PalmSecureConstant.JAVA_PvAPI_PRE_PROFILE_IDENTIFYSENSOR )
			{
				if (lpvParamData instanceof JAVA_PvAPI_SensorInfo != true)
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}

				if (((JAVA_PvAPI_SensorInfo)lpvParamData).szUnitNo == null)
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}

				LOCAL_LONG localSerialNo = new LOCAL_LONG();
				byte[] localUnitNo = null;

				localSerialNo.value = ((JAVA_PvAPI_SensorInfo)lpvParamData).uiSerialNo;

				// convert from utf-8 to shift jis
				localUnitNo = ((JAVA_PvAPI_SensorInfo)lpvParamData).szUnitNo.getBytes("Shift_JIS");

				// call JNI
				jni_ret = PvAPI_PreSetProfile_1(
						this.NativeAreaHandle,
						uiFlag.value,
						localSerialNo.value,
						localUnitNo);

			}
			else
			{
				if ( lpvParamData instanceof JAVA_uint32 == true )
				{
					// call JNI
					jni_ret = PvAPI_PreSetProfile_2(
							this.NativeAreaHandle,
							uiFlag.value,
							((JAVA_uint32) lpvParamData).value);
				}
				else
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}
			}

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	/**
	 * this method sets target palm vein data group to Authentication library in advance
	 */
	public long JAVA_PvAPI_PresetIdentifyPopulation(
			JAVA_uint32 ModuleHandle,
			JAVA_BioAPI_IDENTIFY_POPULATION Population) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( ModuleHandle == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( Population == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[][] lPopulation = null;
			int lPopulationCount = 0;

			if ( Population.Type ==  PalmSecureConstant.JAVA_BioAPI_ARRAY_TYPE)
			{
				if ( (Population.BIRArray == null) || (Population.BIRArray.Members == null) ||
				(Population.BIRArray.NumberOfMembers <= 0) ||
				(Population.BIRArray.NumberOfMembers > Population.BIRArray.Members.length))
				{
					PalmSecureException e = new PalmSecureException();
					e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw e;
				}

				for ( int i = 0; i < Population.BIRArray.NumberOfMembers; i++ )
				{
					if ( (Population.BIRArray.Members[i] == null) ||
					(Population.BIRArray.Members[i] instanceof JAVA_BioAPI_BIR != true))
					{
						PalmSecureException e = new PalmSecureException();
						e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
						throw e;
					}
				}

				lPopulationCount = (int)Population.BIRArray.NumberOfMembers;
				lPopulation = new byte[lPopulationCount][];

				for (int i=0; i < lPopulationCount; i++)
				{
					/* Set BIR Data */
					lPopulation[i] =
							PalmSecureHelper.convertBIRToByte(Population.BIRArray.Members[i]);
				}
			}

			jni_ret = PvAPI_PresetIdentifyPopulation(
					this.NativeAreaHandle,
					ModuleHandle.value,
					Population.Type,
					lPopulationCount,
					lPopulation);

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}


		return jni_ret;
	}


	/**
	 * This method searches Sensors connected to target hardware to acquire Sensor information
	 */
	public long JAVA_PvAPI_GetConnectSensorInfoEx(
			JAVA_uint32 lpuiSensorNum,
			JAVA_PvAPI_SensorInfoEx[] lptSensorInfo) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( lpuiSensorNum == null )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		if ( (lptSensorInfo == null) || (lptSensorInfo.length < PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX))
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_LONG lSensorNum = new LOCAL_LONG();
			long[] lSerialNo = new long[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX];
			byte[][] lUnitNo = new byte[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX][];
			long[] lSensor = new long[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX];
			long[] lFwVersion = new long[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX];
			long[] lFwLevel = new long[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX];
			byte[][] lReserve = new byte[(int)PalmSecureConstant.JAVA_PvAPI_GET_SENSOR_INFO_MAX][];

			// call JNI
			jni_ret = PvAPI_GetConnectSensorInfoEx(
					this.NativeAreaHandle,
					lSensorNum,
					lSerialNo,
					lUnitNo,
					lSensor,
					lFwVersion,
					lFwLevel,
					lReserve);

			lpuiSensorNum.value = lSensorNum.value;

			for(int i = 0; i < lSensorNum.value; i++)
			{
				lptSensorInfo[i] = new JAVA_PvAPI_SensorInfoEx();
				lptSensorInfo[i].uiSerialNo = lSerialNo[i];

				int k = 0;
				for ( k = 0; k < lUnitNo[i].length; k++ )
				{
					if ( lUnitNo[i][k] == 0 )
					{
						break;
					}
				}

				if ( k == 0 )
				{
					lptSensorInfo[i].szUnitNo = new String("");
				}
				else
				{
					lptSensorInfo[i].szUnitNo = new String(lUnitNo[i], 0, k, "Shift_JIS");
				}
				lptSensorInfo[i].uiSensor = lSensor[i];
				lptSensorInfo[i].uiFwVersion = lFwVersion[i];
				lptSensorInfo[i].uiFwLevel = lFwLevel[i];
				lptSensorInfo[i].szReserve = null;
			}

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method acquires file version of Authentication library
	 */
	public long JAVA_PvAPI_GetLibraryInfo(
			JAVA_PvAPI_LBINFO lptLBInfo) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( (lptLBInfo == null) || (lptLBInfo instanceof JAVA_PvAPI_LBINFO != true) )
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			LOCAL_LONG lLibVersion = new LOCAL_LONG();
			LOCAL_LONG lLibLevel = new LOCAL_LONG();
			LOCAL_LONG lLibSubCounter = new LOCAL_LONG();
			byte[] lDrvVersion = new byte[16];
			LOCAL_LONG lFwVersion = new LOCAL_LONG();
			LOCAL_LONG lFwLevel = new LOCAL_LONG();
			LOCAL_LONG lSensorKind = new LOCAL_LONG();
			LOCAL_LONG lSensorExtKind = new LOCAL_LONG();
			LOCAL_LONG lSerialNo = new LOCAL_LONG();
			byte[] lUnitNo = new byte[16];
			LOCAL_LONG lLoopMode = new LOCAL_LONG();
			LOCAL_LONG lCompressMode = new LOCAL_LONG();
			LOCAL_LONG lDriverKind = new LOCAL_LONG();
			LOCAL_LONG lEdition = new LOCAL_LONG();
			byte[] lReserve = new byte[496];

			// call JNI
			jni_ret = PvAPI_GetLibraryInfo(
					this.NativeAreaHandle,
					lLibVersion,
					lLibLevel,
					lLibSubCounter,
					lDrvVersion,
					lFwVersion,
					lFwLevel,
					lSensorKind,
					lSensorExtKind,
					lSerialNo,
					lUnitNo,
					lLoopMode,
					lCompressMode,
					lDriverKind,
					lEdition,
					lReserve);

			lptLBInfo.uiLibVersion = lLibVersion.value;
			lptLBInfo.uiLibLevel = lLibLevel.value;
			lptLBInfo.uiLibSubCounter = lLibSubCounter.value;

			int k = 0;
			for ( k = 0; k < lDrvVersion.length; k++ )
			{
				if ( lDrvVersion[k] == 0 )
				{
					break;
				}
			}
			if ( k == 0 )
			{
				lptLBInfo.szDrvVersion = new String("");
			}
			else
			{
				lptLBInfo.szDrvVersion = new String(lDrvVersion, 0, k, "Shift_JIS");
			}

			lptLBInfo.uiFwVersion = lFwVersion.value;
			lptLBInfo.uiFwLevel = lFwLevel.value;
			lptLBInfo.uiSensorKind = lSensorKind.value;
			lptLBInfo.uiSensorExtKind = lSensorExtKind.value;
			lptLBInfo.uiSerialNo = lSerialNo.value;

			for ( k = 0; k < lUnitNo.length; k++ )
			{
				if ( lUnitNo[k] == 0 )
				{
					break;
				}
			}
			if ( k == 0 )
			{
				lptLBInfo.szUnitNo = new String("");
			}
			else
			{
				lptLBInfo.szUnitNo = new String(lUnitNo, 0, k, "Shift_JIS");
			}

			lptLBInfo.uiLoopMode = lLoopMode.value;
			lptLBInfo.uiCompressMode = lCompressMode.value;
			lptLBInfo.uiDriverKind = lDriverKind.value;
			lptLBInfo.uiEdition = lEdition.value;
			lptLBInfo.szReserve = null;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}

	/**
	 * This method acquires file version of Authentication library
	 */
	public long JAVA_PvAPI_GetTemplateInfoEx(
			JAVA_uint32 ModuleHandle,
			JAVA_BioAPI_INPUT_BIR StoredTemplate,
			JAVA_PvAPI_TemplateInfoEx TemplateInfo) throws PalmSecureException{

		long jni_ret = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
		boolean freeHndl = false;

		if ( (ModuleHandle == null) || (StoredTemplate == null) || (TemplateInfo == null))
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			throw e;
		}

		try
		{
			if ( this.NativeAreaHandle == 0 )
			{
				this.NativeAreaHandle = PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_GET,
						0);
				freeHndl = true;
			}

			byte[] lStoredBIR = null;

			if (StoredTemplate.Form == PalmSecureConstant.JAVA_BioAPI_FULLBIR_INPUT)
			{
				if (StoredTemplate.BIR != null)
				{
					lStoredBIR = PalmSecureHelper.convertBIRToByte(StoredTemplate.BIR);
				}
				else
				{
					PalmSecureException ex = new PalmSecureException();
					ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
					throw ex;
				}
			}

			LOCAL_LONG lVersion = new LOCAL_LONG();
			LOCAL_LONG lSensor = new LOCAL_LONG();
			LOCAL_LONG lGuideMode = new LOCAL_LONG();
			LOCAL_LONG lCompressMode = new LOCAL_LONG();
			LOCAL_LONG lExtractKind = new LOCAL_LONG();
			LOCAL_LONG lIndexKind = new LOCAL_LONG();
			LOCAL_LONG lSensorExtKind = new LOCAL_LONG();
			LOCAL_LONG lM2ExtInfo = new LOCAL_LONG();
			LOCAL_LONG lDataExtInfo = new LOCAL_LONG();
			LOCAL_LONG lGExtendedMode = new LOCAL_LONG();
			long[] lReserve = new long[6];
			long[] lMultiDataMode = new long[2];
			byte[] lReserve2 = new byte[440];

			// call JNI
			jni_ret = PvAPI_GetTemplateInfoEx(
					this.NativeAreaHandle,
					ModuleHandle.value,
					StoredTemplate.Form,
					lStoredBIR,
					lVersion,
					lSensor,
					lGuideMode,
					lCompressMode,
					lExtractKind,
					lIndexKind,
					lSensorExtKind,
					lM2ExtInfo,
					lDataExtInfo,
					lGExtendedMode,
					lReserve,
					lMultiDataMode,
					lReserve2);

			TemplateInfo.uiVersion = lVersion.value;
			TemplateInfo.uiSensor = lSensor.value;
			TemplateInfo.uiGuideMode = lGuideMode.value;
			TemplateInfo.uiCompressMode = lCompressMode.value;
			TemplateInfo.uiExtractKind = lExtractKind.value;
			TemplateInfo.uiIndexKind = lIndexKind.value;
			TemplateInfo.uiSensorExtKind = lSensorExtKind.value;
			TemplateInfo.uiM2ExtInfo = lM2ExtInfo.value;
			TemplateInfo.uiDataExtInfo = lDataExtInfo.value;
			TemplateInfo.uiGExtendedMode = lGExtendedMode.value;
			TemplateInfo.auiReserve = null;
			TemplateInfo.auiMultiDataMode = lMultiDataMode;
			TemplateInfo.szReserve = null;

		}
		catch (PalmSecureException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			PalmSecureException e = new PalmSecureException();
			e.ErrNumber = PalmSecureConstant.ELSE_EXCEPTION;
			e.ErrCause = ex;
			throw e;
		}
		finally
		{
			if (freeHndl)
			{
				PalmSecureIf.getAndFreeNativeAreaHandle(
						NATIVE_AREA_HANDLE_FREE,
						this.NativeAreaHandle);
				this.NativeAreaHandle = 0;
			}
		}

		return jni_ret;
	}


	// convert from array byte to BioAPI_BIR
	private void convertByteToBioAPI_BIR (
			byte[] data,
			JAVA_BioAPI_BIR Bir) throws PalmSecureException, IOException{

		try
		{
			JAVA_BioAPI_BIR localBir = PalmSecureHelper.convertByteToBIR(data);

			Bir.Header = localBir.Header;
			Bir.BiometricData = localBir.BiometricData;
			Bir.Signature = localBir.Signature;

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}

		return;
	}

	// convert from array byte to BioAPI_BIR_HEADER
	private void convertByteToBioAPI_BIR_HEADER (
			byte[] data,
			JAVA_BioAPI_BIR_HEADER BirHeader) throws PalmSecureException, IOException{

		try
		{
			JAVA_BioAPI_BIR_HEADER localHeader = PalmSecureHelper.convertByteToBirHeader(data);

			BirHeader.Format = new JAVA_BioAPI_BIR_BIOMETRIC_DATA_FORMAT();

			// set BIR header
			BirHeader.Length = localHeader.Length;
			BirHeader.HeaderVersion = localHeader.HeaderVersion;
			BirHeader.Type = localHeader.Type;
			BirHeader.Format.FormatOwner = localHeader.Format.FormatOwner;
			BirHeader.Format.FormatID = localHeader.Format.FormatID;
			BirHeader.Quality = localHeader.Quality;
			BirHeader.Purpose = localHeader.Purpose;
			BirHeader.FactorsMask = localHeader.FactorsMask;

		}
		catch (Exception e)
		{
			PalmSecureException ex = new PalmSecureException();
			ex.ErrNumber = PalmSecureConstant.ARGUMENT_EXCEPTION;
			ex.ErrCause = e;
			throw ex;
		}



		return;
	}

}