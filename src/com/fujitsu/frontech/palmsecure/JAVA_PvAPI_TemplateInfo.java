package com.fujitsu.frontech.palmsecure;

public class JAVA_PvAPI_TemplateInfo {

	public long uiVersion;
	public long uiSensor;
	public long uiGuideMode;
	public long uiCompressMode;
	public long uiExtractKind;
	public long uiIndexKind;
	public long uiReserve1;
	public byte[] lpvReserve2;
}
