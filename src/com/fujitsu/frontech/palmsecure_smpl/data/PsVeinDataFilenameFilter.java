/*
 *	PsVeinDataFilenameFilter.java
 *
 *	All Rights Reserved, Copyright(c) FUJITSU FRONTECH LIMITED 2021
 */

package com.fujitsu.frontech.palmsecure_smpl.data;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fujitsu.frontech.palmsecure.util.PalmSecureConstant;

public class PsVeinDataFilenameFilter implements FilenameFilter {

	private static final String VEIN_DATA_FILE_EXT = ".dat";
	private static final int ID_LENGTH = 16;
	private static String sensorType;
	private static String dataType;
//	private static String guideMode;
	private static final Map<String, List<String>> COMPATIBILITY_MAP;
	static {
		Map<String, List<String>> map = new HashMap<>();
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_1), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_1)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_3), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_3)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_4), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_4),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_8)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_8), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_8),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_4)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_C),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_D)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_B), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_B)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_C), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_C),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_D)));
		map.put(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_D), Arrays.asList(Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_D),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_2),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_9),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_A),
																									Long.toString(PalmSecureConstant.JAVA_PvAPI_INFO_SENSOR_TYPE_C)));
		COMPATIBILITY_MAP = map;
	}

	public PsVeinDataFilenameFilter(long sensorType, long dataType) {
		PsVeinDataFilenameFilter.sensorType = Long.toString(sensorType);
		PsVeinDataFilenameFilter.dataType = Long.toString(dataType);
	}

	public static void setSensorType(long sensorType) {
		PsVeinDataFilenameFilter.sensorType = Long.toString(sensorType);
	}

	public static void setDataType(long dataType) {
		PsVeinDataFilenameFilter.dataType = Long.toString(dataType);
	}

//	public static void setGuideMode(long guideMode) {
//		PsVeinDataFilenameFilter.guideMode = Long.toString(guideMode);
//	}

	public boolean accept(File dir, String name) {

		boolean result = false;
		String checkFormat;

		File file = new File(dir, name);
		if (file.isFile()) {
			String fileName = file.getName();
			if (fileName.endsWith(VEIN_DATA_FILE_EXT)) {
				for (String compatSensorType :COMPATIBILITY_MAP.get(sensorType)) {
					checkFormat = compatSensorType + dataType + "_";
					if (fileName.startsWith(checkFormat)) {
						if (fileName.length() <=
								(checkFormat.length() + ID_LENGTH + VEIN_DATA_FILE_EXT.length())) {
							result = true;
							break;
						}
					}
				}
			}
		}

		return result;
	}

	public static String Ps_Sample_Apl_Java_GetId(File file) {

		String id = file.getName();								// XY_ZZZZZZZZZZZZZZZZ.dat
		id = id.substring(0, id.indexOf(VEIN_DATA_FILE_EXT));	// XY_ZZZZZZZZZZZZZZZZ
		id = id.substring(id.indexOf("_") + 1);					// ZZZZZZZZZZZZZZZZ

		return id;
	}

	public static String Ps_Sample_Apl_Java_GetFileName(File dir, String id) {

		//When using BioAPI_IdentifyMatch method between
		//I format enrollment data and I33/I format captured authentication data,
		//set "0" or "1" to "dataType".
		String header = sensorType + dataType + "_";

		StringBuffer buff = new StringBuffer(dir.getAbsolutePath());
		buff = buff.append(File.separator);
		buff = buff.append(header);
		buff = buff.append(id);
		buff = buff.append(VEIN_DATA_FILE_EXT);

		return buff.toString();
	}

//	public static String Ps_Sample_Apl_Java_GetFileName_For_GetExData(File dir, String id) {
//
//		String header = sensorType + guideMode + "_";
//
//		StringBuffer buff = new StringBuffer(dir.getAbsolutePath());
//		buff = buff.append(File.separator);
//		buff = buff.append(header);
//		buff = buff.append(id);
//		buff = buff.append(VEIN_DATA_FILE_EXT);
//
//		return buff.toString();
//	}
}
