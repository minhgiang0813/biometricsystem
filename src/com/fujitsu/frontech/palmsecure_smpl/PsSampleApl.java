/*
 * PsSampleApl.java
 *
 *	All Rights Reserved, Copyright(c) FUJITSU FRONTECH LIMITED 2021
 */
package com.fujitsu.frontech.palmsecure_smpl;

import com.chienowa.nonbio.NonSensorMainFrame;
import com.chienowa.runbackground.Screen;
import com.chienowa.ulti.DataFromController;
import com.chienowa.ulti.SerialUartProcess;
import com.chienowa.ulti.VirtualKeyboard;

public class PsSampleApl {
    
    public static int bioMode;
    

    public static void main(String[] args) {

        SerialUartProcess.StartSerialUartProcess();
        DataFromController.GetTimeFromController();
        VirtualKeyboard.SetNumpad();
        
        bioMode = DataFromController.GetBiometricModeFromController();
        if (bioMode == 1) {
            PsMainFrame mainFrame = new PsMainFrame();

            boolean result = mainFrame.Ps_Sample_Apl_Java();
            if (result != true) {
                System.exit(0);
            }
            mainFrame.setVisible(true);

        } else {
            
            NonSensorMainFrame nonBioFrame = new NonSensorMainFrame();
            nonBioFrame.setVisible(true);

        }

    }
}
