/*
 * PsThreadCapture.java
 *
 *	All Rights Reserved, Copyright(c) FUJITSU FRONTECH LIMITED 2021
 */

package com.fujitsu.frontech.palmsecure_smpl;

import com.fujitsu.frontech.palmsecure.JAVA_BioAPI_BIR;
import com.fujitsu.frontech.palmsecure.JAVA_sint32;
import com.fujitsu.frontech.palmsecure.JAVA_uint32;
import com.fujitsu.frontech.palmsecure.JAVA_uint8;
import com.fujitsu.frontech.palmsecure.PalmSecureIf;
import com.fujitsu.frontech.palmsecure.util.PalmSecureConstant;
import com.fujitsu.frontech.palmsecure.util.PalmSecureException;
import com.fujitsu.frontech.palmsecure.util.PalmSecureHelper;
import com.fujitsu.frontech.palmsecure_smpl.data.PsDataManager;
import com.fujitsu.frontech.palmsecure_smpl.data.PsThreadResult;
import com.fujitsu.frontech.palmsecure_smpl.event.PsBusinessListener;

public class PsThreadCapture extends PsThreadBase {

	public PsThreadCapture(PsMainFrame frame, PsBusinessListener businesslistener, PalmSecureIf palmsecureIf, JAVA_uint32 moduleHandle) {
		super(frame, businesslistener, palmsecureIf, moduleHandle, "");
	}

	public void run() {

		PsThreadResult stResult = new PsThreadResult();

		try {

			do {

				this.frame.captureFlg = true;

				//Capture
				///////////////////////////////////////////////////////////////////////////
				JAVA_uint8 purpose = new JAVA_uint8();
				purpose.value = PalmSecureConstant.JAVA_BioAPI_PURPOSE_VERIFY;
				JAVA_sint32 birHandle = new JAVA_sint32();
				JAVA_sint32 timeout = new JAVA_sint32();
				try {
					stResult.result = palmsecureIf.JAVA_BioAPI_Capture(
							moduleHandle,
							purpose,
							birHandle,
							timeout,
							null);
				} catch(PalmSecureException e) {
					PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, e);
					stResult.result = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
					this.frame.captureFlg = false;
					break;
				}
				///////////////////////////////////////////////////////////////////////////

				this.frame.captureFlg = false;

				//End transaction in case of cancel
				if (this.frame.cancelFlg == true) {
					break;
				}

				//If PalmSecure method failed, get error info
				if (stResult.result != PalmSecureConstant.JAVA_BioAPI_OK) {
					try {
						palmsecureIf.JAVA_PvAPI_GetErrorInfo(stResult.errInfo);
						PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, stResult.errInfo);
					} catch(PalmSecureException e) {
					}
					break;
				}

				//Log a shilouette image
				stResult.info = this.frame.silhouette;

				//Get BIR data ( vein data )
				///////////////////////////////////////////////////////////////////////////
				JAVA_BioAPI_BIR BIR = new JAVA_BioAPI_BIR();
				try {
					stResult.result = palmsecureIf.JAVA_BioAPI_GetBIRFromHandle(
							moduleHandle,
							birHandle,
							BIR);
				} catch(PalmSecureException e) {
					PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, e);
					stResult.result = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
					break;
				}
				///////////////////////////////////////////////////////////////////////////

				//If PalmSecure method failed, get error info
				if (stResult.result != PalmSecureConstant.JAVA_BioAPI_OK) {
					try {
						palmsecureIf.JAVA_PvAPI_GetErrorInfo(stResult.errInfo);
						PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, stResult.errInfo);
					} catch(PalmSecureException e) {
					}
					break;
				}

				//Free BIR handle
				///////////////////////////////////////////////////////////////////////////
				try {
					stResult.result = palmsecureIf.JAVA_BioAPI_FreeBIRHandle(
							moduleHandle,
							birHandle);
				} catch(PalmSecureException e) {
					PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, e);
					stResult.result = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
					break;
				}
				///////////////////////////////////////////////////////////////////////////

				//End transaction in case of cancel
				if (this.frame.cancelFlg == true) {
					break;
				}

				//If PalmSecure method failed, get error info
				if (stResult.result != PalmSecureConstant.JAVA_BioAPI_OK) {
					try {
						palmsecureIf.JAVA_PvAPI_GetErrorInfo(stResult.errInfo);
						PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, stResult.errInfo);
					} catch(PalmSecureException e) {
					}
					break;
				}

				byte[] bufferBIR = null;

				//Create a byte array of vein data and output vein data to file
				///////////////////////////////////////////////////////////////////////////
				try {
					bufferBIR = PalmSecureHelper.convertBIRToByte(BIR);
				} catch(PalmSecureException e) {
					PsMessageDialog.Ps_Sample_Apl_Java_ShowErrorDialog(this.frame, e);
					stResult.result = PalmSecureConstant.JAVA_BioAPI_ERRCODE_FUNCTION_FAILED;
					break;
				}
				///////////////////////////////////////////////////////////////////////////

				PsDataManager dataMng = PsDataManager.GetInstance();
				dataMng.Ps_Sample_Apl_Java_Capture(bufferBIR);

			} while (false);

			Ps_Sample_Apl_Java_NotifyResult_Capture(stResult);

		} catch(Exception e) {
		}

		return;

	}

}
